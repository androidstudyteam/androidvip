package com.firsttream.learn.persenter;

import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.learn.model.LearnWebActivityModel;
import com.firsttream.learn.view.LearnWebActivityView;

public class LearnWebActivityPersenter extends BasePersenter<LearnWebActivityModel,LearnWebActivityView> {

    @Override
    protected void onViewDestory() {
        if (model != null) {
            model.stopRequest();
        }
    }
}
