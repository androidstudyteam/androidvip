package com.firsttream.learn.persenter;

import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.learn.model.ActivityLearnListModel;
import com.firsttream.learn.model.LearnActivityModel;
import com.firsttream.learn.view.ActivityLearnListView;
import com.firsttream.learn.view.LearnActivityView;
/**
*作者：gaojiabao
*时间：2018/12/13 13:07
*作用：学习详细persenter
*/
public class ActivityLearnListPersenter extends BasePersenter<ActivityLearnListModel,ActivityLearnListView>{
    @Override
    protected void onViewDestory() {
        if (model != null) {
            model.stopRequest();
        }
    }

    public void getLearnData(String url,String studyid) {
        model.getLearnData(context,url,getView(),studyid);
    }

}
