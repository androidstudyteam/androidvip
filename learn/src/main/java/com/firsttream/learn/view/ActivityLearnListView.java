package com.firsttream.learn.view;

import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.learn.entity.LearnListBean;
/**
*作者：gaojiabao
*时间：2018/12/13 13:07
*作用：学习详细 view
*/
public interface ActivityLearnListView extends IContract.View {


    void getLearnData(String data);
}
