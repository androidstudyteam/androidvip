package com.firsttream.learn.view;

import com.firsttream.baselibrary.contract.IContract;
/**
*作者：gaojiabao
*时间：2018/12/13 13:07
*作用：学习view
*/
public interface LearnActivityView extends IContract.View {


    void getLearnData(String data);


}
