package com.firsttream.learn.model;

import android.content.Context;

import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.learn.view.LearnActivityView;
/**
*作者：gaojiabao
*时间：2018/12/13 13:06
*作用：学习model
*/
public class LearnActivityModel extends BaseModel {

    private LearnActivityView mLearnActivityView;
    private static final int LEARNTYPE = 0x321;
    public void getLearnData(Context context,IContract.View view) {
        getString(context, Http.LEARNURL, null, LEARNTYPE,true,true,true);
        mLearnActivityView = ((LearnActivityView) view);
    }

    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        switch (type) {
            case LEARNTYPE:
                ((LearnActivityView) mLearnActivityView).getLearnData(data);
                break;
        }
    }

    @Override
    public void failString(String msg) {
        super.failString(msg);

    }
}
