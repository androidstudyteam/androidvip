package com.firsttream.learn.model;

import android.content.Context;
import android.util.Log;

import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.learn.entity.LearnListBean;
import com.firsttream.learn.view.ActivityLearnListView;
import com.firsttream.learn.view.LearnActivityView;
import com.google.gson.Gson;

import java.util.HashMap;
/**
*作者：gaojiabao
*时间：2018/12/13 13:06
*作用：学习详细model
*/
public class ActivityLearnListModel extends BaseModel {
    private static final String TAG="ActivityLearnListModel";
    private ActivityLearnListView mLearnActivityView;
    private static final int LEARNTYPE = 0x322;
    public void getLearnData(Context context,String url, IContract.View view, String studyid) {
//        HashMap<String,String> map = new HashMap<>();
//        map.put("study_id",(studyid+".txt"));
        getString(context,url+(studyid+".txt"), null, LEARNTYPE,true,true,true);
        mLearnActivityView = ((ActivityLearnListView) view);
    }

    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        switch (type) {
            case LEARNTYPE:
//                LearnListBean learnListBean = new Gson().fromJson(data, LearnListBean.class);
                ((ActivityLearnListView) mLearnActivityView).getLearnData(data);
                break;
        }
    }

    @Override
    public void failString(String msg) {
        super.failString(msg);
        Log.i("学习点击后","请求失败");
    }

}
