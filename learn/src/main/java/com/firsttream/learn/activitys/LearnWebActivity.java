package com.firsttream.learn.activitys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.learn.R;
import com.firsttream.learn.model.LearnWebActivityModel;
import com.firsttream.learn.persenter.LearnWebActivityPersenter;
import com.firsttream.learn.view.LearnWebActivityView;

public class LearnWebActivity<M extends LearnWebActivityModel, V extends LearnWebActivityView, P extends LearnWebActivityPersenter> extends BaseActivity implements LearnWebActivityView {

    private WebView mWebview;
    private LearnWebActivityPersenter mLearnWebActivityPersenter;
    private static final String TAG="LearnWebActivity";
    private ProgressDialog dialog;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_learn_web;
    }

    @Override
    public IContract.Model createModel() {
        return new LearnWebActivityModel();
    }

    @Override
    public IContract.View createView() {
        return this;
    }

    @Override
    public BasePersenter createPersenter() {
        mLearnWebActivityPersenter = new LearnWebActivityPersenter();
        mLearnWebActivityPersenter.initContext(this);
        return mLearnWebActivityPersenter;
    }

    @Override
    public void initData() {
        super.initData();
        setTitle("详情");
        Intent intent = getIntent();
        String link = intent.getStringExtra("link");
        mWebview = (WebView) getView(R.id.learn__web);
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.getSettings().setAppCacheEnabled(true);
        //设置 缓存模式
        mWebview.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        // 开启 DOM storage API 功能
        mWebview.getSettings().setDomStorageEnabled(true);
        mWebview.loadUrl(link);
        mWebview.setWebViewClient(new WebViewClient() {
            //覆盖shouldOverrideUrlLoading 方法
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        dialog = ProgressDialog.show(this, "正在加载…", null);

        mWebview.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                toase("加载完毕~",1);
                dialog.dismiss();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                toase("正在加载中~",1);
            }
        });
    }

    //销毁webview
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //销毁webview
        mWebview.destroy();
    }
}
