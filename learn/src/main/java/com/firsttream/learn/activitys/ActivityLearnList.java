package com.firsttream.learn.activitys;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.baselibrary.percent.PercentRelativeLayout;
import com.firsttream.learn.R;
import com.firsttream.learn.adapter.LearnListOneAdapter;
import com.firsttream.learn.adapter.LearnListTowAdapter;
import com.firsttream.learn.entity.LearnListBean;
import com.firsttream.learn.model.ActivityLearnListModel;
import com.firsttream.learn.persenter.ActivityLearnListPersenter;
import com.firsttream.learn.view.ActivityLearnListView;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.scwang.smartrefresh.header.BezierCircleHeader;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.header.WaterDropHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

/**
 * 作者：gaojiabao
 * 时间：2018/12/13 11:20
 * 作用：学习点进去后列表
 */
public class ActivityLearnList<M extends ActivityLearnListModel, V extends ActivityLearnListPersenter, P extends ActivityLearnListView> extends BaseActivity implements ActivityLearnListView {
    private static final String TAG="ActivityLearnList";
    private ActivityLearnListPersenter mLearnListPersenter;
    private String mStudyid, mIslist, mTitle;
    private XRecyclerView mXRecyclerView;
    private LearnListOneAdapter mLearnListOneAdapter;
    private LearnListTowAdapter mLearnListTowAdapter;
    private PercentRelativeLayout mPerLayoutTop;
    private TextView mTextViewTop;
    private SimpleDraweeView mSimpTop;
    private RefreshLayout mLearnlistrefreshLayout;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_learn_list;
    }

    @Override
    public IContract.Model createModel() {
        return new ActivityLearnListModel();
    }

    @Override
    public IContract.View createView() {
        return this;
    }

    @Override
    public BasePersenter createPersenter() {
        mLearnListPersenter = new ActivityLearnListPersenter();
        mLearnListPersenter.initContext(this);
        return mLearnListPersenter;
    }



    @Override
    public void initData() {
        super.initData();
        Intent intent = getIntent();
        mStudyid = intent.getStringExtra("study_id");
        mIslist = intent.getStringExtra("islist");
        mTitle = intent.getStringExtra("title");
        setTitle(mTitle);
        initwidget();
        mLearnListPersenter.getLearnData(Http.LEARN_LATER_URL, mStudyid);
        //设置 Header 为 贝塞尔雷达 样式
        mLearnlistrefreshLayout.setRefreshHeader(new WaterDropHeader(this));
        //设置 Footer 为 球脉冲 样式
        mLearnlistrefreshLayout.setRefreshFooter(new BallPulseFooter(this).setSpinnerStyle(SpinnerStyle.Scale));
        mLearnlistrefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                //网络请求
                mLearnListPersenter.getLearnData(Http.LEARN_LATER_URL, mStudyid);
                refreshlayout.finishRefresh(1000/*,false*/);//传入false表示刷新失败
            }
        });
        mLearnlistrefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                //网络请求
                mLearnListPersenter.getLearnData(Http.LEARN_LATER_URL, mStudyid);
                refreshlayout.finishLoadMore(1000/*,false*/);//传入false表示加载失败
            }
        });
    }

    private void initwidget() {
        mLearnlistrefreshLayout = (RefreshLayout) getView(R.id.learn__list_refreshLayout);
        mXRecyclerView = (XRecyclerView) getView(R.id.list_learn_recy);
        View inflate = View.inflate(this, R.layout.activity_head_layout, null);
        mPerLayoutTop = (PercentRelativeLayout) inflate.findViewById(R.id.learn_simp_top);
        mSimpTop = (SimpleDraweeView) inflate.findViewById(R.id.learn_list1_simp_top);
        mTextViewTop = (TextView) inflate.findViewById(R.id.learn_list1_te_top);
        mXRecyclerView.addHeaderView(inflate);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mXRecyclerView.setPullRefreshEnabled(false);
        mXRecyclerView.setLayoutManager(linearLayoutManager);
        mLearnListOneAdapter = new LearnListOneAdapter(this);
        mLearnListTowAdapter = new LearnListTowAdapter(this);
        //设置适配器
        if (mIslist.equals("0")) {
            mXRecyclerView.setAdapter(mLearnListOneAdapter);
        } else if (mIslist.equals("1")) {
            mXRecyclerView.setAdapter(mLearnListTowAdapter);
        }
    }

    @Override
    public void getLearnData(String data) {
        LearnListBean learnListBean = new Gson().fromJson(data, LearnListBean.class);
        List<LearnListBean.ItemsBean> items = learnListBean.getItems();
        if (mIslist.equals("0")) {
            mSimpTop.setImageURI(Uri.parse(learnListBean.getTopimg()));
            mTextViewTop.setText(learnListBean.getTitle());
            mPerLayoutTop.setVisibility(View.VISIBLE);
            mLearnListOneAdapter.setList(items);
        } else if (mIslist.equals("1")) {
            mPerLayoutTop.setVisibility(View.GONE);
            mLearnListTowAdapter.setList(items);
        }
        mPerLayoutTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityLearnList.this, LearnWebActivity.class);
                intent.putExtra("link", learnListBean.getLink());
                startActivity(intent);
            }
        });
    }
}
