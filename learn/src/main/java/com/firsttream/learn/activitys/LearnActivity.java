package com.firsttream.learn.activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.learn.R;
import com.firsttream.learn.adapter.LearnAdapter;
import com.firsttream.learn.entity.LearnBean;
import com.firsttream.learn.model.LearnActivityModel;
import com.firsttream.learn.persenter.LearnActivityPersenter;
import com.firsttream.learn.view.LearnActivityView;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.List;
/**
*作者：gaojiabao
*时间：2018/12/13 11:21
*作用：学习首页
*/
public class LearnActivity<M extends LearnActivityModel, V extends LearnActivityView, P extends LearnActivityPersenter> extends BaseActivity implements LearnActivityView {
    private LearnActivityPersenter mLearnActivityPersenter;
    private XRecyclerView mRecyclerview;
    private LearnAdapter mLearnAdapter;
    private static final String TAG="LearnActivity";
    @Override
    public IContract.Model createModel() {
        return new LearnActivityModel();
    }

    @Override
    public IContract.View createView() {
        return this;
    }

    @Override
    public BasePersenter createPersenter() {
        mLearnActivityPersenter = new LearnActivityPersenter();
        mLearnActivityPersenter.initContext(this);
        return mLearnActivityPersenter;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.learn_activity;
    }

    @Override
    public void initData() {
        super.initData();
        setTitle("学习");
        //初始化控件
        initwidget();
        mLearnActivityPersenter.getLearnData();
    }

    //初始化控件
    private void initwidget() {
        mRecyclerview = (XRecyclerView) getView(R.id.learn_recyview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerview.setLayoutManager(linearLayoutManager);
        mRecyclerview.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                mLearnActivityPersenter.getLearnData();
            }

            @Override
            public void onLoadMore() {
                mLearnActivityPersenter.getLearnData();
                toase("没有更多了~", 1);
            }
        });
    }

    @Override
    public void getLearnData(String data) {
//        toase(data, 1);
        LearnBean learnBean = new Gson().fromJson(data, LearnBean.class);
        List<LearnBean.ItemsBean> learnlist = learnBean.getItems();
        mLearnAdapter = new LearnAdapter(this);
        mRecyclerview.setAdapter(mLearnAdapter);
        mLearnAdapter.setList(learnlist);
        mRecyclerview.refreshComplete();
    }
}
