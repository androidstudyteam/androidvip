package com.firsttream.learn.entity;

import java.util.List;
/**
*作者：gaojiabao
*时间：2018/12/13 13:06
*作用：学习详细bean
*/
public class LearnListBean {

    /**
     * topimg : https://code.aliyun.com/598254259/AndroidGuide/raw/master/image/kotlin_1.jpg
     * item_id : 0
     * position : a_20180419_01
     * title : Kotlin Primer·第一章·启程
     * isTop : false
     * link : https://kymjs.com/code/2017/02/03/01/
     * items : [{"title":"第二章·基本语法","author":"kymjs张涛","time":"2017-02-03","image":"https://code.aliyun.com/598254259/AndroidGuide/raw/master/txt/study/image7/kotlin_02.png","desc":"本章为学习和使用这一新兴的语言做准备，并讨论在一些常见开发环境中运行 Kotlin 程序的方法。","link":"https://kymjs.com/code/2017/02/04/01/","item_id":"00","isvip":true,"code":"8826","money":"888","position":"a_20180419_02"},{"title":"第三章·Kotlin 与 Java 混编","author":"kymjs张涛","time":"2017-02-07","image":"https://code.aliyun.com/598254259/AndroidGuide/raw/master/txt/study/image7/kotlin_03.png","desc":"虽然 Kotlin 的开发很方便，但当你与他人协作时，总会碰到 Java 与 Kotlin 代码共存的代码项目。本章就教你如何优雅的实现 Kotlin 与 Java 混合编程。","link":"https://kymjs.com/code/2017/02/07/01/","item_id":"02","isvip":true,"code":"8826","money":"888","position":"a_20180419_03"},{"title":"第四章·Kotlin 的类特性(上)","author":"kymjs张涛","time":"2017-02-12","image":"https://code.aliyun.com/598254259/AndroidGuide/raw/master/txt/study/image7/kotlin_041.png","desc":"而从本章开始，才会真正讲述 Kotlin 语言的神奇之处。与 Java 相同，Kotlin 声明类的关键字是class。类声明由类名、类头和类体构成。其中类头和类体都是可选的; 如果一个类没有类体，那么花括号也是可以省略的。","link":"https://code.aliyun.com/598254259/AndroidGuide/raw/master/txt/study/image7/kotlin_041.png","item_id":"02","isvip":true,"code":"8826","money":"888","position":"a_20180419_04"},{"title":"第四章·Kotlin 的类特性(下)","author":"kymjs张涛","time":"2017-02-26","image":"https://code.aliyun.com/598254259/AndroidGuide/raw/master/txt/study/image7/kotlin_042.png","desc":"前面三章的内容是写给希望快速了解 Kotlin 语言的大忙人的。而从本章开始，才会真正讲述 Kotlin 语言的神奇之处。","link":"https://kymjs.com/code/2017/02/26/01/","item_id":"02","isvip":true,"code":"8826","money":"888","position":"a_20180419_05"},{"title":"第五章·函数与闭包","author":"kymjs张涛","time":"2017-04-09","image":"https://code.aliyun.com/598254259/AndroidGuide/raw/master/txt/study/image7/kotlin_05.png","desc":"函数与闭包的特性可以算是 Kotlin 语言最大的特性了。","link":"https://kymjs.com/code/2017/04/09/01/","item_id":"02","isvip":true,"code":"8826","money":"888","position":"a_20180419_06"},{"title":"第六章·集合泛型与操作符","author":"kymjs张涛","time":"2017-06-06","image":"https://code.aliyun.com/598254259/AndroidGuide/raw/master/txt/study/image7/kotlin_06.png","desc":"所谓泛型：就是允许在定义类、接口、方法时指定类型形参，这个类型形参将在声明变量、创建对象、调用方法时动态地指定(即传入实际的类型参数，也可称为类型实参)。","link":"https://kymjs.com/code/2017/06/06/01/","item_id":"02","isvip":true,"code":"8826","money":"888","position":"a_20180419_07"},{"title":"第七章·协程库（上篇）","author":"kymjs张涛","time":"2017-11-24","image":"https://code.aliyun.com/598254259/AndroidGuide/raw/master/txt/study/image7/kotlin_071.png","desc":"协程，协作代码段。相对线程而言，协程更适合于用来实现彼此熟悉的程序组件。协程提供了一种可以避免线程阻塞的能力，这是他的核心功能。","link":"https://kymjs.com/code/2017/11/24/01/","item_id":"02","isvip":true,"code":"8826","money":"888","position":"a_20180419_08"},{"title":"第七章·协程库（中篇）","author":"kymjs张涛","time":"2017-11-06","image":"https://code.aliyun.com/598254259/AndroidGuide/raw/master/txt/study/image7/kotlin_072.png","desc":"首先回顾上一篇文章，你需要明白一点，协程是通过编码实现的一个任务。它和操作系统或者 JVM 没有任何关系，它的存在更类似于虚拟的线程。","link":"https://kymjs.com/code/2017/11/06/01/","item_id":"02","isvip":true,"code":"8826","money":"888","position":"a_20180419_09"}]
     */

    private String topimg;
    private String item_id;
    private String position;
    private String title;
    private boolean isTop;
    private String link;
    private List<ItemsBean> items;

    public String getTopimg() {
        return topimg;
    }

    public void setTopimg(String topimg) {
        this.topimg = topimg;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isIsTop() {
        return isTop;
    }

    public void setIsTop(boolean isTop) {
        this.isTop = isTop;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * title : 第二章·基本语法
         * author : kymjs张涛
         * time : 2017-02-03
         * image : https://code.aliyun.com/598254259/AndroidGuide/raw/master/txt/study/image7/kotlin_02.png
         * desc : 本章为学习和使用这一新兴的语言做准备，并讨论在一些常见开发环境中运行 Kotlin 程序的方法。
         * link : https://kymjs.com/code/2017/02/04/01/
         * item_id : 00
         * isvip : true
         * code : 8826
         * money : 888
         * position : a_20180419_02
         */

        private String title;
        private String author;
        private String time;
        private String image;
        private String desc;
        private String link;
        private String item_id;
        private boolean isvip;
        private String code;
        private String money;
        private String position;
        private boolean isClick;

        public boolean isClick() {
            return isClick;
        }

        public void setClick(boolean click) {
            isClick = click;
        }
        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getItem_id() {
            return item_id;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public boolean isIsvip() {
            return isvip;
        }

        public void setIsvip(boolean isvip) {
            this.isvip = isvip;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }
    }
}
