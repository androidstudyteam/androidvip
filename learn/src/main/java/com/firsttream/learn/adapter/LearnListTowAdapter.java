package com.firsttream.learn.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firsttream.learn.R;
import com.firsttream.learn.activitys.LearnWebActivity;
import com.firsttream.learn.entity.LearnListBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：gaojiabao
 * 时间：2018/12/13 13:06
 * 作用：学习详细列表分类二
 */
public class LearnListTowAdapter extends RecyclerView.Adapter<LearnListTowAdapter.MyViewHolder> {

    private Context context;
    private View view;

    public LearnListTowAdapter(Context context) {
        this.context = context;
    }
    private List<LearnListBean.ItemsBean> itemsBeans=new ArrayList<>();

    public void setList(List<LearnListBean.ItemsBean> itemsBeans) {
        this.itemsBeans = itemsBeans;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = View.inflate(context, R.layout.item_learnlist2_recy, null);
        MyViewHolder myViewHolder=new MyViewHolder(view);
        myViewHolder.learn_list2_lay=view.findViewById(R.id.learn_list2_lay);
        myViewHolder.learn_list2_te=view.findViewById(R.id.learn_list2_te);
        return myViewHolder;
    }



    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        //赋值给控件
        myViewHolder.learn_list2_te.setText(itemsBeans.get(i).getTitle());
        //判断换色
        if(itemsBeans.get(i).isClick()){
            myViewHolder.learn_list2_te.setTextColor(Color.RED);
        }else {
            myViewHolder.learn_list2_te.setTextColor(Color.BLACK);
        }
        myViewHolder.learn_list2_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //改变字体颜色的bean先
                for (int a=0;a<itemsBeans.size();a++){
                    if(i==a){
                        itemsBeans.get(a).setClick(true);
                    }else {
                        itemsBeans.get(a).setClick(false);
                    }
                }
                itemsBeans.get(i).setClick(true);
                //跳转
                Intent intent = new Intent(context, LearnWebActivity.class);
                intent.putExtra("link", itemsBeans.get(i).getLink());
                context.startActivity(intent);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemsBeans.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout learn_list2_lay;
        TextView learn_list2_te;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
