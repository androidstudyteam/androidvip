package com.firsttream.learn.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.firsttream.baselibrary.base.recycle.RecycleAdapter;
import com.firsttream.baselibrary.base.viewholder.ViewHolder;
import com.firsttream.learn.R;
import com.firsttream.learn.activitys.ActivityLearnList;
import com.firsttream.learn.activitys.LearnWebActivity;
import com.firsttream.learn.entity.LearnBean;
import com.firsttream.learn.entity.LearnListBean;
/**
*作者：gaojiabao
*时间：2018/12/13 13:06
*作用：学习详细列表分类一
*/
public class LearnListOneAdapter extends RecycleAdapter<LearnListBean.ItemsBean>{

    private  Context mContext;

    public LearnListOneAdapter(Context mcontext) {
        super(mcontext);
        this.mContext=mcontext;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_learnlist1_recy;
    }

    @Override
    protected void convert(ViewHolder viewHolder, LearnListBean.ItemsBean itemsBean, int postion) {
        viewHolder.setSimpleDraweViewUrl(R.id.learn_list1_simp,itemsBean.getImage());
        viewHolder.setText(R.id.learn_list1_te1,itemsBean.getTitle());
        viewHolder.setText(R.id.learn_list1_te2,itemsBean.getAuthor()+"   "+itemsBean.getTime());
        viewHolder.setText(R.id.learn_list1_te3,itemsBean.getDesc());
        viewHolder.setClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(context,"URL"+itemsBean.getLink(),Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, LearnWebActivity.class);
                intent.putExtra("link",itemsBean.getLink());
                mContext.startActivity(intent);
            }
        },R.id.learn_list1_lay);

    }
}
