package com.firsttream.learn.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.firsttream.baselibrary.base.recycle.RecycleAdapter;
import com.firsttream.baselibrary.base.viewholder.ViewHolder;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.learn.R;
import com.firsttream.learn.activitys.ActivityLearnList;
import com.firsttream.learn.activitys.LearnActivity;
import com.firsttream.learn.entity.LearnBean;
/**
*作者：gaojiabao
*时间：2018/12/13 13:05
*作用：学习列表展示适配器
*/
public class LearnAdapter extends RecycleAdapter<LearnBean.ItemsBean>{

    private  Context mContext;

    public LearnAdapter(Context mcontext) {
        super(mcontext);
        this.mContext=mcontext;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_learn_recy;
    }

    @Override
    protected void convert(ViewHolder viewHolder, LearnBean.ItemsBean itemsBean, int postion) {
       viewHolder.setSimpleDraweViewUrl(R.id.item_learn_simp,itemsBean.getImage());
       viewHolder.setText(R.id.item_learn_te1,itemsBean.getTitle());
        viewHolder.setClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(mContext,ActivityLearnList.class);
               intent.putExtra("study_id",itemsBean.getStudy_id());
               intent.putExtra("islist",itemsBean.getIslist()+"");
               intent.putExtra("title",itemsBean.getTitle());
                mContext.startActivity(intent);
            }
        }, R.id.learn_layout);
    }
}
