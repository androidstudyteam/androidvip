package com.firsttream.account.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.firsttream.account.R;
import com.firsttream.account.entity.RobotBean;

import java.util.ArrayList;
import java.util.List;

/**
*作者：gaojiabao
*时间：2018/12/25 9:16
*作用：连天对话适配器
*/
public class ChatMessageAdapter extends BaseAdapter {// /继承BaseAdapter，实现方法

    private List<RobotBean> lists=new ArrayList<>();// 集合的数据内容
    private Context mContext;// 承接上下文的Context
    private RelativeLayout layout;
    public ChatMessageAdapter( Context mContext) {
        this.mContext = mContext;
    }

    public void setLists(List<RobotBean> lists) {
        this.lists = lists;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {// 返回lists所承载的条数
        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {// convertView表示要加载的view，在本例子中有两个view要加载的，一是机器人说话，二是用户说话，分别对应leftitem和rightitem
        LayoutInflater inflater = LayoutInflater.from(mContext);
        if (lists.get(position).getFlag() == RobotBean.RECEIVE) {
            layout = (RelativeLayout) inflater.inflate(R.layout.robot_item_left, null);
            SimpleDraweeView iv_left = (SimpleDraweeView)layout.findViewById(R.id.iv_left);
            iv_left.setImageURI("res:///"+R.drawable.app_icon);
        }
        if (lists.get(position).getFlag() == RobotBean.SEND) {
            layout = (RelativeLayout) inflater
                    .inflate(R.layout.robot_item_right, null);
            SimpleDraweeView iv_right = (SimpleDraweeView)layout.findViewById(R.id.iv_right);
            iv_right.setImageURI("res:///"+R.drawable.start_icon);
        }
        TextView tv = (TextView) layout.findViewById(R.id.tv);// 对话textView
        TextView tv_time = (TextView) layout.findViewById(R.id.tv_time);// 时间textView
        tv.setText(lists.get(position).getText()); // 将数据内容放进TextView中
        tv_time.setText(lists.get(position).getTime());// 将事件写进TextView
        return layout;
    }
}
