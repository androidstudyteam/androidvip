package com.firsttream.account.adapter;

import android.content.Context;

import com.firsttream.account.R;
import com.firsttream.account.entity.AttentionForMeBean;
import com.firsttream.baselibrary.base.recycle.RecycleAdapter;
import com.firsttream.baselibrary.base.viewholder.ViewHolder;

public    class AttentionAdapter extends RecycleAdapter<AttentionForMeBean.ItemsBean> {
    public AttentionAdapter(Context mcontext) {
        super(mcontext);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_attention_iteam;
    }

    @Override
    protected void convert(ViewHolder viewHolder, AttentionForMeBean.ItemsBean itemsBean, int postion) {
        viewHolder.setText(R.id.txt_username,itemsBean.getMy_nick()+"关注了您");
    }
}
