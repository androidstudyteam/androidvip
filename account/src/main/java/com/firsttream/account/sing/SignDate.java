package com.firsttream.account.sing;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firsttream.account.R;
import com.firsttream.baselibrary.util.Logger;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 2017/8/16.
 */

public class SignDate extends LinearLayout {

    private TextView tvYear;
    private InnerGridView gvWeek;
    private InnerGridView gvDate;
    private AdapterDate adapterDate;
    private Context mContext;
    private List<String> nowDays = new ArrayList<>();
    public SignDate(Context context) {
        super(context);
        this.mContext = context;
        init();
    }

    public SignDate(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init();
    }

    public SignDate(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init();
    }

    private void init(){
      //  View view = View.inflate(mContext, R.layout.layout_signdate,null);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_signdate,null);
        tvYear = view.findViewById(R.id.tvYear);
        gvWeek = view.findViewById(R.id.gvWeek);
        gvDate = view.findViewById(R.id.gvDate);
        tvYear.setText(DateUtil.getCurrentYearAndMonth());
        gvWeek.setAdapter(new AdapterWeek(mContext));
        adapterDate = new AdapterDate(mContext);
        gvDate.setAdapter(adapterDate);
        addView(view);
    }

    /**
     * 签到成功的回调
     * @param onSignedSuccess
     */
    public void setOnSignedSuccess(OnSignedSuccess onSignedSuccess){
        adapterDate.setOnSignedSuccess(onSignedSuccess);
    }

    public void setList(List<String> nowDays) {
        this.nowDays = nowDays;
        adapterDate.setList(nowDays);
    }

    public void notfiyChange(){
        adapterDate.notifyDataSetChanged();
    }
}