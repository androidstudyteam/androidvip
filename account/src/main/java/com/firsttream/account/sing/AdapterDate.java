package com.firsttream.account.sing;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firsttream.account.R;
import com.firsttream.account.entity.DateEntity;
import com.firsttream.baselibrary.util.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2018/12/26.
 */

public class AdapterDate extends BaseAdapter {

    private  int mFlag;
    private  String mDay;
    private Context context;
    private List<Integer> days = new ArrayList<>();
    private List<String> nowDays = new ArrayList<>();
    //日历数据
    private List<Boolean> status = new ArrayList<>();
    //签到状态，实际应用中初始化签到状态可通过该字段传递
    private OnSignedSuccess onSignedSuccess;
    private List<DateEntity> dates = new ArrayList<>();

    //签到成功的回调方法，相应的可自行添加签到失败时的回调方法

    public AdapterDate(Context context) {
        this.context = context;
        int maxDay = DateUtil.getCurrentMonthLastDay();//获取当月天数
        String currentYearAndMonth = DateUtil.getCurrentYearAndMonth();
        for (int i = 0; i < DateUtil.getFirstDayOfMonth() - 1; i++) {
            //DateUtil.getFirstDayOfMonth()获取当月第一天是星期几，星期日是第一天，依次类推
            days.add(0);
            //0代表需要隐藏的item
            status.add(false);
            //false代表为签到状态
        }
        for (int i = 0; i < maxDay; i++) {
//            days.add(i+1);
            //初始化日历数据
            status.add(false);
            //初始化日历签到状态
            DateEntity dateEntity = new DateEntity();
            dateEntity.setDate(currentYearAndMonth+"-"+(i+1));
            dateEntity.setDay((i+1)+"");
            dates.add(dateEntity);
        }

        SimpleDateFormat mSimpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        mDay=mSimpleDateFormat.format(new Date());
        //判斷是否是当前
        String[] split = mDay.split("-");
        mFlag = Integer.parseInt(split[2]);


    }

    @Override
    public int getCount() {
        return dates.size();
    }

    @Override
    public Object getItem(int i) {
        return dates.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if(view==null){
            view = LayoutInflater.from(context).inflate(R.layout.item_gv,null);
            viewHolder = new ViewHolder();
            view.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.tv = view.findViewById(R.id.tvWeek);
        viewHolder.rlItem = view.findViewById(R.id.rlItem);
        viewHolder.ivStatus = view.findViewById(R.id.ivStatus);
        viewHolder.tv.setText(dates.get(i).getDay()+"");

        //初始化样式
       if (dates.get(i).isChecked()){
           viewHolder.ivStatus.setVisibility(View.VISIBLE);
       } else {
           viewHolder.ivStatus.setVisibility(View.GONE);
       }

        if (  Integer.parseInt(dates.get(i).getDay()) >= mFlag){
            viewHolder.tv.setTextColor(Color.parseColor("#FD0000") );
        }else{
            viewHolder.tv.setTextColor(Color.GRAY);
        }

        //如果为0隐藏掉
        if(Integer.parseInt(dates.get(i).getDay())==0){
            viewHolder.rlItem.setVisibility(View.GONE);
        }

        //为当天设置点击事件
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( dates.get(i).getDay().equals(mFlag+"")){
                    notifyDataSetChanged();
                    if(onSignedSuccess!=null){
                        onSignedSuccess.OnSignedSuccess();
                    }
                }

            }
        });

        return view;
    }

    class ViewHolder{
        RelativeLayout rlItem;
        TextView tv;
        ImageView ivStatus;
    }

    public void setOnSignedSuccess(OnSignedSuccess onSignedSuccess){
        this.onSignedSuccess = onSignedSuccess;
    }

    public void setList(List<String> nowDays){
        this.nowDays = nowDays;
        for (DateEntity entity : dates) {
            for (int i = 0 ;i <nowDays.size(); i ++) {
                String s = nowDays.get(i);
                if (s.equals(entity.getDate())){
                    entity.setChecked(true);
                }else{
                    entity.setChecked(false);
                }
            }
        }


    notifyDataSetChanged();
    }
}
