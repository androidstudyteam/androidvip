package com.firsttream.account.utils;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import com.firsttream.account.R;

import java.util.List;

public class PopWindowUtils {

    private final View view;
    private final PopupWindow mPopupWindow;
    private String data;
    private Context context;
    private Spinner spinner;
    private TextView txtTitle;

    public PopWindowUtils(Context mContext, int mScreenWidth, int mScreenHeight) {
        this.context = mContext;
        view = LayoutInflater.from(mContext).inflate(R.layout.layout_select_dialog, null);

        //初始化控件
        initWeight();
        //创建popwindow
        mPopupWindow = new PopupWindow(view, mScreenWidth, mScreenHeight, true);
        // 需要设置一下此参数，点击外边可消失
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        // 设置点击窗口外边窗口消失    这两步用于点击手机的返回键的时候，不是直接关闭activity,而是关闭pop框
        mPopupWindow.setOutsideTouchable(true);
        // 设置此参数获得焦点，否则无法点击，即：事件拦截消费
        mPopupWindow.setFocusable(true);
        //设置动画   采用属性动画
        mPopupWindow.setAnimationStyle(R.style.BaseDialog);
        mPopupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT); //设置宽度 布局文件里设置的没有用
        mPopupWindow.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);  //设置高度  必须代码设置
        mPopupWindow.setContentView(view);  //设置布局
        //popupWindow.showAsDropDown(window_view);  //设置显示位置

        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        mPopupWindow.setOutsideTouchable(true); //外部是否可点击 点击外部消失
        mPopupWindow.setFocusable(true);  //响应返回键  点击返回键 消失
    }

    private void initWeight() {
        //初始化控件
        spinner = (Spinner) view.findViewById(R.id.spinner);
        txtTitle = (TextView) view.findViewById(R.id.txt_title);
        //点击pop框的其他地方也可以关闭pop框
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupWindow.dismiss();
            }
        });

        view.findViewById(R.id.btn_updata).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.backData(data);
            }
        });
    }


    public PopWindowUtils setTitle(String title) {
        txtTitle.setText(title);
        return this;
    }

    public void show(final List<String> datas) {
        //设置监听
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                data = datas.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //初始化adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1, datas);
        //设置adapter
        spinner.setAdapter(adapter);

        mPopupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);      //相对父布局
    }

    private BackDataListener listener;

    public PopWindowUtils setListener(BackDataListener listener) {
        this.listener = listener;
        return this;
    }

    public interface BackDataListener {
        void backData(String data);
    }

}
