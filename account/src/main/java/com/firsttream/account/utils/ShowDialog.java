package com.firsttream.account.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.firsttream.account.R;
import com.firsttream.baselibrary.util.LodingDialog;

public class ShowDialog extends Dialog {

    public ShowDialog(Context context) {
        this(context, R.style.BaseDialog);
    }

    public ShowDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        initView(context);
    }

    protected ShowDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        int w = getWinWidth(context);
        WindowManager.LayoutParams params = this.getWindow().getAttributes();
        params.width = w - (w >> 3);
        this.getWindow().setAttributes(params);
    }

    public int getWinWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        return wm.getDefaultDisplay().getWidth();//
    }

    /**
     * 初始化
     *
     * @param context
     */
    private void initView(Context context) {
        int w = getWinWidth(context);
        WindowManager.LayoutParams params = this.getWindow().getAttributes();
        params.width = w - (w >> 3);
        this.getWindow().setAttributes(params);
    }

    public static class Builder {

        private final ShowDialog dialog;
        private Context context;
        private TextView txtTitle;
        private EditText etContent;
        private OnBackContentListener listener;


        public Builder(Context context) {
            this.context = context;
            dialog = new ShowDialog(context);
            View view = LayoutInflater.from(context).inflate(R.layout.layout_dialog, null);
            initWeight(view);
            dialog.addContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }

        public Builder setListener(OnBackContentListener listener) {
            this.listener = listener;
            return this;
        }


        public ShowDialog create() {
            return dialog;
        }

        private void initWeight(View view) {
            txtTitle = (TextView) view.findViewById(R.id.txt_title);
            etContent = (EditText) view.findViewById(R.id.et_content);
            view.findViewById(R.id.btn_updata).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String trim = etContent.getText().toString().trim();
                    listener.backContent(trim);
                }
            });
        }

        public Builder setTitle(String title) {
            txtTitle.setText(title);
            return this;
        }
    }

    public interface OnBackContentListener {
        void backContent(String data);
    }

}
