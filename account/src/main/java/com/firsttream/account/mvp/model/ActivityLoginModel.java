package com.firsttream.account.mvp.model;

import android.content.Context;

import com.firsttream.account.mvp.view.ActivityLoginView;
import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;

import java.util.HashMap;
import java.util.Map;

/**
 * 作者：周建峰
 * 时间：2018-12-16
 * 登录注册的model
 */
public class ActivityLoginModel extends BaseModel {

    private static final int USER_REIGIS_CODE =  0x424;
    private IContract.View view;
    private static final int USER_LOGIN_CODE = 0x423;

    public void userLogin(Context context, IContract.View view, String email, String pass) {
        this.view = view;
        Map<String, String> map = new HashMap<>();
        map.put("user_name", email);
        map.put("user_pass", pass);
        postString(context, Http.USER_LOGIN_URL, map, USER_LOGIN_CODE, false, true,false);
    }


    public void userReigis(Context context, IContract.View view, String email, String pass) {
        this.view = view;
        Map<String, String> map = new HashMap<>();
        map.put("user_name", email);
        map.put("user_pass", pass);
        postString(context, Http.USER_REIGIS_URL, map, USER_REIGIS_CODE, false, true,false);
    }


    @Override
    public void successString(String data, int type) {
        super.successString(data, type);

        switch (type){
            case USER_LOGIN_CODE:
                ((ActivityLoginView)view).backUserLogin(data);
                break;
            case USER_REIGIS_CODE:
                ((ActivityLoginView)view).backUserReigis(data);
                break;
        }

    }

    @Override
    public void failString(String msg) {
        super.failString(msg);
    }
}