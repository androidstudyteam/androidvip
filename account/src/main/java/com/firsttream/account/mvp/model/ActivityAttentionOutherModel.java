package com.firsttream.account.mvp.model;

import android.content.Context;

import com.firsttream.account.mvp.view.ActivityAttentionOutherView;
import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;

import java.util.HashMap;
import java.util.Map;

public    class ActivityAttentionOutherModel extends BaseModel {

    private static final int USER_ATTENTUON_OUTHER = 0x442;
    private IContract.View view;

    public void getAttentionOuther(Context context, IContract.View view, String useId){
        this.view = view;
        Map<String, String> map = new HashMap();
        map.put("my_user", useId);
        getString(context, Http.USER_STUDENT_OUTHER, map, USER_ATTENTUON_OUTHER, false, false, false);
    }


    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        switch (type) {
            case USER_ATTENTUON_OUTHER:
                ((ActivityAttentionOutherView) view).getAttentionViewOuther(data);
                break;
        }
    }

    @Override
    public void failString(String msg) {
        super.failString(msg);
    }
}
