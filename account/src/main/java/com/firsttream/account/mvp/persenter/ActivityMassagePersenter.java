package com.firsttream.account.mvp.persenter;

import com.firsttream.account.mvp.model.ActivityMassageModel;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;

/**
 * 作者:周建峰
 * 时间;2018-12-20
 * ActivityMassagePersenter
 *
 * @param <M>
 * @param <V>
 */
public class ActivityMassagePersenter<M extends IContract.Model, V extends IContract.View> extends BasePersenter<M, V> {
    @Override
    protected void onViewDestory() {

    }

    /**
     * 修改昵称
     *
     * @param userName
     * @param userNickName
     */
    public void updataNickName(String userName, String userNickName) {
        ((ActivityMassageModel) model).upDataNickName(context, getView(), userName, userNickName);
    }

    /**
     * 修改地址
     *
     * @param userName
     * @param userAddress
     */
    public void updataAddress(String userName, String userAddress) {
        ((ActivityMassageModel) model).upDataAddress(context, getView(), userName, userAddress);
    }

    /**
     * 修改年龄
     *
     * @param userName
     * @param age
     */
    public void updataAge(String userName, String age) {
        ((ActivityMassageModel) model).upDataAge(context, getView(), userName, age);
    }


    /**
     * 修改手机号
     *
     * @param userName
     * @param phone
     */
    public void updataPhone(String userName, String phone) {
        ((ActivityMassageModel) model).upDataPhone(context, getView(), userName, phone);
    }


    /**
     * 修改公司
     *
     * @param userName
     * @param company
     */
    public void updataCompany(String userName, String company) {
        ((ActivityMassageModel) model).upDataCompany(context, getView(), userName, company);
    }

    /**
     * 修改性别
     *
     * @param userName
     * @param sex
     */
    public void updataSex(String userName, String sex) {
        ((ActivityMassageModel) model).upDataSex(context, getView(), userName, sex);
    }

    /**
     * 修改微博
     *
     * @param userName
     * @param wb
     */
    public void updataWb(String userName, String wb) {
        ((ActivityMassageModel) model).upDataWb(context, getView(), userName, wb);
    }

}
