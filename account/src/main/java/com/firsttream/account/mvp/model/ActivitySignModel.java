package com.firsttream.account.mvp.model;

import android.content.Context;

import com.firsttream.account.mvp.view.ActivitySignView;
import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;

import java.util.HashMap;
import java.util.Map;

public    class ActivitySignModel extends BaseModel {

    private static final int USER_IS_SIGN = 0x451;
    private static final int USER_ADD_SIGN = 0x452;
    private static final int USER_QUERY_SIGN = 0x453;
    private IContract.View view;

    /**
     * 判斷是否簽到
     * @param context
     * @param view
     * @param userName
     * @param userDay
     */
    public void getIsSign(Context context, IContract.View view, String userName,String userDay){
        this.view = view;
        Map<String, String> map = new HashMap();
        map.put("user_name", userName);
        map.put("user_day", userDay);
        getString(context, Http.USER_IS_SIGN, map,USER_IS_SIGN, false, false, false);
    }

    /**
     *
     * 签到
     * @param context
     * @param view
     * @param userName
     * @param userNickName
     */
    public void getAddSign(Context context, IContract.View view, String userName,String userNickName) {
        this.view = view;
        Map<String, String> map = new HashMap();
        map.put("user_name", userName);
        map.put("user_nick", userNickName);
        getString(context, Http.USRE_ADD_SIGN, map,USER_ADD_SIGN, false, false, false);
    }

    /**
     * 查询签到
     * @param context
     * @param view
     * @param userName
     */
    public void getQuerySign(Context context, IContract.View view, String userName) {
        this.view = view;
        Map<String, String> map = new HashMap();
        map.put("user_name", userName);
        getString(context, Http.USER_QUERY_SIGN, map,USER_QUERY_SIGN, false, false, false);
    }


    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        switch (type){
            case USER_IS_SIGN:
                ((ActivitySignView)view).getIsSign(data);
                break;
            case USER_ADD_SIGN:
                ((ActivitySignView)view).getAddSign(data);
                break;
            case USER_QUERY_SIGN:
                ((ActivitySignView)view).getQuerySign(data);
                break;
        }
    }
}
