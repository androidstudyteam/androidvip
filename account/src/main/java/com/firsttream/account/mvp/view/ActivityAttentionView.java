package com.firsttream.account.mvp.view;

import com.firsttream.baselibrary.contract.IContract;

public interface ActivityAttentionView extends IContract.View {

    void getAttentionForMe(String data);

}