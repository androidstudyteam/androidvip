package com.firsttream.account.mvp.view;

import com.firsttream.baselibrary.contract.IContract;

public interface ActivitySignView extends IContract.View{
    //是否签到过
    void getIsSign(String data);

    //签到
    void getAddSign(String data);

    //查询签到的日期

    void getQuerySign(String data);
}
