package com.firsttream.account.mvp.view;


import com.firsttream.baselibrary.contract.IContract;

/**
 * 作者:高佳宝
 * 日期:2018/12/19
 * 作用:显示机器人页面的Activity view 接口 RobotActivityView
 */

//继承契约类的view 接口
public interface RobotActivityView extends IContract.View{

}
