package com.firsttream.account.mvp.view;

import com.firsttream.baselibrary.contract.IContract;

public interface ActivityMassageView extends IContract.View {

    /**
     * 修改昵称
     *
     * @param data
     */
    void getUpdataNickName(String data);

    /**
     * 修改地址
     */

    void getUpdataAddress(String data);

    /**
     * 修改年龄
     *
     * @param data
     */
    void getUpdataAge(String data);


    /**
     * 修改手机号
     */
    void getUpdataPhone(String data);

    /**
     * 修改公司
     */
    void getUpdataCompany(String data);

    /**
     * 修改手机号
     */
    void getUpdataSex(String data);

    /**
     * 修改微博
     * @param data
     */
    void getUpdataWb(String data);
}
