package com.firsttream.account.mvp.persenter;

import com.firsttream.account.mvp.model.ActivityAttentionOutherModel;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;

public    class ActivityAttentionOutherPersenter<M extends IContract.Model,V extends IContract.View> extends BasePersenter<M,V> {
    @Override
    protected void onViewDestory() {

    }

    /**
     * 获取关注我的用户
     * @param userId
     */
    public void getAttentionOuther(String userId){
        ( (ActivityAttentionOutherModel)model).getAttentionOuther(context,getView(),userId);
    }

}
