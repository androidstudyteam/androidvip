package com.firsttream.account.mvp.persenter;

import com.firsttream.account.mvp.model.ActivityLoginModel;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;

/**
 * 作者：周建峰
 * 时间：2018-12-16
 * 登录注册页面的persenter
 *
 * @param <M>
 * @param <V>
 */
public class ActivityLoginPersenter<M extends IContract.Model, V extends IContract.View> extends BasePersenter<M, V> {
    @Override
    protected void onViewDestory() {
//        destory();
    }

    public void userLogin(String email, String pass) {
        ((ActivityLoginModel) model).userLogin(context, getView(), email, pass);
    }

    public void userReigis(String email, String pass) {
        ((ActivityLoginModel) model).userReigis(context,getView(),email,pass);
    }
}
