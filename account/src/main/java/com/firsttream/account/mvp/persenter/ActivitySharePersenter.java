package com.firsttream.account.mvp.persenter;

import com.firsttream.account.mvp.model.ActivityAttentionOutherModel;
import com.firsttream.account.mvp.model.ActivityShareModel;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;

public    class ActivitySharePersenter<M extends IContract.Model,V extends IContract.View> extends BasePersenter<M,V>{
    @Override
    protected void onViewDestory() {

    }
    /**
     * 获取关注我的分享
     * @param userId
     */
    public void getAttentionShare(String userId){
        ( (ActivityShareModel)model).getAttentionForMe(context,getView(),userId);
    }


}
