package com.firsttream.account.mvp.persenter;

import com.firsttream.account.mvp.model.ActivityMassageModel;
import com.firsttream.account.mvp.model.ActivitySignModel;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;

public  class ActivitySignPersenter<M extends IContract.Model,V extends IContract.View>  extends BasePersenter<M,V> {
    @Override
    protected void onViewDestory() {

    }

    /**
     * 判断是否签到
     * @param userName
     * @param day
     */
    public void getIsSign(String userName, String day) {
        ((ActivitySignModel) model).getIsSign(context, getView(), userName, day);
    }

    /**
     * 签到
     * @param userName
     * @param userNickName
     */
    public void getAddSign(String userName, String userNickName){
        ((ActivitySignModel) model).getAddSign(context,getView(),userName,userNickName);
    }


    /**
     * 查询签到的日期
     * @param userName
     */
    public void getQuerySign(String userName){
        ((ActivitySignModel) model).getQuerySign(context,getView(),userName);
    }

}