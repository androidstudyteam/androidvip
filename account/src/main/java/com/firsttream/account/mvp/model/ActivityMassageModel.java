package com.firsttream.account.mvp.model;

import android.content.Context;

import com.firsttream.account.mvp.view.ActivityMassageView;
import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;

import java.util.HashMap;
import java.util.Map;

public class ActivityMassageModel extends BaseModel {

    private static final int UPDATA_NICKNAME = 0x431;
    private static final int UPDATA_ADDRESS = 0x432;
    private static final int UPDATA_AGE = 0x433;
    private static final int UPDATA_PHONE = 0x434;
    private static final int UPDATA_COMPANY = 0x435;
    private static final int UPDATA_SEX = 0x436;
    private static final int UPDATA_WB = 0x437;
    private IContract.View view;


    /**
     * 修改昵称
     *
     * @param context
     * @param view
     * @param userName
     * @param nickName
     */
    public void upDataNickName(Context context, IContract.View view, String userName, String nickName) {
        this.view = view;
        Map<String, String> map = new HashMap<>();
        map.put("user_name", userName);
        map.put("user_nickname", nickName);
        getString(context, Http.USER_UPDATA_NICKNAME, map, UPDATA_NICKNAME, false, false, false);
    }


    /**
     * 修改地址
     *
     * @param context
     * @param view
     * @param userName
     * @param address
     */
    public void upDataAddress(Context context, IContract.View view, String userName, String address) {
        this.view = view;
        Map<String, String> map = new HashMap<>();
        map.put("user_name", userName);
        map.put("user_address", address);
        getString(context, Http.USERCHANGE_ADDRESS, map, UPDATA_ADDRESS, false, false, false);
    }

    /**
     * 修改年龄
     *
     * @param context
     * @param view
     * @param userName
     * @param age
     */
    public void upDataAge(Context context, IContract.View view, String userName, String age) {
        this.view = view;
        Map<String, String> map = new HashMap<>();
        map.put("user_name", userName);
        map.put("user_age", age);
        getString(context, Http.USERCHANGE_AGE, map, UPDATA_AGE, false, false, false);
    }


    /**
     * 修改手机号
     *
     * @param context
     * @param view
     * @param userName
     * @param phone
     */
    public void upDataPhone(Context context, IContract.View view, String userName, String phone) {
        this.view = view;
        Map<String, String> map = new HashMap<>();
        map.put("user_name", userName);
        map.put("user_phone", phone);
        getString(context, Http.USERCHANGE_PHONE, map, UPDATA_PHONE, false, false, false);
    }


    /**
     * 修改公司
     *
     * @param context
     * @param view
     * @param userName
     * @param phone
     */
    public void upDataCompany(Context context, IContract.View view, String userName, String phone) {
        this.view = view;
        Map<String, String> map = new HashMap<>();
        map.put("user_name", userName);
        map.put("user_company", phone);
        getString(context, Http.USERCHANGE_COMPANY, map, UPDATA_COMPANY, false, false, false);
    }


    /**
     * 修改年龄
     *
     * @param context
     * @param view
     * @param userName
     * @param sex
     */
    public void upDataSex(Context context, IContract.View view, String userName, String sex) {
        this.view = view;
        Map<String, String> map = new HashMap<>();
        map.put("user_name", userName);
        map.put("user_sex", sex);
        postString(context, Http.USERCHANGE_SEX, map, UPDATA_SEX, false, false, false);
    }


    public void upDataWb(Context context, IContract.View view, String userName, String wb) {
        this.view = view;
        Map<String, String> map = new HashMap<>();
        map.put("user_name", userName);
        map.put("user_wb", wb);
        getString(context, Http.USERCHANGE_WB, map, UPDATA_WB, false, false, false);
    }


    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        switch (type) {
            case UPDATA_NICKNAME:
                ((ActivityMassageView) view).getUpdataNickName(data);
                break;
            case UPDATA_ADDRESS:
                ((ActivityMassageView) view).getUpdataAddress(data);
                break;
            case UPDATA_AGE:
                ((ActivityMassageView) view).getUpdataAge(data);
                break;
            case UPDATA_PHONE:
                ((ActivityMassageView) view).getUpdataPhone(data);
                break;
            case UPDATA_COMPANY:
                ((ActivityMassageView) view).getUpdataCompany(data);
            case UPDATA_SEX:
                ((ActivityMassageView) view).getUpdataSex(data);
                break;
            case UPDATA_WB:
                ((ActivityMassageView) view).getUpdataWb(data);
                break;
        }
    }

    @Override
    public void failString(String msg) {
        super.failString(msg);

    }
}
