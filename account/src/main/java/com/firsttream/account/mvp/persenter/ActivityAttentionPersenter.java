package com.firsttream.account.mvp.persenter;

import com.firsttream.account.mvp.model.ActivityAttentionModel;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;

public class ActivityAttentionPersenter<M extends IContract.Model, V extends IContract.View> extends BasePersenter<M, V> {
    @Override
    protected void onViewDestory() {

    }

    /**
     * 获取关注我的
     * @param userId
     *
     */
    public void getAttentionFoeMe(String userId) {
        ((ActivityAttentionModel) model).getAttentionForMe(context, getView(), userId);
    }

}
