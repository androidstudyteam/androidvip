package com.firsttream.account.mvp.persenter;


import com.firsttream.account.mvp.model.RobotActivityModle;
import com.firsttream.account.mvp.view.RobotActivityView;
import com.firsttream.baselibrary.base.BasePersenter;

/**
 * 作者:高佳宝
 * 日期:2018/12/19
 * 作用:显示机器人页面的Activity Persenter RobotActivityPersenter
 */

//继承父类persenter
public class RobotActivityPersenter extends BasePersenter<RobotActivityModle,RobotActivityView>{

    @Override
    protected void onViewDestory() {
        //判断modle非空
        if(model!=null){
            //非空就用modle调用方法
            model.stopRequest();
        }
    }
    //要是生成第二个方法 就删了就不报错了


}
