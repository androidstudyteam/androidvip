package com.firsttream.account.mvp.view;

import com.firsttream.baselibrary.contract.IContract;

public interface ActivityLoginView extends IContract.View {

    void backUserLogin(String data);

    void backUserReigis(String data);

}
