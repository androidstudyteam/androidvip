package com.firsttream.account.mvp.model;

import android.content.Context;

import com.firsttream.account.mvp.view.ActivityAttentionView;
import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;

import java.util.HashMap;
import java.util.Map;

public class ActivityAttentionModel extends BaseModel {

    private static final int USER_FOCUS = 0x441;
    private IContract.View view;

    /**
     * 请求关注我的
     *
     * @param context
     * @param view
     * @param useId
     */
    public void getAttentionForMe(Context context, IContract.View view, String useId) {
        this.view = view;
        Map<String, String> map = new HashMap();
        map.put("my_user", useId);
        getString(context, Http.USERFOCUS_ME, map, USER_FOCUS, true, true, true);
    }

    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        switch (type) {
            case USER_FOCUS:
                ((ActivityAttentionView) view).getAttentionForMe(data);
                break;
        }
    }

    @Override
    public void failString(String msg) {
        super.failString(msg);
    }
}
