package com.firsttream.account.mvp.model;

import android.content.Context;

import com.firsttream.account.activitys.ActivityShare;
import com.firsttream.account.mvp.view.ActivityAttentionOutherView;
import com.firsttream.account.mvp.view.ActivityShareViwe;
import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;

import java.util.HashMap;
import java.util.Map;

public    class ActivityShareModel extends BaseModel {
    private IContract.View view;
    private static final int USER_SHARE = 0x443;


    /**
     *
     * 获取我分享的文章
     * @param context
     * @param view
     * @param useId
     */
    public void getAttentionForMe(Context context, IContract.View view, String useId){
        this.view = view;
        Map<String, String> map = new HashMap();
        map.put("share_user", useId);
        getString(context, Http.USER_ARITICLE, map, USER_SHARE, false, false, false);
    }


    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        switch (type) {
            case USER_SHARE:
                ((ActivityShareViwe)view).getShareFoMe(data);
                break;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
