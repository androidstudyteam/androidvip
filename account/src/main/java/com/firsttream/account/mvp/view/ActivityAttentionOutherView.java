package com.firsttream.account.mvp.view;

import com.firsttream.baselibrary.contract.IContract;

public interface ActivityAttentionOutherView extends IContract.View{
    void getAttentionViewOuther(String data);
}
