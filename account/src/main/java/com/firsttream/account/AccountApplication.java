package com.firsttream.account;

import android.app.Application;

import com.mob.MobSDK;

public class AccountApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        MobSDK.init(this);
    }
}
