package com.firsttream.account.activitys;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firsttream.account.R;
import com.firsttream.account.entity.RegisterEntity;
import com.firsttream.account.entity.UserEntity;
import com.firsttream.account.mvp.model.ActivityLoginModel;
import com.firsttream.account.mvp.persenter.ActivityLoginPersenter;
import com.firsttream.account.mvp.view.ActivityLoginView;
import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.percent.PercentRelativeLayout;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.baselibrary.util.Md5Util;
import com.firsttream.baselibrary.util.SpUtil;
import com.firsttream.baselibrary.util.Validator;
import com.google.gson.Gson;

import java.util.Timer;

import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;

/**
 * 作者：周建峰
 * 时间：2018-12-14
 *
 * @param <M>
 * @param <V>
 * @param <P>
 */
public class ActivityLoginOrRegister<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> implements ActivityLoginView, View.OnClickListener {

    private static final String TAG = ActivityLoginOrRegister.class.getName();
    private ActivityLoginPersenter persenter;
    private TextView txtLogin, txtReigis;
    private PercentRelativeLayout prLogin, prReigis;
    private View viewLinLeft, viewLinRight;
    private Timer timer;
    private EditText etUserLoginMailbox,
            etUserLoginPass,
            etUserVerifier,
            etUserReigisMailbox,
            etUserReigisPass,
            etUserReigisSurePass,
            etUserPhone;
    private Button butSendCode;
    private boolean isSend = false;

    EventHandler eventHandler = new EventHandler() {
        public void afterEvent(int event, int result, Object data) {
            // afterEvent会在子线程被调用，因此如果后续有UI相关操作，需要将数据发送到UI线程
            Message msg = new Message();
            msg.arg1 = event;
            msg.arg2 = result;
            msg.obj = data;
            new Handler(Looper.getMainLooper(), new Handler.Callback() {
                @Override
                public boolean handleMessage(Message msg) {
                    int event = msg.arg1;
                    int result = msg.arg2;
                    Object data = msg.obj;
                    if (result == SMSSDK.RESULT_COMPLETE) {
                        //回调完成
                        if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {
                            //提交验证码成功
                            Logger.i("EventHandler", "提交验证码成功");
                            String Md5pass = Md5Util.mD5(strUserReigisPass);
                            persenter.userReigis(strUserReigisMailbox, Md5pass);
                        } else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE) {
                            //获取验证码成功
                            Logger.i("EventHandler", "获取验证码成功");
                        } else if (event == SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES) {
                            //返回支持发送验证码的国家列表
                            Logger.i("EventHandler", "返回支持发送验证码的国家列表");
                        }
                    } else {
                        ((Throwable) data).printStackTrace();
                        Logger.i("EventHandler", "回调失败");
                    }

                    return false;
                }
            }).sendMessage(msg);
        }
    };
    private String strUserReigisMailbox;
    private String strUserReigisPass;


    @Override
    public void initData() {
        super.initData();
        //设置标题
        setTitle("登录");
        //初始化控件
        initWight();
    }

    /**
     * 初始化控件
     */
    private void initWight() {
        //公共部分
        SMSSDK.setAskPermisionOnReadContact(true);
        txtLogin = (TextView) getView(R.id.acc_txt_login);
        txtReigis = (TextView) getView(R.id.acc_txt_register);
        prLogin = (PercentRelativeLayout) getView(R.id.acc_incliud_login);
        prReigis = (PercentRelativeLayout) getView(R.id.acc_incliud_register);
        viewLinLeft = (View) getView(R.id.acc_view_linleft);
        viewLinRight = (View) getView(R.id.acc_view_linright);
        //登录页面的控件
        etUserLoginMailbox = (EditText) getView(R.id.acc_et_mailbox);
        etUserLoginPass = (EditText) getView(R.id.acc_input_pass);
        //注册页面的控件
        etUserReigisMailbox = (EditText) getView(R.id.acc_et_reg_mailbox);
        etUserReigisPass = (EditText) getView(R.id.acc_inputreg_surepass);
        etUserVerifier = (EditText) getView(R.id.ac_etreg_verifier);
        etUserReigisSurePass = (EditText) getView(R.id.acc_inputreg_surepass);
        etUserPhone = (EditText) getView(R.id.acc_inputreg_phone);
        butSendCode = (Button) getView(R.id.acc_btn_send_code);
        //设置点击事件
        setClick(this, R.id.acc_txt_login, R.id.acc_txt_register, R.id.acc_btn_login, R.id.acc_btn_send_code, R.id.acc_btn_reigis);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public M createModel() {
        return (M) new ActivityLoginModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        persenter = new ActivityLoginPersenter();
        persenter.initContext(this);
        return (P) persenter;
    }

    /**
     * 展示登录
     */
    public void showLogin() {
        txtLogin.setTextColor(Color.parseColor("#22C5AA"));
        txtReigis.setTextColor(Color.WHITE);
        prLogin.setVisibility(View.VISIBLE);
        prReigis.setVisibility(View.GONE);
        viewLinLeft.setVisibility(View.VISIBLE);
        viewLinRight.setVisibility(View.GONE);
    }

    /**
     * 展示注册
     */
    public void showRegister() {
        txtLogin.setTextColor(Color.WHITE);
        txtReigis.setTextColor(Color.parseColor("#22C5AA"));
        prLogin.setVisibility(View.GONE);
        prReigis.setVisibility(View.VISIBLE);
        viewLinLeft.setVisibility(View.GONE);
        viewLinRight.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.acc_txt_login) {
            showLogin();

        } else if (i == R.id.acc_txt_register) {
            showRegister();

        } else if (i == R.id.acc_btn_login) {
            userLogin();

        } else if (i == R.id.acc_btn_reigis) {
            userReigis();

        } else if (i == R.id.acc_btn_send_code) {
            if (isSend == true) {
                return;
            }
            sendCode();
        }
    }


    /**
     * 发送code
     */
    private void sendCode() {

        String strUserPhone = etUserPhone.getText().toString().trim();
        if (!Validator.isMobile(strUserPhone)) {
            toase("请输入正确的手机号", 1);
            return;
        }
        // 注册一个事件回调，用于处理SMSSDK接口请求的结果
        SMSSDK.registerEventHandler(eventHandler);
        // 请求验证码，其中country表示国家代码，如“86”；phone表示手机号码，如“13800138000”
        isSend = true;
        SMSSDK.getVerificationCode("86", strUserPhone);// 提交验证码，其中的code表示验证码，如“1357”
/*        int min = 1000;
        int max = 9999;
        Random random = new Random();
        int num = random.nextInt(max) % (max - min + 1) + min;
        code = num;*/
        countimer.start();
    }


    /**
     * 60秒延迟
     */
    private CountDownTimer countimer = new CountDownTimer(60 * 1000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            // TODO Auto-generated method stub
            butSendCode.setText(millisUntilFinished + "s");
            butSendCode.setText(millisUntilFinished / 1000 + "秒");
        }

        @Override
        public void onFinish() {
            butSendCode.setText("从新发送");
            isSend = false;
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        SMSSDK.unregisterEventHandler(eventHandler);
    }

    /**
     * 注册方法
     */
    private void userReigis() {
        strUserReigisMailbox = etUserReigisMailbox.getText().toString().trim();
        strUserReigisPass = etUserReigisPass.getText().toString().trim();
        String strUserReigisSurePass = etUserReigisSurePass.getText().toString().trim();
        String strUserVerifier = etUserVerifier.getText().toString().trim();
        String strUserPhone = etUserPhone.getText().toString().trim();
        if (!Validator.isMobile(strUserPhone)) {
            toase("请输入正确的手机号", 1);
            return;
        }
        //判断是否为空
        if (TextUtils.isEmpty(strUserReigisMailbox) || TextUtils.isEmpty(strUserReigisPass) || TextUtils.isEmpty(strUserReigisSurePass) || TextUtils.isEmpty(strUserVerifier)) {
            toase("请填写完整信息", 1);
            return;
        }

        //正则表达式判读邮箱是否正确
        if (!Validator.isEmail(strUserReigisMailbox)) {
            toase("请输入正确的邮箱", 1);
            return;
        }

        //判断两次密码是否相同
        if (!strUserReigisPass.equals(strUserReigisSurePass)) {
            toase("密码不一致", 1);
            return;
        }

        //发送验证码
        SMSSDK.submitVerificationCode("86", strUserPhone, strUserVerifier);
    }

    /**
     * 登录方法
     */
    private void userLogin() {
        //获取邮箱密码
        String strUserLoginMailbox = etUserLoginMailbox.getText().toString().trim();
        String strUserPass = etUserLoginPass.getText().toString().trim();

        //判断是否为空

        if (TextUtils.isEmpty(strUserLoginMailbox) || TextUtils.isEmpty(strUserPass)) {
            toase("用户名不能为空", 1);
            return;
        }

        //正则表达式判读邮箱是否正确
        if (!Validator.isEmail(strUserLoginMailbox)) {
            toase("请输入正确的邮箱", 1);
            return;
        }
        String Md5pass = Md5Util.mD5(strUserPass);
        persenter.userLogin(strUserLoginMailbox, Md5pass);

    }


    /**
     * 登录回调
     *
     * @param data
     */
    @Override
    public void backUserLogin(String data) {
        UserEntity userEntity = new Gson().fromJson(data, UserEntity.class);
        if (userEntity.getStatus() == 0) {
            SpUtil.saveData(this, "userName", userEntity.getUser_name());
            SpUtil.saveData(this, "userTime", userEntity.getUser_time());
            SpUtil.saveData(this, "userNickname", userEntity.getUser_nickname());
            SpUtil.saveData(this, "userMail", userEntity.getUser_mail());
            SpUtil.saveData(this, "userAddress", userEntity.getUser_address());
            SpUtil.saveData(this, "userStar", userEntity.getUser_star());
            SpUtil.saveData(this, "userMoney", userEntity.getUser_money());
            SpUtil.saveData(this, "userMoneyNum", userEntity.getUser_money_num());
            SpUtil.saveData(this, "userVip", userEntity.getUser_vip());
            SpUtil.saveData(this, "userFor", userEntity.getUser_for());
            SpUtil.saveData(this, "userLogo", userEntity.getUser_logo());
            SpUtil.saveData(this, "userId", userEntity.getUser_id());
            SpUtil.saveData(this, "userAge", userEntity.getUser_age());
            SpUtil.saveData(this, "userPhone", userEntity.getUser_phone());
            SpUtil.saveData(this, "userCompany", userEntity.getUser_company());
            SpUtil.saveData(this, "userSex", userEntity.getUser_sex());
            SpUtil.saveData(this, "userWb", userEntity.getUser_wb());
            SpUtil.saveData(this, "isLogin", true);
            finish();
            return;
        }
        toase("登录失败", 1);
        /**
         * status : 0
         * z : 1206521225@qq.com
         * user_pass : 123456
         * user_time : 2018-12-17 11:00:52
         * user_nickname : 用户**12
         * user_sex :
         * user_mail : 1206521225@qq.com1
         * user_address :1
         * user_star :1
         * user_money :
         * user_money_num :
         * user_desc :
         * user_logo :
         * user_wx :
         * user_id : abner12065
         * user_wb :
         * user_phone :
         * user_vip : 1
         * user_company :
         * user_occupation :
         * user_age :
         * user_tznum :
         * user_tzji :
         * user_tzdeng :
         * user_tzfen :
         * user_for : 123456
         */
    }

    /**
     * 注册回调
     *
     * @param data
     */
    @Override
    public void backUserReigis(String data) {
        RegisterEntity registerEntity = new Gson().fromJson(data, RegisterEntity.class);
        Logger.d("Tagger", data);
        if (registerEntity.getStatus() == 0) {
            showLogin();
            toase("注册成功", 1);
            destoryRegister();
        } else {
            toase("注册失败", 1);
        }
    }

    /**
     * 赋空
     */
    public void destoryRegister() {
        etUserReigisMailbox.setText("");
        etUserReigisPass.setText("");
        etUserReigisSurePass.setText("");
        etUserVerifier.setText("");
        etUserPhone.setText("");
        butSendCode.setText("获取验证码");
        isSend = false;
    }
}