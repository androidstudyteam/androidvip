package com.firsttream.account.activitys;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.firsttream.account.R;
import com.firsttream.account.entity.RegisterEntity;
import com.firsttream.account.mvp.model.ActivityMassageModel;
import com.firsttream.account.mvp.persenter.ActivityMassagePersenter;
import com.firsttream.account.utils.Pop;
import com.firsttream.account.utils.PopWindowUtils;
import com.firsttream.account.utils.ShowDialog;
import com.firsttream.account.mvp.view.ActivityMassageView;
import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.SpUtil;
import com.firsttream.baselibrary.util.Validator;
import com.google.gson.Gson;
import com.lljjcoder.citypickerview.widget.CityPicker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 作者：周建峰
 * 时间：2018-12-19
 *
 * @param <M>
 * @param <V>
 * @param <P>
 */
@RequiresApi(api = Build.VERSION_CODES.N)
public class ActivityMassage<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> implements View.OnClickListener, ActivityMassageView {
    private ImageView smUserImage, ivSex;
    private Context mContext;
    private ActivityMassagePersenter mPersenter;
    private TextView txtUserName, txtUserTime, txtUserAddress, txtUserAge, txtUserPhone, txtUserCompany, txtUserSex, txtUserWb;
    private CityPicker cityPack;
    private String strCity;
    private String nickName;
    private String userAge;
    private String userCompany;
    private String userPhone;
    private int mScreenWidth;
    private int mScreenHeight;
    private String content;
    private List<String> sexList = new ArrayList<>();
    private String userSex;
    private TextView txtSignNum;
    private TextView txtAttentionme;
    private TextView txtAttentionOuther;
    private String pic = "head.png";

    @Override
    public void initContext(Context context) {
        super.initContext(context);
        this.mContext = context;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_massage;
    }

    @Override
    public void initData() {
        super.initData();
        setTitle("个人信息");
        //初始化控件
        initWeight();
    }

    /**
     * 获取是否登录
     *
     * @return
     */
    public boolean isLogin() {
        boolean isLogin = (boolean) SpUtil.getSpData(mContext, "isLogin", false);
        return isLogin;
    }

    /**
     * 初始化控件
     */
    public void initWeight() {
        //用户头像
        smUserImage = (ImageView) getView(R.id.sm_userimage);
        //男女图片
        ivSex = (ImageView) getView(R.id.iv_sex);
        //用户名
        txtUserName = (TextView) getView(R.id.txt_user_name);
        //生日
        txtUserTime = (TextView) getView(R.id.txt_user_time);
        //住址
        txtUserAddress = (TextView) getView(R.id.txt_user_address);
        //年龄
        txtUserAge = (TextView) getView(R.id.txt_user_age);
        //手机号
        txtUserPhone = (TextView) getView(R.id.txt_user_phone);

        //公司
        txtUserCompany = (TextView) getView(R.id.txt_user_complay);
        //男女
        txtUserSex = (TextView) getView(R.id.txt_user_sex);
        //关联微博
        txtUserWb = (TextView) getView(R.id.txt_user_wb);
        //签到数量
        txtSignNum = (TextView)getView(R.id.txt_sign_num);
        //我关注的
        txtAttentionme = (TextView)getView(R.id.txt_attentionme);
        //关注我的
        txtAttentionOuther = (TextView)getView(R.id.txt_AttentionOuther);
        //设置点击事件
        setClick(this, R.id.rl_address, R.id.txt_user_name, R.id.rl_age, R.id.rl_phone, R.id.rl_company, R.id.rl_sex, R.id.rl_wb,R.id.sm_userimage);
        cityPack = new CityPicker.Builder(mContext).build();
        cityPack.setType(CityPicker.DEFAULT_TEXT_COLOR);
        cityPack.setOnCityItemClickListener(new CityPicker.OnCityItemClickListener() {
            @Override
            public void onSelected(String... citySelected) {

                //省份
                String province = citySelected[0];
                //城市
                String city = citySelected[1];
                //区县
                String district = citySelected[2];
                //邮编
                strCity = province + "-" + city + "-" + district;
                //Toast.makeText(mContext, province + "-" + city + "-" + district, Toast.LENGTH_LONG).show();
                mPersenter.updataAddress(getUserName(), strCity);
            }

            @Override
            public void onCancel() {

            }
        });
        sexList.add("男");
        sexList.add("女");
    }

    /**
     * 获取用户名
     */


    public String getUserName() {
        String username = (String) SpUtil.getSpData(mContext, "userName", "");
        return username;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isLogin()) {
            return;
        }
        setUserMessage();
    }

    /**
     * 设置用户信息
     */
    private void setUserMessage() {
        //获取昵称
        String nickName = (String) SpUtil.getSpData(mContext, "userNickname", "");
        String userTime = (String) SpUtil.getSpData(mContext, "userTime", "");
        String userAddress = (String) SpUtil.getSpData(mContext, "userAddress", "");
        String userAge = (String) SpUtil.getSpData(mContext, "userAge", "");
        String userPhone = (String) SpUtil.getSpData(mContext, "userPhone", "");
        String userCompany = (String) SpUtil.getSpData(mContext, "userCompany", "");
        String userWb = (String) SpUtil.getSpData(mContext, "userWb", "");
        String userSex = (String) SpUtil.getSpData(mContext, "userSex", "");
        String singNum = (String) SpUtil.getSpData(mContext,"singNum","");
        String attentionOuther = (String) SpUtil.getSpData(mContext,"attentionOuther","");
        String attentionMe = (String) SpUtil.getSpData(mContext,"attentionMe","");
        //获取登录用户头像attentionMe
        String userLogo = (String) SpUtil.getSpData(mContext, "userLogo", "");

        if ("".equals(userLogo)) {
            Glide.with(mContext).load(R.drawable.user_image).apply(RequestOptions.bitmapTransform(new CircleCrop())).into(smUserImage);
        } else {
            Glide.with(mContext).load(userLogo).apply(RequestOptions.bitmapTransform(new CircleCrop())).into(smUserImage);
        }
        if (!"".equals(nickName)) {
            txtUserName.setText(nickName);
        } else {
            txtUserName.setText("请设置昵称");
        }

        if (!"".equals(userTime)) {
            txtUserTime.setText(userTime);
        } else {
            txtUserTime.setText("请设置生日");
        }

        if (!"".equals(userAddress)) {
            txtUserAddress.setText(userAddress);
        } else {
            txtUserAddress.setText("请设置地址");
        }

        if (!"".equals(userAge)) {
            txtUserAge.setText(userAge);
        } else {
            txtUserAge.setText("请设置年龄");
        }

        if (!"".equals(attentionMe)) {
            txtAttentionme.setText(attentionMe);
        } else {
            txtAttentionme.setText("0");
        }

        if (!"".equals(attentionOuther)) {
            txtAttentionOuther.setText(attentionOuther);
        } else {
            txtAttentionOuther.setText("0");
        }

        if (!"".equals(userPhone)) {
            txtUserPhone.setText(userPhone);
        } else {
            txtUserPhone.setText("请设置手机号");
        }


        if (!"".equals(userCompany)) {
            txtUserCompany.setText(userCompany);
        } else {
            txtUserCompany.setText("请设置公司");
        }

        if (!"".equals(userWb)) {
            txtUserWb.setText(userWb);
        } else {
            txtUserWb.setText("立刻关联");
        }

        if (!"".equals(singNum)){
            txtSignNum.setText("已签到"+singNum+"天");
        }else{
            txtSignNum.setText("暂没有签到");
        }

        if (!"".equals(userSex)) {
            txtUserSex.setText(userSex);

            if (userSex.equals("男")) {
                ivSex.setImageResource(R.drawable.pc_min);
            } else {
                ivSex.setImageResource(R.drawable.pc_womain);
            }
        } else {
            txtUserSex.setText("设置性别");
        }

    }

    @Override
    public M createModel() {
        return (M) new ActivityMassageModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new ActivityMassagePersenter();
        mPersenter.initContext(this);
        return (P) mPersenter;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.rl_address) {
            cityPack.show();
        } else if (i == R.id.txt_user_name) {
            alertNickName();
        } else if (i == R.id.rl_age) {
            alertAge();
        } else if (i == R.id.rl_phone) {
            alertPhone();
        } else if (i == R.id.rl_company) {
            alertCompany();
        } else if (i == R.id.rl_sex) {
            alertSex();
        } else if (i == R.id.rl_wb) {

        }else if(i==R.id.sm_userimage){
            //点击头像弹出popupwindow方法
            pop();
        }
    }

    //弹出popupwindow的方法
    private void pop() {
        //new 调用pop工具类 括号里传上下文和本页面布局整体的id new 接口提示
        new Pop(this, smUserImage, new Pop.OnSelectPictureListener() {
            @Override
            public void onTakePhoto() {
                //new 意图跳转相机 mediastore .相机
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //意图传值mediastor .ex  ,uri.fromfile括号new文件传获取系统路径传文件名
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Environment.getExternalStorageDirectory(), pic)));
                //回调方法跳转相机强转本页面0 回调方法写在Activity 调用方法生成过来
                startActivityForResult(intent, 0);

            }

            @Override
            public void onSelectPicture() {
                //new 意图intent跳转相册
                Intent intent = new Intent(Intent.ACTION_PICK, null);
                //意图设置dataAnd Type mediastore images.media .ex ,imgae/*通配符
                intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                //带值跳转1
               startActivityForResult(intent, 1);

            }

            @Override
            public void onCancel() {
                //取消方法不用写
            }
        });
    }


    /**
     * 设置性别
     */
    private void alertSex() {
        mScreenWidth = getWindowManager().getDefaultDisplay().getWidth();
        mScreenHeight = getWindowManager().getDefaultDisplay().getHeight();
        new PopWindowUtils(mContext, mScreenWidth, mScreenHeight).setTitle("修改性別").setListener(new PopWindowUtils.BackDataListener() {
            @Override
            public void backData(String data) {
                userSex = data;
                mPersenter.updataSex(getUserName(),data);
            }
        }).show(sexList);
    }

    /**
     * 修改年龄
     */
    private void alertAge() {
        new ShowDialog.Builder(mContext)
                .setTitle("修改年龄")
                .setListener(new ShowDialog.OnBackContentListener() {
                    @Override
                    public void backContent(String data) {
                        userAge = data;
                        if (Validator.isAge(data)) {
                            mPersenter.updataAge(getUserName(), data);
                        } else {
                            toase("请输入正确的年龄", 0);
                        }
                    }
                })
                .create()
                .show();
    }

    /**
     * 手机号
     */
    private void alertPhone() {
        new ShowDialog.Builder(mContext)
                .setTitle("修改手机号")
                .setListener(new ShowDialog.OnBackContentListener() {
                    @Override
                    public void backContent(String data) {
                        userPhone = data;
                        if (Validator.isMobile(data)) {
                            mPersenter.updataPhone(getUserName(), data);
                        } else {
                            toase("请输入正确的手机号", 0);
                        }
                    }
                })
                .create()
                .show();
    }


    /**
     * 修改公司
     */
    private void alertCompany() {
        new ShowDialog.Builder(mContext)
                .setTitle("修改公司")
                .setListener(new ShowDialog.OnBackContentListener() {
                    @Override
                    public void backContent(String data) {
                        userCompany = data;
                        mPersenter.updataCompany(getUserName(), data);
                    }
                })
                .create()
                .show();
    }


    /**
     * 修改昵称
     */
    private void alertNickName() {
        new ShowDialog.Builder(mContext)
                .setTitle("修改昵称")
                .setListener(new ShowDialog.OnBackContentListener() {
                    @Override
                    public void backContent(String data) {
                        nickName = data;
                        if (Validator.isPassword(data) || Validator.isChinese(data)) {
                            mPersenter.updataNickName(getUserName(), data);
                        } else {
                            toase("不要输入非法字符", 0);
                        }
                    }
                })
                .create()
                .show();
    }

    /**
     * 修改昵称的回调
     *
     * @param data
     */

    @Override
    public void getUpdataNickName(String data) {
        RegisterEntity registerEntity = new Gson().fromJson(data, RegisterEntity.class);
        if (registerEntity.getStatus() == 0) {
            toase("修改成功", 1);
            txtUserName.setText(nickName);
            SpUtil.saveData(mContext, "userNickname", nickName);
            return;
        }
        toase("修改失败", 1);
    }

    /**
     * 修改地址的回调
     *
     * @param data
     */
    @Override
    public void getUpdataAddress(String data) {
        RegisterEntity registerEntity = new Gson().fromJson(data, RegisterEntity.class);
        if (registerEntity.getStatus() == 0) {
            toase("修改成功", 1);
            txtUserAddress.setText(strCity);
            SpUtil.saveData(mContext, "userAddress", strCity);
            return;
        }
        toase("修改失败", 1);
    }


    /**
     * 修改年龄的回调
     *
     * @param data
     */
    @Override
    public void getUpdataAge(String data) {
        RegisterEntity registerEntity = new Gson().fromJson(data, RegisterEntity.class);
        if (registerEntity.getStatus() == 0) {
            toase("修改成功", 0);
            txtUserAge.setText(userAge);
            SpUtil.saveData(mContext, "userAge", userAge);
            return;
        }
        toase("修改失败", 0);
    }

    @Override
    public void getUpdataPhone(String data) {
        RegisterEntity registerEntity = new Gson().fromJson(data, RegisterEntity.class);
        if (registerEntity.getStatus() == 0) {
            toase("修改成功", 0);
            txtUserPhone.setText(userPhone);
            SpUtil.saveData(mContext, "userPhone", userPhone);
            return;
        }
        toase("修改失败", 0);
    }

    @Override
    public void getUpdataCompany(String data) {
        RegisterEntity registerEntity = new Gson().fromJson(data, RegisterEntity.class);
        if (registerEntity.getStatus() == 0) {
            toase("修改成功", 0);
            txtUserCompany.setText(userCompany);
            SpUtil.saveData(mContext, "userCompany", userCompany);
            return;
        }
        toase("修改失败", 0);
    }

    @Override
    public void getUpdataSex(String data) {
        RegisterEntity registerEntity = new Gson().fromJson(data, RegisterEntity.class);
        if (registerEntity.getStatus() == 0) {
            toase("修改成功", 0);
            txtUserSex.setText(userSex);
            SpUtil.saveData(mContext, "userSex", userSex);
            if ("男".equals(userSex)){
                ivSex.setImageResource(R.drawable.pc_min);
            }else{
                ivSex.setImageResource(R.drawable.pc_womain);
            }
            return;
        }
        toase("修改失败", 0);
    }

    @Override
    public void getUpdataWb(String data) {

    }
}