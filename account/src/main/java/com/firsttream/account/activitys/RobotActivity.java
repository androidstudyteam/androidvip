package com.firsttream.account.activitys;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firsttream.account.R;
import com.firsttream.account.adapter.ChatMessageAdapter;
import com.firsttream.account.entity.RobotBean;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.baselibrary.util.OkHttpUtiles;
import com.firsttream.baselibrary.util.UltimateBar;
import com.google.gson.Gson;
import com.scwang.smartrefresh.header.WaterDropHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.FalsifyFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 作者:高佳宝
 * 日期:2018/12/19
 * 作用:显示机器人页面的Activity RobotActivity
 */
//先是泛型分贝继承这个类的m  v  p 在继承baseActivity 在实现自己的view接口
public class RobotActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnsend;
    private EditText etsendText;
    private ListView robotlv;
    private ImageView ivback;
    private TextView txttitle;
    private ChatMessageAdapter chatMessageAdapter;
    private List<RobotBean> robotBeanList = new ArrayList<>();
    private long currentTime;
    private long oldTime;
    private String[] welcome_arry;
    private TextView tesendText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_robot);
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        //沉浸式
        UltimateBar.newImmersionBuilder().applyNav(false)
                .build(this).apply();
        //初始化控件
        initView();
        // 如果屏幕上的对话数据多于30，则移除前面的数据
        if (robotBeanList.size() > 30) {
            for (int i = 0; i < robotBeanList.size(); i++) {
                robotBeanList.remove(i);
            }
        }
    }

    //初始化控件
    private void initView() {
        btnsend = (Button) findViewById(R.id.btn_send);
        etsendText = (EditText) findViewById(R.id.et_sendText);
        tesendText = (TextView) findViewById(R.id.te_sendText);
        robotlv = (ListView) findViewById(R.id.robot_lv);
        ivback = (ImageView) findViewById(R.id.iv_back);
        txttitle = (TextView) findViewById(R.id.txt_title);
        txttitle.setText("机器人客服");
        ivback.setOnClickListener(this);
        btnsend.setOnClickListener(this);
        tesendText.setOnClickListener(this);
        chatMessageAdapter = new ChatMessageAdapter(this);
        robotlv.setAdapter(chatMessageAdapter);
        //获取欢迎语
        String randomWelcomeTips = getRandomWelcomeTips();
        RobotBean robotBean1 = new RobotBean();
        robotBean1.setText(randomWelcomeTips);
        robotBean1.setTime(getTime());
        robotBean1.setFlag(RobotBean.RECEIVE);
        robotBeanList.add(robotBean1);
        chatMessageAdapter.setLists(robotBeanList);
        RefreshLayout refreshLayout = (RefreshLayout)findViewById(R.id.refreshLayout);
        //设置 Header 为 贝塞尔雷达 样式
        refreshLayout.setRefreshHeader(new WaterDropHeader(this));
        //设置 Footer 为 球脉冲 样式
        refreshLayout.setRefreshFooter(new FalsifyFooter(this));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(1000/*,false*/);//传入false表示刷新失败
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                refreshlayout.finishLoadMore(false);//传入false表示加载失败
            }
        });
    }

    //获取欢迎语
    public String getRandomWelcomeTips() {
        String welcome_tip = null;
        welcome_arry = this.getResources().getStringArray(R.array.welcome_tips);// 从string.xml中获取名为welcome_tips的字符串数组
        int index = (int) (Math.random() * (welcome_arry.length - 1));// 获取一个随机数
        welcome_tip = welcome_arry[index];
        return welcome_tip;
    }

    //点击事件
    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btn_send) {
            String trim = etsendText.getText().toString().trim();
            if (TextUtils.isEmpty(trim)) {
                toast("不能发送空内容哦~");
                return;
            }
            etsendText.setText("");//附空值
            String dropk = trim.replace(" ", "");// 去掉空格
            String droph = dropk.replace("\n", "");// 去掉回车
            RobotBean robotBean1 = new RobotBean();
            robotBean1.setText(droph);
            robotBean1.setTime(getTime());
            robotBean1.setFlag(RobotBean.SEND);
            robotBeanList.add(robotBean1);
            chatMessageAdapter.setLists(robotBeanList);
            //网络请求
            dohttp(droph);
        } else if (i == R.id.iv_back) {//返回键
            finish();
        } else if (i == R.id.te_sendText) {//点击输入框
//            tesendText.setVisibility(View.GONE);
//            etsendText.setVisibility(View.VISIBLE);
        }
    }

    //网络请求
    private void dohttp(String trim) {
        new OkHttpUtiles().get(Http.TULING_ROBOTURL + trim + "&userid=366918").result(new OkHttpUtiles.HttpListener() {
            @Override
            public void success(String data) {
                //添加
                RobotBean robotBean = new Gson().fromJson(data, RobotBean.class);
                RobotBean robotBean1 = new RobotBean();
                robotBean1.setText(robotBean.getText());
                robotBean1.setTime(getTime());
                robotBean1.setFlag(RobotBean.RECEIVE);
                robotBeanList.add(robotBean1);
                chatMessageAdapter.setLists(robotBeanList);
            }

            @Override
            public void errror() {
                toast("网络好像超时了~");
            }
        });
    }

    //吐司方法
    private void toast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    /**
     * 设置时间
     *
     * @return
     */
    private String getTime() {
        currentTime = System.currentTimeMillis();
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日   HH:mm:ss");
        SimpleDateFormat format1 = new SimpleDateFormat("HH:mm:ss");
        Date curDate = new Date();
        String str = format.format(curDate);
        String str1 = format1.format(curDate);
        if (currentTime - oldTime >= 5 * 60 * 1000) {// 如果超过5分钟，显示时间
            oldTime = currentTime;
            return str;
        }
        if (currentTime - oldTime >= 30*1000) {// 如果超过30秒，显示时间
            oldTime = currentTime;
            return str1;
        } else {
            return "";
        }
    }

//    //隐藏键盘的方法
//    public static void hideSystemKeyboard(Context context, View view) {
//        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//    }
//    //显示键盘的方法
//    public static void showSystemKeyboard(Context context, View view) {
//        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(view, 0);
//    }


}
