package com.firsttream.account.activitys;

import com.firsttream.account.R;
import com.firsttream.account.entity.DayEntity;
import com.firsttream.account.mvp.model.ActivitySignModel;
import com.firsttream.account.mvp.persenter.ActivitySignPersenter;
import com.firsttream.account.mvp.view.ActivitySignView;
import com.firsttream.account.sing.OnSignedSuccess;
import com.firsttream.account.sing.SignDate;
import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.SpUtil;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActivitySign<M extends IContract.Model,V extends IContract.View,P extends BasePersenter> extends BaseActivity<M,V,P> implements ActivitySignView{

    private SignDate signDate;
    private ActivitySignPersenter mPersenter;
    private List<String> nowDatas = new ArrayList<>();
    @Override
    protected int getLayoutId() {
        return R.layout.activity_sign;
    }

    @Override
    public void initData() {
        super.initData();
        setTitle("签到");
        signDate = (SignDate) findViewById(R.id.sing_data);
        signDate.setOnSignedSuccess(new OnSignedSuccess() {
            @Override
            public void OnSignedSuccess() {
                SimpleDateFormat mSimpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
                String mDay = mSimpleDateFormat.format(new Date());
                mPersenter.getIsSign(getUserId(),mDay);
            }
        });
        mPersenter.getQuerySign(getUserId());
    }


    /**
     * 获取用户id
     * @return
     */
    public  String  getUserId(){
        String userid = (String) SpUtil.getSpData(this,"userId","");
        return userid;
    }

    /**
     * 获取昵称
     * @return
     */
    public String getUserNick(){
        String userNick= (String) SpUtil.getSpData(this,"userNickname","");
        return userNick;
    }

    @Override
    public M createModel() {
        return (M) new ActivitySignModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new ActivitySignPersenter();
        mPersenter.initContext(this);
        return (P) mPersenter;
    }

    @Override
    public void getIsSign(String data) {

        DayEntity dayEntity = new Gson().fromJson(data, DayEntity.class);
        if (dayEntity.getStatus() == 0){
            toase("已经关注",0);
            signDate.notfiyChange();
        }else{
            mPersenter.getAddSign(getUserId(),getPackageName());
        }
    }

    @Override
    public void getAddSign(String data) {
        DayEntity dayEntity = new Gson().fromJson(data, DayEntity.class);
        if (dayEntity.getStatus() == 0){
            toase("关注成功",0);
            mPersenter.getQuerySign(getUserId());
        } else {
            toase("关注失败",0);
            signDate.notfiyChange();
        }
    }

    @Override
    public void getQuerySign(String data) {
        nowDatas.clear();
        DayEntity dayEntity = new Gson().fromJson(data, DayEntity.class);
        if (dayEntity.getStatus() == 0){
            List<DayEntity.ItemsBean> items = dayEntity.getItems();
            for (int i = 0; i <items.size(); i ++){
                nowDatas.add(items.get(i).getUser_day());
            }
            SpUtil.saveData(this,"singNum",nowDatas.size()+"");
        }

        signDate.setList(nowDatas);
    }
}