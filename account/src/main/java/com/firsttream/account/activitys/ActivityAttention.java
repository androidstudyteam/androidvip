package com.firsttream.account.activitys;

import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firsttream.account.R;
import com.firsttream.account.adapter.AttentionAdapter;
import com.firsttream.account.entity.AttentionForMeBean;
import com.firsttream.account.mvp.model.ActivityAttentionModel;
import com.firsttream.account.mvp.persenter.ActivityAttentionPersenter;
import com.firsttream.account.mvp.view.ActivityAttentionView;
import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.baselibrary.util.SpUtil;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

public class ActivityAttention<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> implements ActivityAttentionView {
    private ActivityAttentionPersenter mPersenter;
    private RelativeLayout includeRelativ;
    private XRecyclerView mRecyAttentionView;
    private AttentionAdapter attentionAdapter;


    @Override
    public void initData() {
        super.initData();
        setTitle("我关注的");
        //初始化控件
        initWeight();
        //网络请求
        doHttp();
    }

    public void doHttp(){
        String userId = (String) SpUtil.getSpData(this, "userId", "");
        Logger.d("Tagger",userId);
        mPersenter.getAttentionFoeMe(userId);
    }

    public void initWeight(){
        TextView txtContext = (TextView) getView(R.id.txt_show);

        //初始化控件
        includeRelativ = (RelativeLayout)getView(R.id.include_user);
        mRecyAttentionView = (XRecyclerView)getView(R.id.recy_attention_me);
        //初始化適配器
        attentionAdapter = new AttentionAdapter(this);
         //初始化管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyAttentionView.setLayoutManager(linearLayoutManager);
        mRecyAttentionView.setAdapter(attentionAdapter);
        //设置上拉刷新下拉加载
        mRecyAttentionView.setRefreshProgressStyle(ProgressStyle.BallSpinFadeLoader);
        mRecyAttentionView.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                doHttp();
                mRecyAttentionView.refreshComplete();
            }

            @Override
            public void onLoadMore() {
                mRecyAttentionView.refreshComplete();
            }
        });
        //設置初始數據
        txtContext.setText("您还没有关注别人呢");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_attention;
    }

    @Override
    public M createModel() {
        return (M) new ActivityAttentionModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new ActivityAttentionPersenter();
        mPersenter.initContext(this);
        return (P) mPersenter;
    }

    @Override
    public void getAttentionForMe(String data) {
        AttentionForMeBean attentionForMeBean = new Gson().fromJson(data, AttentionForMeBean.class);
        if (attentionForMeBean.getStatus() == 0) {
            includeRelativ.setVisibility(View.GONE);
            attentionAdapter.setList(attentionForMeBean.getItems());
            SpUtil.saveData(this,"attentionMe",attentionForMeBean.getItems().size()+"");
            return;
        }
        includeRelativ.setVisibility(View.VISIBLE);
    }
}