package com.firsttream.account.activitys;

import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firsttream.account.R;
import com.firsttream.account.adapter.AttentionAdapter;
import com.firsttream.account.entity.AttentionForMeBean;
import com.firsttream.account.mvp.model.ActivityShareModel;
import com.firsttream.account.mvp.persenter.ActivitySharePersenter;
import com.firsttream.account.mvp.view.ActivityShareViwe;
import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.baselibrary.util.SpUtil;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

public class ActivityShare<M extends IContract.Model,V extends IContract.View, P extends BasePersenter> extends BaseActivity<M,V,P> implements ActivityShareViwe{

    private ActivitySharePersenter mPersenter;
    private RelativeLayout includeRelativ;
    private XRecyclerView mRecyAttentionView;
    private AttentionAdapter attentionAdapter;

    @Override
    public void initData() {
        super.initData();
        setTitle("我的分享");
        //初始化控件
        initWeight();
        //网络请求
        doHttp();
    }

    @Override
    protected int getLayoutId() {
        return  R.layout.activity_share;
    }

    @Override
    public M createModel() {
        return (M) new ActivityShareModel();
    }



    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new ActivitySharePersenter();
        mPersenter.initContext(this);
        return (P) mPersenter;
    }

    public void initWeight() {
        TextView txtContext = (TextView) getView(R.id.txt_mecontent);
        txtContext.setText("还没有分享有呦，快分享吧");
        //初始化控件
        includeRelativ = (RelativeLayout)getView(R.id.rel_not);
        mRecyAttentionView = (XRecyclerView)getView(R.id.recy_attention_me);
        //初始化適配器
        attentionAdapter = new AttentionAdapter(this);
        //初始化管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyAttentionView.setLayoutManager(linearLayoutManager);
        mRecyAttentionView.setAdapter(attentionAdapter);
        //设置上拉刷新下拉加载
        mRecyAttentionView.setRefreshProgressStyle(ProgressStyle.BallScaleMultiple);
        mRecyAttentionView.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                doHttp();
                mRecyAttentionView.refreshComplete();
            }

            @Override
            public void onLoadMore() {
                mRecyAttentionView.refreshComplete();
            }
        });
    }

    /**
     * 网络请求
     */
    public void doHttp(){
        String userId = (String) SpUtil.getSpData(this, "userId", "");
        mPersenter.getAttentionShare(userId);
    }

    @Override
    public void getShareFoMe(String data) {
        AttentionForMeBean attentionForMeBean = new Gson().fromJson(data, AttentionForMeBean.class);
        if (attentionForMeBean.getStatus() == 0) {
            includeRelativ.setVisibility(View.GONE);
            attentionAdapter.setList(attentionForMeBean.getItems());
            return;
        }
        includeRelativ.setVisibility(View.VISIBLE);
    }
}
