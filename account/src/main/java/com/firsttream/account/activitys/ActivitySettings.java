package com.firsttream.account.activitys;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.firsttream.account.R;
import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.SpUtil;

public class ActivitySettings<M extends IContract.Model ,V extends IContract.View, P extends BasePersenter> extends BaseActivity<M,V,P> implements View.OnClickListener {

    private ImageView userImage;
    private TextView txtUserName;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_settings;
    }

    @Override
    public M createModel() {
        return null;
    }

    @Override
    public V createView() {
        return null;
    }

    @Override
    public P createPersenter() {
        return null;
    }

    @Override
    public void initData() {
        super.initData();
        setTitle("设置");
        initWeight();
    }

    public void initWeight(){
       userImage = (ImageView) findViewById(R.id.iv_sex);
       txtUserName = (TextView)findViewById(R.id.txt_user_name);
       setClick(this,R.id.btn_remove_user);
       setUserMessage();
    }

    private void setUserMessage() {
        String userSex = (String) SpUtil.getSpData(this, "userSex", "");
        String nickName = (String) SpUtil.getSpData(this, "userNickname", "");
        if (userSex.equals("男")) {
            userImage.setImageResource(R.drawable.pc_min);
        } else {
            userImage.setImageResource(R.drawable.pc_womain);
        }

        if (!"".equals(nickName)) {
            txtUserName.setText(nickName);
        } else {
            txtUserName.setText("请设置昵称");
        }

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_remove_user) {
            SpUtil.clear();
            finish();
        }
    }
}