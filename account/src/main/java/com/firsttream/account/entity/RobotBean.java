package com.firsttream.account.entity;

public class RobotBean {
    public static final int SEND = 1;
    public static final int RECEIVE = 2;
    /**
     * code : 100000
     * text : 我是棒棒哒Android干货铺
     */

    private int code;
    private String text;
    private int flag;// 标识
    private String time;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
