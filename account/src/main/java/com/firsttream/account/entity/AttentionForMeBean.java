package com.firsttream.account.entity;

import java.util.List;

public    class AttentionForMeBean   {

    /**
     * status : 0
     * items : [{"my_user":"abner16223","my_nick":"来了老弟！！！","student_user":"abner13248","student_nick":"略略略"}]
     */

    private int status;
    private List<ItemsBean> items;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * my_user : abner16223
         * my_nick : 来了老弟！！！
         * student_user : abner13248
         * student_nick : 略略略
         */

        private String my_user;
        private String my_nick;
        private String student_user;
        private String student_nick;

        public String getMy_user() {
            return my_user;
        }

        public void setMy_user(String my_user) {
            this.my_user = my_user;
        }

        public String getMy_nick() {
            return my_nick;
        }

        public void setMy_nick(String my_nick) {
            this.my_nick = my_nick;
        }

        public String getStudent_user() {
            return student_user;
        }

        public void setStudent_user(String student_user) {
            this.student_user = student_user;
        }

        public String getStudent_nick() {
            return student_nick;
        }

        public void setStudent_nick(String student_nick) {
            this.student_nick = student_nick;
        }
    }
}
