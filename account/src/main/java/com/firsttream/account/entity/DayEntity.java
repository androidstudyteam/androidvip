package com.firsttream.account.entity;

import java.util.List;

public    class DayEntity   {

    /**
     * status : 0
     * items : [{"user_name":"abner12065","user_nick":"","user_time":"2018-12-26 14:28:55","user_day":"2018-12-26"},{"user_name":"abner12065","user_nick":"Tome123","user_time":"2018-12-26 14:31:22","user_day":"2018-12-26"},{"user_name":"abner12065","user_nick":"","user_time":"2018-12-26 14:32:58","user_day":"2018-12-26"},{"user_name":"abner12065","user_nick":"","user_time":"2018-12-26 14:33:25","user_day":"2018-12-26"},{"user_name":"abner12065","user_nick":"","user_time":"2018-12-26 14:35:06","user_day":"2018-12-26"},{"user_name":"abner12065","user_nick":"","user_time":"2018-12-26 14:35:40","user_day":"2018-12-26"},{"user_name":"abner12065","user_nick":"","user_time":"2018-12-26 14:36:02","user_day":"2018-12-26"},{"user_name":"abner12065","user_nick":"","user_time":"2018-12-26 14:36:43","user_day":"2018-12-26"},{"user_name":"abner12065","user_nick":"","user_time":"2018-12-26 14:37:49","user_day":"2018-12-26"},{"user_name":"abner12065","user_nick":"","user_time":"2018-12-26 14:38:14","user_day":"2018-12-26"},{"user_name":"abner12065","user_nick":"","user_time":"2018-12-26 14:39:35","user_day":"2018-12-26"}]
     */

    private int status;
    private List<ItemsBean> items;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * user_name : abner12065
         * user_nick :
         * user_time : 2018-12-26 14:28:55
         * user_day : 2018-12-26
         */

        private String user_name;
        private String user_nick;
        private String user_time;
        private String user_day;

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getUser_nick() {
            return user_nick;
        }

        public void setUser_nick(String user_nick) {
            this.user_nick = user_nick;
        }

        public String getUser_time() {
            return user_time;
        }

        public void setUser_time(String user_time) {
            this.user_time = user_time;
        }

        public String getUser_day() {
            return user_day;
        }

        public void setUser_day(String user_day) {
            this.user_day = user_day;
        }
    }
}
