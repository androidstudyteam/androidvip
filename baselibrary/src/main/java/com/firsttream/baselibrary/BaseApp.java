package com.firsttream.baselibrary;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.firsttream.baselibrary.net.HttpHelper;
import com.firsttream.baselibrary.util.SpUtil;
import com.firsttream.baselibrary.util.SqlUtil;

public class BaseApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        SqlUtil.getInstens().init(this,"vip_db");
    }
}
