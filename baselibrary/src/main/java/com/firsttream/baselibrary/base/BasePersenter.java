package com.firsttream.baselibrary.base;

import android.content.Context;

import com.firsttream.baselibrary.contract.IContract;

import java.lang.ref.WeakReference;

public abstract class BasePersenter<M extends IContract.Model, V extends IContract.View> implements IContract.Persenter<M, V> {

    protected M model;
    protected WeakReference<V> wrf;
    protected Context context;

    @Override
    public void registerView(V view) {
        wrf = new WeakReference<V>(view);
    }

    @Override
    public void registerModel(IContract.Model model) {
        this.model = (M) model;
    }

    @Override
    public V getView() {
        return wrf == null ? null : wrf.get();
    }

    @Override
    public void destory() {
        if (wrf != null) {
            wrf.clear();
            wrf = null;
        }
        onViewDestory();
    }

    public void initContext(Context context) {
        this.context = context;
    }

    protected abstract void onViewDestory();
}
