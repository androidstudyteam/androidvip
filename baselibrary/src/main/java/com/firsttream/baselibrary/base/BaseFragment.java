package com.firsttream.baselibrary.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firsttream.baselibrary.R;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.percent.PercentRelativeLayout;
import com.firsttream.baselibrary.util.UltimateBar;

public abstract class BaseFragment<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends Fragment implements BaseMVP<M, V, P> {
    private P persenter;
    private SparseArray<View> views = new SparseArray<>();
    private View view;
    private RelativeLayout prelateChild;
    private RelativeLayout RelatBar;
    private TextView txtTitle;
    private View child;
    private ImageView ivRightimage;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        persenter = createPersenter();
        if (persenter != null) {
            persenter.registerModel(createModel());
            persenter.registerView(createView());
        }
        initContext(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        UltimateBar.newImmersionBuilder().applyNav(false)
                .build(getActivity()).apply();
        view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_base, null);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initWeight();
        initData();
    }


    public void initContext(Context context) {

    }

    private void initWeight() {
        prelateChild = (RelativeLayout) view.findViewById(R.id.prelate_child);
        //获取一个view
        child = LayoutInflater.from(getActivity()).inflate(getLayoutId(), null);
        //追加到布局中
        prelateChild.addView(child);
        //获取bar
        RelatBar = (RelativeLayout) view.findViewById(R.id.relat_bar);
        view.findViewById(R.id.iv_back).setVisibility(View.GONE);
        //设置标题
        txtTitle = (TextView) view.findViewById(R.id.txt_title);
        ivRightimage = (ImageView) view.findViewById(R.id.iv_right);
        ivRightimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rightClick();
            }
        });
    }

    /**
     * 右面的点击事件
     */
    public void rightClick() {

    }


    /**
     * 设置右面
     *
     * @param isShow
     */
    public void setShowRight(boolean isShow) {
        if (isShow) {
            ivRightimage.setVisibility(View.VISIBLE);
        } else {
            ivRightimage.setVisibility(View.GONE);
        }
    }

    /**
     * 吐司的方法
     *
     * @param msg
     * @param s
     */
    public void toase(String msg, int s) {
        Toast.makeText(getActivity(), msg, s).show();
    }

    public View getView(int viewId) {
        View view = views.get(viewId);
        if (view == null) {
            view = child.findViewById(viewId);
            views.put(viewId, view);
        }
        return view;
    }

    /**
     * 设置点击事件
     *
     * @param listener
     * @param viewIds
     */
    public void setClick(View.OnClickListener listener, int... viewIds) {
        for (int id : viewIds) {
            getView(id).setOnClickListener(listener);
        }
    }

    /**
     * 设置title
     */
    public void setTitle(String title) {
        txtTitle.setText(title);
    }

    /**
     * 显示是否展示bar
     *
     * @param flag
     */
    public void setShowBar(boolean flag) {
        if (flag) {
            RelatBar.setVisibility(View.VISIBLE);
        } else {
            RelatBar.setVisibility(View.GONE);
        }
    }


    public void initData() {

    }

    /**
     * 获取控件id
     *
     * @return
     */
    protected abstract int getLayoutId();

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (persenter != null) {
            persenter.destory();
        }
    }
}
