package com.firsttream.baselibrary.base;

import com.firsttream.baselibrary.contract.IContract;

/**
 *作者：周建峰
 * 时间：201812-10
 * @param <M>
 * @param <V>
 * @param <P>
 */
public interface BaseMVP<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> {

    M createModel();

    V createView();

    P createPersenter();

}
