package com.firsttream.baselibrary.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firsttream.baselibrary.R;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.NetBroadcastReceiver;
import com.firsttream.baselibrary.util.NetUtil;
import com.firsttream.baselibrary.util.UltimateBar;

/**
 * 作者：周建峰
 * 时间：2018-12-11
 *
 * @param <M>
 * @param <V>
 * @param <P>
 */
public abstract class BaseActivity<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends AppCompatActivity implements BaseMVP<M, V, P>,NetBroadcastReceiver.NetEvevt {

    private P persenter;
    private SparseArray<View> views = new SparseArray<>();
    private View view;
    private RelativeLayout prelateChild;
    private RelativeLayout RelatBar;
    private TextView txtTitle;
    private ImageView ivBack;
    private ImageView ivRightimage;
    public static NetBroadcastReceiver.NetEvevt evevt;
    private int netMobile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = LayoutInflater.from(this).inflate(R.layout.layout_base, null);
        setContentView(view);
        UltimateBar.newImmersionBuilder().applyNav(false)
                .build(this).apply();
        evevt = this;
        /**
         * 初始化时判断有没有网络
         */
        inspectNet();
        //初始化控件
        initWeight();
        persenter = createPersenter();
        if (persenter != null) {
            persenter.registerModel(createModel());
            persenter.registerView(createView());
        }
        initContext(this);
        initData();
    }
    /**
     * 初始化时判断有没有网络
     */
    private boolean inspectNet() {
        this.netMobile = NetUtil.getNetWorkState(BaseActivity.this);
        return isNetConnect();
    }
    /**
     * 网络变化之后的类型   重写该方法判断netMobile即可
     */
    @Override
    public void onNetChange(int netMobile) {
        // TODO Auto-generated method stub
        this.netMobile = netMobile;
        isNetConnect();
    }
    /**
     * 判断有无网络 。
     *
     * @return true 有网, false 没有网络.
     */
    public boolean isNetConnect() {
        if (netMobile == 1) {
            return true;
        } else if (netMobile == 0) {
            return true;
        } else if (netMobile == -1) {
            return false;

        }
        return false;
    }

    private void initWeight() {
        prelateChild = (RelativeLayout) view.findViewById(R.id.prelate_child);
        //获取一个view
        View child = LayoutInflater.from(this).inflate(getLayoutId(), null);
        //追加到布局中
        prelateChild.addView(child);
        //获取bar
        RelatBar = (RelativeLayout) view.findViewById(R.id.relat_bar);
        txtTitle = (TextView) view.findViewById(R.id.txt_title);
        ivBack = (ImageView) view.findViewById(R.id.iv_back);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ivRightimage = (ImageView) findViewById(R.id.iv_right);
        ivRightimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rightClick();
            }
        });
    }

    public void rightClick() {

    }


    /**
     * 设置右面
     *
     * @param isShow
     */
    public void setShowRight(boolean isShow) {
        if (isShow) {
            ivRightimage.setVisibility(View.VISIBLE);
        } else {
            ivRightimage.setVisibility(View.GONE);
        }
    }

    /**
     * 设置title
     */
    public void setTitle(String title) {
        txtTitle.setText(title);
    }


    /**
     * 显示是否展示bar
     *
     * @param flag
     */
    public void setShowBar(boolean flag) {
        if (flag) {
            RelatBar.setVisibility(View.VISIBLE);
        } else {
            RelatBar.setVisibility(View.GONE);
        }
    }


    public void setShopBackBar(boolean flag) {
        if (flag) {
            ivBack.setVisibility(View.VISIBLE);
        } else {
            ivBack.setVisibility(View.GONE);
        }
    }

    /**
     * 执行方法
     */
    public void initData() {

    }

    /**
     * 获取子view
     *
     * @return
     */
    protected abstract int getLayoutId();

    /**
     * 吐司的方法
     *
     * @param msg
     * @param s
     */
    public void toase(String msg, int s) {
        Toast.makeText(this, msg, s).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (persenter != null) {
            persenter.destory();
        }
    }

    /**
     * 获取控件
     *
     * @param viewId
     * @return
     */

    public View getView(int viewId) {
        View view = views.get(viewId);
        if (view == null) {
            view = findViewById(viewId);
            views.put(viewId, view);
        }
        return view;
    }

    public void initContext(Context context) {

    }

    /**
     * 设置点击事件
     *
     * @param listener
     * @param viewIds
     */
    public void setClick(View.OnClickListener listener, int... viewIds) {
        for (int id : viewIds) {
            getView(id).setOnClickListener(listener);
        }
    }

}
