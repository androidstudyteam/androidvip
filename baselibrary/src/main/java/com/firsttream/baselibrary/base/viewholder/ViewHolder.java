package com.firsttream.baselibrary.base.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.drawee.view.SimpleDraweeView;
import com.firsttream.baselibrary.gif.GifTextView;

/**
 * 作者：xujiahui
 * 时间：2018/11/27
 * 作用：ViewHolder(recyclerview的Viewholder)
 */
public class ViewHolder extends RecyclerView.ViewHolder {
    private Context mContext;
    private SparseArray<View> views = new SparseArray<>();
    View rootView;

    public ViewHolder(@NonNull View itemView, Context mcontext) {
        super(itemView);
        this.mContext = mcontext;
        rootView = itemView;

    }


    public <T extends View> T getView(int viewId) {
        T view = (T) views.get(viewId);
        if (view == null) {
            view = rootView.findViewById(viewId);
            views.put(viewId, view);
        }
        return view;
    }

    public ViewHolder setText(int viewId, String msg) {
        TextView txtview = (TextView) getView(viewId);
        txtview.setText(msg);
        return this;
    }
    public ViewHolder setFromHtml(int viewId, String msg) {
        TextView txtview = (TextView) getView(viewId);
        txtview.setText(msg);
        return this;
    }

    public ViewHolder setTextInsert(int viewId, String msg) {
        GifTextView txtview = (GifTextView) getView(viewId);
        txtview.insertGif(msg);
        return this;
    }

    public ViewHolder setSimpleDraweViewUrl(int viewId, String url) {
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) getView(viewId);
        simpleDraweeView.setImageURI(url);
        return this;
    }

    public ViewHolder setImageResource(int viewId, int resource) {
        ImageView imageView = (ImageView) getView(viewId);
        imageView.setImageResource(resource);
        return this;
    }

    //设置文本是否显示
    public ViewHolder setVisibilityText(int viewId, int resource) {
        TextView textView = (TextView) getView(viewId);
        textView.setVisibility(resource);
        return this;
    }

    public ViewHolder setUrlImage(int viewId, String url) {
        ImageView view = getView(viewId);
        Glide.with(mContext).load(url).into(view);
        return this;
    }

    public void setClick(View.OnClickListener listener, int... ids) {
        if (ids == null) {
            return;
        }
        for (int id : ids) {
            getView(id).setOnClickListener(listener);
        }
    }

    public View getRootView() {
        return rootView;
    }
    //    public ViewHolder setImageUrl(int viewId, String url) {
//        ImageView imageView = (ImageView) getView(viewId);
//        Picasso.with(mContext).load(url).fit().into(imageView);
//        return this;
//    }

    //设置文本颜色
    public ViewHolder setTextColor(int viewId, int colorid) {
        TextView txtview = (TextView) getView(viewId);
        txtview.setTextColor(colorid);
        return this;
    }

    //如果想添加其他的方法请在下方添加参照setText如获取网络图片等....



}
