package com.firsttream.baselibrary.base;

import android.content.Context;

import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.HttpHelper;

import java.util.Map;

public class BaseModel implements IContract.Model {
    @Override
    public void getString(Context context, final String url, Map<String, String> map, final int type, final boolean isLurCatch, boolean isShow,boolean upDataCache) {
        new HttpHelper(context).result(new HttpHelper.HttpListener() {
            @Override
            public void Success(String data) {
                successString(data, type);
            }

            @Override
            public void Error(String msg) {
                failString(msg);
            }
        }).readCache(isLurCatch).updateCache(upDataCache).get(type,url, map, isShow);
    }

    //post
    @Override
    public void postString(Context context, String url, Map<String, String> map, final int type, boolean isLurCatch, boolean isShow,boolean upDataCache) {

        new HttpHelper(context).result(new HttpHelper.HttpListener() {
            @Override
            public void Success(String data) {
                successString(data, type);
            }

            @Override
            public void Error(String msg) {
                failString(msg);
            }
        }).post(type,url, map, isShow).readCache(isLurCatch).updateCache(upDataCache);
    }

    //请求成功
    public void successString(String data, int type) {

    }

    //请求失败
    public void failString(String msg) {

    }


    @Override
    public void stopRequest() {

    }
}
