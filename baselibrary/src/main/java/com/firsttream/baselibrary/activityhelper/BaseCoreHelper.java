package com.firsttream.baselibrary.activityhelper;

import android.content.Context;
/**
 * Created by lixiaoming on 8/4/16.
 */
public interface BaseCoreHelper {
    /**
     * login
     * @param mContext
     */
    void openLogin(Context mContext);

    //学习
    void openLearn(Context context);
}