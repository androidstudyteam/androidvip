package com.firsttream.baselibrary.activityhelper;

import android.content.Context;

/**
 * Created by lixiaoming on 8/4/16.
 */
public class ActionHelper {
    BaseCoreHelper helper;

    public ActionHelper() {
        helper = new BaseCoreHelperImpl();
    }

    public void openLogin(Context mContext) {
        helper.openLogin(mContext);
    }

    //跳转学习
    public void openLearn(Context mContext) {
        helper.openLearn(mContext);
    }


}