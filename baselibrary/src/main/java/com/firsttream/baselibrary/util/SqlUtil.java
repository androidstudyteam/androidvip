package com.firsttream.baselibrary.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.firsttream.baselibrary.entity.HistoryEntity;
import com.firsttream.baselibrary.greendao.DaoMaster;
import com.firsttream.baselibrary.greendao.DaoSession;
import com.firsttream.baselibrary.greendao.HistoryEntityDao;

import java.util.List;

public class SqlUtil {
    private static SqlUtil sqlUtil;
    private HistoryEntityDao historyEntityDao;


    private SqlUtil() {

    }

    public static SqlUtil getInstens() {
        if (sqlUtil == null) {
            synchronized (SqlUtil.class) {
                sqlUtil = new SqlUtil();
            }
        }
        return sqlUtil;
    }

    public void init(Context context, String dbName) {
        DaoMaster.OpenHelper helper = new DaoMaster.DevOpenHelper(context, dbName);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoSession daoSession = new DaoMaster(db).newSession();
        historyEntityDao = daoSession.getHistoryEntityDao();
    }

    public void insert(String type, String data) {
        HistoryEntity historyEntity = new HistoryEntity();
        historyEntity.setTpye(type);
        historyEntity.setHistory(data);
        historyEntityDao.insert(historyEntity);
    }

    /**
     * 删除全部
     */
    public void deleteAll() {
        historyEntityDao.deleteAll();
    }

    /**
     * 查询全部
     *
     * @return
     */
    public List<HistoryEntity> queryAll() {
        return historyEntityDao.loadAll();
    }

    /**
     * 根据类型查询
     *
     * @param type
     * @return
     */
    public HistoryEntity queryByType(String type) {
        List<HistoryEntity> historyEntities = historyEntityDao.loadAll();
        for (int i = 0; i < historyEntityDao.loadAll().size(); i++) {
            if (type.equals(historyEntities.get(i).getTpye())) {
                return historyEntities.get(i);
            }
        }
        return null;
    }

    /**
     * 更加key删除
     *
     * @param key
     */

    public void deleteByKey(Long key) {
        historyEntityDao.deleteByKey(key);
    }

    /**
     * 删除
     *
     * @param type
     */
    public void deleteByType(String type) {
        List<HistoryEntity> historyEntities = historyEntityDao.loadAll();
        for (int i = 0; i < historyEntityDao.loadAll().size(); i++) {
            if (type.equals(historyEntities.get(i).getTpye())) {
                historyEntityDao.delete(historyEntities.get(i));
            }
        }
    }


    public void upDataByType(String type, String data) {
        List<HistoryEntity> historyEntities = historyEntityDao.loadAll();
        for (int i = 0; i < historyEntityDao.loadAll().size(); i++) {
            if (type.equals(historyEntities.get(i).getTpye())) {
                HistoryEntity historyEntity = historyEntities.get(i);
                historyEntity.setHistory(data);
                historyEntityDao.update(historyEntity);
            }
        }
    }

    /**
     * 判断是否有相同的类型
     *
     * @param type
     * @return
     */
    public boolean queryIsExist(String type) {
        List<HistoryEntity> historyEntities = historyEntityDao.loadAll();
        for (int i = 0; i < historyEntityDao.loadAll().size(); i++) {
            if (historyEntities.get(i).getTpye().equals(type)) {
                return true;
            }
        }
        return false;
    }

}