package com.firsttream.baselibrary.util;

import android.os.Handler;
import android.os.Message;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
/**
*作者：gaojiabao
*时间：2018/12/25 8:55
*作用：ok网络请求
*/
public class OkHttpUtiles {
    private Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case HTTP_SUCCESS:
                        String data = (String) msg.obj;
                        //
                        httpListener.success(data);
                        break;
                    case HTTP_ERROR:
                        break;
                }
            }
        };
        private final int HTTP_ERROR = 100;
        private final int HTTP_SUCCESS = 313;
        private HttpListener httpListener;

        //GET请求
        public OkHttpUtiles get(String url) {
            //实例化OkHttpClient
            OkHttpClient client = new OkHttpClient();
            //Request.Builder()对象
            Request resquest = new Request.Builder()
                    .get()
                    .url(url)
                    .build();
            //异步请求enqueue
            client.newCall(resquest).enqueue(new Callback() {
                //失败
                @Override
                public void onFailure(Call call, IOException e) {
                    //发送失败 消息
                    handler.sendEmptyMessage(HTTP_ERROR);
                }

                //成功
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String string = response.body().string();
                    Message obtain = Message.obtain();
                    obtain.what = HTTP_SUCCESS;
                    obtain.obj = string;
                    handler.sendMessage(obtain);
                }
            });
            return this;
        }
        //传递接口
        public void result(HttpListener httpListener) {
            this.httpListener = httpListener;
        }

    //接口回调
        public interface HttpListener{
            void success(String data);
            void errror();
    }

}
