package com.firsttream.baselibrary.net;

import android.content.Context;

import com.firsttream.baselibrary.entity.HistoryEntity;
import com.firsttream.baselibrary.util.LodingDialog;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.baselibrary.util.NetworkUtils;
import com.firsttream.baselibrary.util.SqlUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpHelper {
    private LodingDialog loadingDialog;
    private Context context;
    private BaseService mBaseService;
    private boolean isreadCache;//默认情况下不读取缓存
    private boolean updateCache ;//默认情况下更新缓存
    private int type;

    public HttpHelper(Context context) {
        this.context = context;
        loadingDialog = new LodingDialog.Builder(context).create();
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(Http.BASE_URL).build();
        mBaseService = retrofit.create(BaseService.class);

    }
    //设置是否读取缓存
    public HttpHelper readCache(boolean isreadCache) {
        this.isreadCache = isreadCache;
        return this;
    }

    //设置是否更新缓存
    public HttpHelper updateCache(boolean updateCache) {
        this.updateCache = updateCache;
        return this;
    }

    //get请求
    public HttpHelper get(int type,String url, Map<String, String> map, boolean isShow) {
        doHttp(type,0, url, map, isShow);
        return this;
    }

    private void doHttp(int type,int i, String url, Map<String, String> map, boolean isShow) {
        if (isShow) {
            loadingDialog.show();
        }
        if (map == null) {
            map = new HashMap<>();
        }
        this.type = type;
        if (isreadCache && !NetworkUtils.isConnected(context)) {
            HistoryEntity historyEntity = SqlUtil.getInstens().queryByType(type+"");
            listener.Success(historyEntity.getHistory());
            return;
        }
        Observable<ResponseBody> ob;
        if (i == 1) {
            ob = mBaseService.post(url, map);
        }else{
            ob = mBaseService.get(url, map);
        }
        ob.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    //post请求
    public HttpHelper post(int type,String url, Map<String, String> map, boolean isShow) {
        doHttp(type,1, url, map, isShow);
        return this;
    }

    private BaseObserver observer = new BaseObserver<ResponseBody>() {

        @Override
        public void onNext(ResponseBody responseBody) {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
            }
            String data = null;
            try {
                data = responseBody.string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            listener.Success(data);
            if (isreadCache && !SqlUtil.getInstens().queryIsExist(type+"")) {
                SqlUtil.getInstens().insert(type+"", data);
            } else {
                if (updateCache) {
                    SqlUtil.getInstens().upDataByType(type+"", data);
                }
            }
        }

        @Override
        public void onError(Throwable e) {
            listener.Error(e.getMessage());
            if (loadingDialog != null) {
                loadingDialog.dismiss();
            }
        }
    };

    public interface HttpListener {
        void Success(String data);
        void Error(String msg);
    }

    private HttpListener listener;

    public HttpHelper result(HttpListener listener) {
        this.listener = listener;
        return this;
    }


}