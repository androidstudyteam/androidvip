package com.firsttream.baselibrary.net;

/**
 * 日期:2018/12/10
 * 作用:全部接口
 */
public class Http {
    public static final String BASE_URL = "http://www.abnerming.cn/";
    //首页Banner图
    public static final String HOMEPAFE_BANNER = "/txt/banner_1.txt";
    //首页弹一弹发送消息
    public static final String HOMEPAFE_SENDPLAYAPALYURL = "/cert/add_news_speak.php";
    //首页弹一弹接收消息
    public static final String HOMEPAFE_RECEIVEPLAYAPALYURL = "/cert/get_news_speak.php";
    //首页群聊大厅接收消息
    public static final String HOMEPAFE_RECEIVEGROUPCHAR = "/cert/get_android_msg.php";
    //首页群聊大厅发送消息
    public static final String HOMEPAFE_SENDGROUPCHAR = "/cert/add_android_msg.php";
    //首页获取精华
    public static final String HOMEPAFE_RECEIVEESSENCE = "/cert/get_tree_msg_bbs2.php";
    //首页获取热门
    public static final String HOMEPAFE_RECEIVEHOT = "/cert/get_tree_msg_bbs3.php";
    public static final String HOMEPAFE_GETPIC =BASE_URL+ "/cert/upload/";
    //首页获取全部论坛消息
    public static final String HOMEPAFE_RECEIVEFORUM = "/cert/get_tree_msg_bbs1.php";
  /*  //首页弹一弹发送消息
    public static final String HOMEPAFE_SENDPLAYAPALYURL = "/cert/add_news_speak.php";
    //首页弹一弹接收消息
    public static final String HOMEPAFE_RECEIVEPLAYAPALYURL = "/cert/get_news_speak.php";*/
    //首页底部文章
    public static final String HOMEPAFE_BUTTOMARTICLE = "/cert/bw_get_index_news.php";
    /*经典和视频页面接口*/
    public static final String CLASSICURL = "/txt/jingdian_title.txt";
    public static final String CLASSICDESCURL = "/cert/bw_get_index_news.php";
    public static final String VIDEOHOMEURL = "/txt/live/video_index.txt";
    public static final String VIDEORVURL = "/txt/live/video_";
    public static final String VIDEOMORETITLE = "/txt/more_live.txt";
    public static final String VIDEOMOREDESC = "/txt/live_";
    public static final String VIDEOMOREDESCLIST = "/txt/";
    /* 学习页面接口*/
    public static final String LEARNURL = "/txt/study.txt";

    public static final String LEARN_LATER_URL = "/txt/study_";
    //视频域名
    public static final String BASE_VIDEOURL = "http://www.vipandroid.cn/";
    //视频地址
    public static final String VIDEO_U_URL = "/cert/gan_live.php";
    //登录
    public static final String USER_LOGIN_URL = "/cert/LoginUser.php";
    //注册
    public static final String USER_REIGIS_URL = "/cert/RegisterUser.php";
    //图灵机器人
    public static final String  TULING_ROBOTURL="http://www.tuling123.com/openapi/api?key=e2dfa348daf24852bbcc3ab88bf9c1ea&info=";
    //我關注的
    public static final String USERFOCUS_ME = "/cert/get_my_student.php";
    //修改昵称
    public static final String USER_UPDATA_NICKNAME = "/cert/ChangeUser_nickname.php";
    //修改地址
    public static final String USERCHANGE_ADDRESS = "/cert/ChangeUser_address.php";
    //修改年龄
    public static final String USERCHANGE_AGE = "/cert/ChangeUser_age.php";
    //修改手机号
    public static final String USERCHANGE_PHONE = "/cert/ChangeUser_phone.php";
    //修改公司
    public static final String USERCHANGE_COMPANY = "/cert/ChangeUser_company.php";
    //修改性别
    public static final String USERCHANGE_SEX = "/cert/ChangeUser_sex.php";
    //关联微博
    public static final String USERCHANGE_WB = "/cert/ChangeUser_wb.php";
    //關注我的
    public static final String USER_STUDENT_OUTHER = "/cert/get_my_student_other.php";
    //我的分享
    public static final String USER_ARITICLE = "/cert/get_shared_ariticle_me.php";
    //判断是否签到
    public static final String USER_IS_SIGN = "/cert/is_sign_in.php";

    //添加签到
    public static final String USRE_ADD_SIGN = "/cert/add_sign_in.php";

    //查询
    public static final String USER_QUERY_SIGN = "/cert/get_sign_in.php";
}