package com.firsttream.baselibrary.contract;

import android.content.Context;

import java.util.Map;

public interface IContract {

    interface Model {
        void getString(Context context, String url, Map<String, String> map, int type, boolean isLurCatch, boolean isShow,boolean upDataCache);

        void postString(Context context, String url, Map<String, String> map, int type, boolean isLurCatch, boolean isShow,boolean upDataCache);

        void stopRequest();//销毁
    }

    interface Persenter<M extends Model, V extends View> {

        //注册view
        void registerView(V view);

        //注册model
        void registerModel(M model);

        //获取view
        V getView();

        //销毁
        void destory();
    }

    interface View {

    }

}