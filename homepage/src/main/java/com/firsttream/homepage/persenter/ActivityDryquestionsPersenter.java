package com.firsttream.homepage.persenter;


import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.homepage.model.ActivityDryquestionsModel;

/**
 * 作者:xjh
 * 时间:2018-12-14
 * 作用:ActivityDryquestionsPersenter
 */
public class ActivityDryquestionsPersenter<M extends IContract.Model, V extends IContract.View> extends BasePersenter<M, V> {
    @Override
    protected void onViewDestory() {

    }

    public void getReceiveGroupChar(int page) {
        ((ActivityDryquestionsModel)model).getReceiveGroupChar(context,getView(),page);
    }
    public void getSendGroupChar(String message) {
        ((ActivityDryquestionsModel)model).getSendGroupChar(context,getView(),message);
    }
}
