package com.firsttream.homepage.persenter;

import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.homepage.model.ActivityPlayAPlayModel;


/**
 * 作者:xjh
 * 时间:2018-12-15
 * 作用:ActivityVipDrayCargoPersenter
 */
public class ActivityPlayAPlayPersenter<M extends IContract.Model, V extends IContract.View> extends BasePersenter<M, V> {
    @Override
    protected void onViewDestory() {

    }

    public void getReceivePlayaplayurl(int page) {
        ((ActivityPlayAPlayModel) model).getReceivePlayaplayurl(context, getView(),page);
    }

    public void getSendPlayaplayurl(String message) {
        ((ActivityPlayAPlayModel) model).getSendPlayaplayurl(context, getView(), message);
    }
}
