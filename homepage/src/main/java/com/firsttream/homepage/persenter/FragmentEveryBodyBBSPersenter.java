package com.firsttream.homepage.persenter;

import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.homepage.model.ActivityEveryBodyBBSModel;
import com.firsttream.homepage.model.FragmentEveryBodyBBSModel;

/**
 * 作者:xjh
 * 时间:2018-12-14
 * 作用:ActivityEveryBodyBBSPersenter
 */
public class FragmentEveryBodyBBSPersenter<M extends IContract.Model, V extends IContract.View> extends BasePersenter<M, V> {
    @Override
    protected void onViewDestory() {

    }

    /***
     * 获取全部论坛消息
     */
    public void getReceiveForum(int size) {
        ((FragmentEveryBodyBBSModel) model).getReceiveForum(context, getView(), size);
    }

    /***
     * 获取精华
     */
    public void getReceiveEssence(int size) {
        ((FragmentEveryBodyBBSModel) model).getReceiveEssence(context, getView(), size);
    }

    /***
     * 获取热门
     * */
    public void getReceiveHot(int size) {
        ((FragmentEveryBodyBBSModel) model).getReceiveHot(context, getView(), size);
    }
}
