package com.firsttream.homepage.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.firsttream.baselibrary.base.BaseFragment;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.homepage.R;
import com.firsttream.homepage.adapaters.EveryBodyBBSAdapter;
import com.firsttream.homepage.entity.InvitationBean;
import com.firsttream.homepage.model.FragmentEveryBodyBBSModel;
import com.firsttream.homepage.persenter.FragmentEveryBodyBBSPersenter;
import com.firsttream.homepage.view.FragmentEveryBodyBBSView;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.scwang.smartrefresh.header.BezierCircleHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

/**
 * 作者:xjh
 * 时间:2018-12-23
 * 作用:FragmentEveryBodyBBS
 */
public class FragmentEveryBodyBBS<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseFragment<M, V, P> implements FragmentEveryBodyBBSView {
    private FragmentEveryBodyBBSPersenter mPersenter;
    private XRecyclerView mXrecycleEveryBodyBBS;
    private EveryBodyBBSAdapter everyBodyBBSAdapter;
    private RefreshLayout mEveryBodyBBSRF;
    private int mFlag;


    @Override
    protected int getLayoutId() {
        return R.layout.layout_fragment_everybodybbs;
    }

    @Override
    public void initData() {
        super.initData();
        setShowBar(false);
        Bundle arguments = getArguments();
        mFlag = arguments.getInt("flag");
        initWidget();
        switch (mFlag) {
            case 0:
                mPersenter.getReceiveForum(1);
                break;
            case 1:
                mPersenter.getReceiveEssence(1);
                break;
            case 2:
                mPersenter.getReceiveHot(1);
                break;
        }

    }

    private void initWidget() {
        mXrecycleEveryBodyBBS = (XRecyclerView) getView(R.id.homepage__xrecycler_everybodybbs);
        mEveryBodyBBSRF= (RefreshLayout) getView(R.id.homepage_refresh_everybodybbsrf);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mXrecycleEveryBodyBBS.setLayoutManager(linearLayoutManager);
        everyBodyBBSAdapter = new EveryBodyBBSAdapter(getContext());
        mXrecycleEveryBodyBBS.setAdapter(everyBodyBBSAdapter);
        mXrecycleEveryBodyBBS.setPullRefreshEnabled(false);
        mXrecycleEveryBodyBBS.setLoadingMoreEnabled(false);
        //设置 Header 为 贝塞尔雷达 样式
        mEveryBodyBBSRF.setRefreshHeader(new BezierCircleHeader(getContext())).setPrimaryColorsId(R.color.colorYellow);
        //设置 Footer 为 球脉冲 样式
        mEveryBodyBBSRF.setRefreshFooter(new BallPulseFooter(getContext()).setSpinnerStyle(SpinnerStyle.Scale));
        mEveryBodyBBSRF.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                switch (mFlag) {
                    case 0:
                        mPersenter.getReceiveForum(1);
                        break;
                    case 1:
                        mPersenter.getReceiveEssence(1);
                        break;
                    case 2:
                        mPersenter.getReceiveHot(1);
                        break;
                }
                refreshlayout.finishRefresh(1000/*,false*/);//传入false表示刷新失败
            }
        });

        mEveryBodyBBSRF.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                switch (mFlag) {
                    case 0:
                        mPersenter.getReceiveForum(1);
                        break;
                    case 1:
                        mPersenter.getReceiveEssence(1);
                        break;
                    case 2:
                        mPersenter.getReceiveHot(1);
                        break;
                }
                refreshlayout.finishLoadMore(1000/*,false*/);//传入false表示加载失败
            }
        });
    }

    @Override
    public M createModel() {
        return (M) new FragmentEveryBodyBBSModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new FragmentEveryBodyBBSPersenter();
        mPersenter.initContext(getContext());
        return (P) mPersenter;
    }

    @Override
    public void getReceiveForum(String data) {//全部论坛
        Gson gson = new Gson();
        InvitationBean invitationBean = gson.fromJson(data, InvitationBean.class);
        everyBodyBBSAdapter.setList(invitationBean.getItems());
    }

    @Override
    public void getReceiveEssence(String data) {//精华论坛
        Gson gson = new Gson();
        InvitationBean invitationBean= gson.fromJson(data, InvitationBean.class);
        everyBodyBBSAdapter.setList(invitationBean.getItems());

    }

    @Override
    public void getReceiveHot(String data) {//热门论坛
        Gson gson = new Gson();
        InvitationBean invitationBean = gson.fromJson(data, InvitationBean.class);
        everyBodyBBSAdapter.setList(invitationBean.getItems());

    }


}
