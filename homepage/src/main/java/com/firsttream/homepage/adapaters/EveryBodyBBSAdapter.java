package com.firsttream.homepage.adapaters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.homepage.R;
import com.firsttream.homepage.activitys.ActivityShowArticle;
import com.firsttream.homepage.entity.InvitationBean;
import com.zzhoujay.richtext.RichText;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者:xjh
 * 时间:2018-12-24
 * 作用:EveryBodyBBSAdapter
 */
public class EveryBodyBBSAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private List<InvitationBean.ItemsBean> list = new ArrayList<>();

    public EveryBodyBBSAdapter(Context mcontext) {
        this.mContext = mcontext;
    }

    public void setList(List<InvitationBean.ItemsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_everybodybbsadatper_item, null);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {

        ((Viewholder) viewHolder).txttitle.setText(list.get(i).getMsg_title());
        ((Viewholder)viewHolder).txtdesc.setMovementMethod(LinkMovementMethod.getInstance());
        RichText.fromHtml(list.get(i).getMsg_content()).into(((Viewholder) viewHolder).txtdesc);
        ((Viewholder) viewHolder).txttime.setText(list.get(i).getMsg_hf_time());
        ((Viewholder)viewHolder).txtnickname.setText(list.get(i).getMsg_nick());
        ((Viewholder)viewHolder).txthot.setText(list.get(i).getMsg_hf());
        Glide.with(mContext).load("http://www.vipandroid.cn/cert/upload/" + list.get(i).getMsg_user() + ".png").into(((Viewholder) viewHolder).imgpic);
        Logger.d("EveryBodyBBSAdaptertag", "http://www.vipandroid.cn/cert/upload/" + list.get(i).getMsg_user() + ".png" + "");
        ((Viewholder)viewHolder).txtreply.setText("查看:" + list.get(i).getMsg_ck());
        /* viewHolder.mGood.setText(list.get(i).getMsg_hf());*/
        ((Viewholder)viewHolder).mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityShowArticle.class);
                InvitationBean.ItemsBean itemsBean = list.get(i);
                String msgContent = itemsBean.getMsg_content();
                intent.putExtra("msgContent",msgContent+"");
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /***
     * ViewHolder
     */
    public static class Viewholder extends RecyclerView.ViewHolder {

        private final ImageView imgpic;
        private final TextView txttitle, txtdesc, txttime, txtnickname, txthot,txtreply;
        private final LinearLayout mLinearLayout;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            txttitle = (TextView) itemView.findViewById(R.id.homepage_txt_bbstitle);
            txtdesc = (TextView) itemView.findViewById(R.id.homepage_txt_bbsdesc);
            txtnickname = (TextView) itemView.findViewById(R.id.homepage_txt_bbsnickname);
            txttime = (TextView) itemView.findViewById(R.id.homepage_txt_bbsnicktime);
            txthot = (TextView) itemView.findViewById(R.id.homepage_txt_bbshot);
            imgpic = (ImageView) itemView.findViewById(R.id.homepage_img_bbspic);
            txtreply = (TextView) itemView.findViewById(R.id.homepage_txt_bbsreply);
            mLinearLayout= (LinearLayout) itemView.findViewById(R.id.homepage_liner_bbsliner);
        }
    }

}
