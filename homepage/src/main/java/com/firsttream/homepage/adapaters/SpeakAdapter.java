
package com.firsttream.homepage.adapaters;

import android.content.Context;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.firsttream.baselibrary.base.recycle.RecycleAdapter;
import com.firsttream.baselibrary.base.viewholder.ViewHolder;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.homepage.R;
import com.firsttream.homepage.entity.SpeakBean;

/**
 * 作者:xjh
 * 时间:2018-12-17
 * 作用:SpeakAdapter
 */
public class SpeakAdapter extends RecycleAdapter<SpeakBean.ItemsBean> {

    private Context mContext;

    public SpeakAdapter(Context mcontext) {
        super(mcontext);
        this.mContext=mcontext;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.speakadapter_xrecycle_item;
    }

    @Override
    protected void convert(ViewHolder viewHolder, SpeakBean.ItemsBean itemsBean, int postion) {
        viewHolder.setText(R.id.homepage_txt_speaknickname, itemsBean.getOpen_nickname());
        viewHolder.setText(R.id.homepage_txt_speaktime, itemsBean.getOpen_time());
        ImageView imgPic =(ImageView) viewHolder.getView(R.id.homepage_img_speakpic);
        RequestOptions options = new RequestOptions()
                .fallback( R.drawable.pc_man) //url为空的时候,显示的图片
                .error(R.drawable.pc_man);//图片加载失败后，显示的图片
        Glide.with(mContext).load(Http.HOMEPAFE_GETPIC + itemsBean.getOpen_user() + ".png").apply(options).apply(RequestOptions.bitmapTransform(new CircleCrop())).into(imgPic);
        viewHolder.setTextInsert(R.id.homepage_txt_speakcontent, itemsBean.getOpen_content());


    }
}
