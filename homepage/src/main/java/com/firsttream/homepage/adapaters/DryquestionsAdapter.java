package com.firsttream.homepage.adapaters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.firsttream.baselibrary.gif.GifTextView;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.baselibrary.util.SpUtil;
import com.firsttream.homepage.R;
import com.firsttream.homepage.entity.DryquestionBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者:xjh
 * 时间:2018-12-21
 * 作用:DryquestionsAdapter
 */
public class DryquestionsAdapter extends RecyclerView.Adapter {

    private List<DryquestionBean.ItemsBean> list = new ArrayList<>();
    private Context mContext;
    private int type = 0;

    public DryquestionsAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setList(List<DryquestionBean.ItemsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewtype) {
        if (viewtype == 0) {
            View outherView = LayoutInflater.from(mContext).inflate(R.layout.layout_otherchatting_item, null);
            return new OutherViewHolder(outherView);
        } else {
            View accountView = LayoutInflater.from(mContext).inflate(R.layout.layout_mechatting_item, null);
            return new AccountViewHolder(accountView);
        }      
    }

    @Override
    public int getItemViewType(int position) {
        String userId = (String) SpUtil.getSpData(mContext, "userId", "");
        if (!userId.equals(list.get(position).getMsg_user())) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        int itemViewType = getItemViewType(i);
        if (itemViewType == 0) {
            RequestOptions options = new RequestOptions()
                    .fallback( R.drawable.pc_man) //url为空的时候,显示的图片
                    .error(R.drawable.pc_man);//图片加载失败后，显示的图片
            Glide.with(mContext).load(Http.HOMEPAFE_GETPIC + list.get(i).getMsg_user()+ ".png").apply(options).apply(RequestOptions.bitmapTransform(new CircleCrop())).into(((OutherViewHolder) viewHolder).chattingPic);
            ((OutherViewHolder) viewHolder).chattingNickName.setText(list.get(i).getNickName());
            ((OutherViewHolder) viewHolder).chattingContent.insertGif(list.get(i).getMsg_content());
        } else {
            RequestOptions options = new RequestOptions()
                    .fallback( R.drawable.pc_man) //url为空的时候,显示的图片
                    .error(R.drawable.pc_man);//图片加载失败后，显示的图片
            Glide.with(mContext).load(Http.HOMEPAFE_GETPIC + list.get(i).getMsg_user()+ ".png").apply(options).apply(RequestOptions.bitmapTransform(new CircleCrop())).into(((OutherViewHolder) viewHolder).chattingPic);
            ((AccountViewHolder) viewHolder).chattingNickName.setText(list.get(i).getNickName());
            ((AccountViewHolder) viewHolder).chattingContent.insertGif(list.get(i).getMsg_content());
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class OutherViewHolder extends RecyclerView.ViewHolder {
        private final TextView chattingNickName;
        private final GifTextView chattingContent;
        private final ImageView chattingPic;

        public OutherViewHolder(@NonNull View itemView) {
            super(itemView);
            chattingPic = (ImageView) itemView.findViewById(R.id.homepage_img_chattingpic);
            chattingNickName = (TextView) itemView.findViewById(R.id.homepage_txt_chattingnickname);
            chattingContent = (GifTextView) itemView.findViewById(R.id.homepage_txt_chattingcontent);

        }
    }

    public static class AccountViewHolder extends RecyclerView.ViewHolder {
        private final TextView chattingNickName;
        private final ImageView chattingPic;
        private final GifTextView chattingContent;

        public AccountViewHolder(@NonNull View itemView) {
            super(itemView);
            chattingPic = (ImageView) itemView.findViewById(R.id.homepage_img_chattingpic);
            chattingNickName = (TextView) itemView.findViewById(R.id.homepage_txt_chattingnickname);
            chattingContent = (GifTextView) itemView.findViewById(R.id.homepage_txt_chattingcontent);
        }
    }
}
