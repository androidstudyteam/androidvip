package com.firsttream.homepage;

import android.content.Context;

import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.homepage.model.ActivityHomePageMainModel;
import com.firsttream.homepage.persenter.ActivityHomePageMainPersenter;
import com.firsttream.homepage.view.ActivityHomePageMainView;

public class HomePageActivityMain<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> implements ActivityHomePageMainView {

    private ActivityHomePageMainPersenter persenter;

    private Context mcontext;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_homepage;
    }

    @Override
    public void initData() {
        super.initData();


    }

    private void initWidget() {

    }

    @Override
    public M createModel() {
        return (M) new ActivityHomePageMainModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        persenter = new ActivityHomePageMainPersenter();
        return (P) persenter;
    }

    @Override
    public void getData(String data) {

    }

    @Override
    public void initContext(Context context) {
        super.initContext(context);
        this.mcontext=context;
    }
}
