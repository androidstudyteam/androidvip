package com.firsttream.homepage.view;

import com.firsttream.baselibrary.contract.IContract;

public interface ActivityHomePageMainView extends IContract.View {

    void getData(String data);

}
