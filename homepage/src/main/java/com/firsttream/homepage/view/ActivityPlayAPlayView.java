package com.firsttream.homepage.view;

import com.firsttream.baselibrary.contract.IContract;

/**
 * 作者:xjh
 * 时间:2018-12-15
 * 作用:ActivityPlayAPlayView
 */
public interface ActivityPlayAPlayView extends IContract.View {

    //获取弹一弹消息
    void  getReceivePlayaplayurl(String data);

    //发送弹一弹消息
    void  getSendPlayaplayurl(String data);

}