package com.firsttream.homepage.view;

import com.firsttream.baselibrary.contract.IContract;

/**
 * 作者:xjh
 * 时间:2018-12-14
 * 作用:ActivityDryquestionsView
 */
public interface ActivityDryquestionsView extends IContract.View {
    //获取群聊消息
    void getReceiveGroupChar(String data);

    //添加群聊消息
    void getSendGroupChar(String data);

}