package com.firsttream.homepage.view;

import com.firsttream.baselibrary.contract.IContract;

/**
 * 作者:xjh
 * 时间:2018-12-14
 * 作用:ActivityEveryBodyBBSView
 */
public interface FragmentEveryBodyBBSView extends IContract.View {

    /***
     * 获取全部论坛消息
     */
    void getReceiveForum(String data);
    /***
     * 获取精华
     */
    void getReceiveEssence(String data);
    /***
     * 获取热门
     * */
    void getReceiveHot(String data);
}