package com.firsttream.homepage.model;

import android.content.Context;
import android.widget.Toast;

import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.baselibrary.util.SpUtil;
import com.firsttream.homepage.view.ActivityDryquestionsView;

import java.util.HashMap;
import java.util.Map;

/**
 * 作者:xjh
 * 时间:2018-12-14
 * 作用:ActivityDryquestionsModel
 */
public class ActivityDryquestionsModel extends BaseModel {

    private static final int RECEIVEGROUPCHAR = 0x127;
    private static final int SENDGROUPCHAR = 0x128;
    private IContract.View view;

    /***
     * 获取群聊消息
     * @param context
     * @param view
     */
    public void getReceiveGroupChar(Context context, IContract.View view,int page) {
        this.view = view;
      Map<String, String> map = new HashMap<>();
        map.put("open_page",page+"" );
        map.put("open_num", 20+ "");
        getString(context, Http.HOMEPAFE_RECEIVEGROUPCHAR, map, RECEIVEGROUPCHAR, true, true, true);
    }
    public void getSendGroupChar(Context context, IContract.View view,String message) {
        this.view = view;
        String userNickname = (String) SpUtil.getSpData(context, "userNickname", "");
        String userId = (String) SpUtil.getSpData(context, "userId", "");
        Map<String, String> map = new HashMap<>();
        map.put("msg_user", userId);
        map.put("nike_name",userNickname);
        map.put("msg_content", message);
        getString(context, Http.HOMEPAFE_SENDGROUPCHAR, map, SENDGROUPCHAR, false, true, false);
    }
    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        switch (type){
            case RECEIVEGROUPCHAR://获取群聊消息
                ((ActivityDryquestionsView)view).getReceiveGroupChar(data);
                break;
            case SENDGROUPCHAR://获取群聊消息
                ((ActivityDryquestionsView)view).getSendGroupChar(data);
                break;
        }
    }

    @Override
    public void failString(String msg) {
        super.failString(msg);

    }
}
