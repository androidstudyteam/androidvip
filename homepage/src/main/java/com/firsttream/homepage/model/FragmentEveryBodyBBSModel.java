package com.firsttream.homepage.model;

import android.content.Context;

import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.homepage.view.ActivityEveryBodyBBSView;
import com.firsttream.homepage.view.FragmentEveryBodyBBSView;

import java.util.HashMap;
import java.util.Map;

/**
 * 作者:xjh
 * 时间:2018-12-14
 * 作用:ActivityEveryBodyBBSModel
 */
public class FragmentEveryBodyBBSModel extends BaseModel {
    private static final int RECEIVEGROUPCHAR = 0x129;
    private static final int RECEIVEHOT = 0x130;
    private static final int RECEIVEESSENCE = 0x131;
    private IContract.View view;

    /***
     * 获取全部论坛消息
     * @param context
     * @param view
     */
    public void getReceiveForum(Context context, IContract.View view, int size) {
        this.view = view;
        Map<String, String> map = new HashMap<>();
        map.put("size", size + "");
        getString(context, Http.HOMEPAFE_RECEIVEFORUM, map, RECEIVEGROUPCHAR, true, true, true);
    }

    /***
     * 获取热门
     * @param context
     * @param view
     */
    public void getReceiveHot(Context context, IContract.View view, int size) {
        this.view = view;
        Map<String, String> map = new HashMap<>();
        map.put("size", size + "");
        getString(context, Http.HOMEPAFE_RECEIVEHOT, map, RECEIVEHOT, false, true, false);
    }

    /***
     * 获取精华
     * @param context
     * @param view
     */
    public void getReceiveEssence(Context context, IContract.View view, int size) {
        this.view = view;
        Map<String, String> map = new HashMap<>();
        map.put("size", size + "");
        getString(context, Http.HOMEPAFE_RECEIVEESSENCE, map, RECEIVEESSENCE, false, true, false);
    }

    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        switch (type){
            case RECEIVEGROUPCHAR://获取全部论坛
                ((FragmentEveryBodyBBSView)view).getReceiveForum(data);
                break;
            case RECEIVEHOT://获取热门
                ((FragmentEveryBodyBBSView)view).getReceiveHot(data);
                break;
            case RECEIVEESSENCE://获取精华
                ((FragmentEveryBodyBBSView)view).getReceiveEssence(data);
                break;
        }
    }

    @Override
    public void failString(String msg) {
        super.failString(msg);

    }
}
