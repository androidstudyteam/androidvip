package com.firsttream.homepage.model;

import android.content.Context;

import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.baselibrary.util.SpUtil;
import com.firsttream.homepage.view.ActivityPlayAPlayView;

import java.util.HashMap;
import java.util.Map;
/**
 * 作者:xjh
 * 时间:2018-12-14
 * 作用:ActivityPlayAPlayModel
 */
public class ActivityPlayAPlayModel extends BaseModel {

    private IContract.View view;
    private static final int RECEIVEPLAYAPALYURL = 0x125;
    private static final int SENDPLAYAPALYURL = 0x126;

    public void getReceivePlayaplayurl(Context context, IContract.View view, int page) {
        this.view = view;

        Map<String, String> map = new HashMap<>();
        map.put("open_id", 100 + "");
        map.put("open_page",page+"" );
        map.put("open_num", 15+ "");
        getString(context, Http.HOMEPAFE_RECEIVEPLAYAPALYURL, map, RECEIVEPLAYAPALYURL, true, true, true);
    }

    public void getSendPlayaplayurl(Context context, IContract.View view,String message) {
        this.view = view;
        String userNickname = (String) SpUtil.getSpData(context, "userNickname", "");
        String userId = (String) SpUtil.getSpData(context, "userId", "");
        Map<String, String> map = new HashMap<>();
        map.put("open_id", 100+"");
        map.put("open_user", userId);
        map.put("open_nickname",userNickname);
    }


    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        switch (type) {
            case RECEIVEPLAYAPALYURL:
                ((ActivityPlayAPlayView) view).getReceivePlayaplayurl(data);
                break;
            case SENDPLAYAPALYURL:
                ((ActivityPlayAPlayView) view).getSendPlayaplayurl(data);
                break;
        }
    }

    @Override
    public void failString(String msg) {
        super.failString(msg);
        Logger.d("TAG",msg.toString());
    }
}