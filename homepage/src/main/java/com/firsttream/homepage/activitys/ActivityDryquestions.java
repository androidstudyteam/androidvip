package com.firsttream.homepage.activitys;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;

import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.gif.GifLayoutView;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.baselibrary.util.SpUtil;
import com.firsttream.homepage.R;
import com.firsttream.homepage.adapaters.DryquestionsAdapter;
import com.firsttream.homepage.entity.DryquestionBean;
import com.firsttream.homepage.entity.SendBean;
import com.firsttream.homepage.model.ActivityDryquestionsModel;
import com.firsttream.homepage.persenter.ActivityDryquestionsPersenter;
import com.firsttream.homepage.view.ActivityDryquestionsView;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.scwang.smartrefresh.header.BezierCircleHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者:xjh
 * 时间:2018-12-14
 * 作用:ActivityDryquestions
 */
public class ActivityDryquestions<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> implements ActivityDryquestionsView, GifLayoutView.SendMessageListener {
    private final String TAG = ActivityDryquestions.class.getName();
    private ActivityDryquestionsPersenter mPersenter;
    private Context mContext;
    private XRecyclerView mXRecyclerDryquestions;
    private  int page = 1;
    private DryquestionsAdapter mDryquestionsAdapter;
    private GifLayoutView mViewDryquestionsGif;
    private List<DryquestionBean.ItemsBean> mLastData=new ArrayList<>();
    private RefreshLayout mDryQuestionsRF;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_dryquestions;
    }

    @Override
    public void initData() {
        super.initData();
        Boolean isLogin = (Boolean) SpUtil.getSpData(mContext, "isLogin", false);
        if (!isLogin) {
            toase("还没登录哦！", 2);
            return;
        }
        setTitle("群聊");
        initWidget();
        mPersenter.getReceiveGroupChar(page);
    }

    private void initWidget() {
        mXRecyclerDryquestions = (XRecyclerView) getView(R.id.homepage_xrecycler_dryquestions);
        mViewDryquestionsGif = (GifLayoutView) getView(R.id.homepage_view_dryquestionsgif);
        mDryQuestionsRF = (RefreshLayout) getView(R.id.homepage_refresh_dryquestionsrf);
        mViewDryquestionsGif.sendMessage(this);
        mDryquestionsAdapter = new DryquestionsAdapter(mContext);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mXRecyclerDryquestions.setLayoutManager(linearLayoutManager);
        mXRecyclerDryquestions.setAdapter(mDryquestionsAdapter);
        mXRecyclerDryquestions.setPullRefreshEnabled(false);
        mXRecyclerDryquestions.setLoadingMoreEnabled(false);
        //设置 Header 为 贝塞尔雷达 样式
        mDryQuestionsRF.setRefreshHeader(new BezierCircleHeader(mContext)).setPrimaryColorsId(R.color.colorYellow);
        //设置 Footer 为 球脉冲 样式
        mDryQuestionsRF.setRefreshFooter(new BallPulseFooter(mContext).setSpinnerStyle(SpinnerStyle.Scale));
        mDryQuestionsRF.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                page+=1;
                mPersenter.getReceiveGroupChar(page);
                refreshlayout.finishRefresh(1000/*,false*/);//传入false表示刷新失败
            }
        });

        mDryQuestionsRF.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                page--;
                if (page==0){
                    toase("没有更多数据了",2);
                }
                mPersenter.getReceiveGroupChar(page);
                refreshlayout.finishLoadMore(1000/*,false*/);//传入false表示加载失败
            }
        });
    }

    @Override
    public M createModel() {
        return (M) new ActivityDryquestionsModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new ActivityDryquestionsPersenter();
        mPersenter.initContext(this);
        return (P) mPersenter;
    }


    @Override
    public void initContext(Context context) {
        super.initContext(context);
        this.mContext = context;
    }
    /***
     * 获取群聊消息
     * @param data
     */
    @Override
    public void getReceiveGroupChar(String data) {
        Gson gson = new Gson();
        DryquestionBean dryquestionBean = gson.fromJson(data, DryquestionBean.class);
        mDryquestionsAdapter.setList(dryquestionBean.getItems());
        for (int i = dryquestionBean.getItems().size() - 1; i >=0; i--) {
            mLastData.add(dryquestionBean.getItems().get(i));
        }
        Logger.i(TAG, data);
        Logger.d(TAG, dryquestionBean.getItems().size() + "");
        mDryquestionsAdapter.setList(mLastData);
    }
    /***
     * 发送群聊消息
     * @param data
     */
    @Override
    public void getSendGroupChar(String data) {

        Gson gson = new Gson();
        SendBean sendBean = gson.fromJson(data, SendBean.class);
        if (0 == sendBean.getStatus()) {
            toase("发送成功", 2);
            mPersenter.getReceiveGroupChar(page);
        } else {
            toase("发送失败", 2);
        }
    }


    @Override
    public void send(String message) {

        if (TextUtils.isEmpty(message)) {
            toase("请输入内容,", 2);
        } else {
            mPersenter.getSendGroupChar(message);
            Logger.d("ActivityPlayAPlayTAG", message);
        }
    }
}
