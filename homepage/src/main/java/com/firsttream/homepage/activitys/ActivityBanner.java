package com.firsttream.homepage.activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.homepage.R;
import com.firsttream.homepage.model.ActivityBannerModel;
import com.firsttream.homepage.persenter.ActivityBannerPersenter;
import com.firsttream.homepage.view.ActivityBannerView;
/**
 * 作者:xjh
 * 时间:2018-12-14
 * 作用:ActivityBanner
 */
public class ActivityBanner<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> implements ActivityBannerView {
    private final String TAG = ActivityBanner.class.getName();
    private ActivityBannerPersenter mpersenter;
    private Context mcontext;
    private WebView mwebviewweb;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_banner;
    }

    @Override
    public void initData() {
        super.initData();
        setTitle("详情");
        initWidget();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String desc = bundle.getString("homepagedesc");
        mwebviewweb.loadUrl(desc);

    }

    private void initWidget() {
        mwebviewweb = (WebView) getView(R.id.homepage_webview_myweb);
        mwebviewweb.setWebViewClient(new WebViewClient());
        mwebviewweb.getSettings().setJavaScriptEnabled(true);
    }

    @Override
    public M createModel() {
        return (M) new ActivityBannerModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mpersenter = new ActivityBannerPersenter();
        return (P) mpersenter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mwebviewweb.destroy();

    }

    @Override
    public void initContext(Context context) {
        super.initContext(context);
        this.mcontext = context;
    }
}
