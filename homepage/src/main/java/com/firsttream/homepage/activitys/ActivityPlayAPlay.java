package com.firsttream.homepage.activitys;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;

import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.gif.GifLayoutView;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.baselibrary.util.SpUtil;
import com.firsttream.homepage.R;
import com.firsttream.homepage.adapaters.SpeakAdapter;
import com.firsttream.homepage.entity.SendBean;
import com.firsttream.homepage.entity.SpeakBean;
import com.firsttream.homepage.model.ActivityPlayAPlayModel;
import com.firsttream.homepage.persenter.ActivityPlayAPlayPersenter;
import com.firsttream.homepage.view.ActivityPlayAPlayView;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.scwang.smartrefresh.header.BezierCircleHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者:xjh
 * 时间:2018-12-15
 * 作用:ActivityPlayAPlay
 */
public class ActivityPlayAPlay<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> implements ActivityPlayAPlayView, GifLayoutView.SendMessageListener {
    private final String TAG = ActivityPlayAPlay.class.getName();
    private ActivityPlayAPlayPersenter mPersenter;
    private XRecyclerView mPlayspeak;
    private Context mContext;
    private SpeakAdapter mSpeakAdapter;
    private GifLayoutView mGifview;
    private int page = 1;
    private List<SpeakBean.ItemsBean> mLastData= new ArrayList<>();
    private RefreshLayout mPlayAPlayRF;

    @Override

    protected int getLayoutId() {
        return R.layout.activity_playaplay;
    }

    @Override
    public void initData() {
        super.initData();
        setTitle("弹一弹");
        initWidget();
        //弹一弹获取消息
        mPersenter.getReceivePlayaplayurl(page);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mPlayspeak.setLayoutManager(linearLayoutManager);
        mSpeakAdapter = new SpeakAdapter(mContext);
        mPlayspeak.setAdapter(mSpeakAdapter);
    }

    private void initWidget() {
        mPlayspeak = (XRecyclerView) getView(R.id.homepage_xrecycler_playspeak);
        mGifview = (GifLayoutView) getView(R.id.homepage_view_gif);
        mPlayAPlayRF = (RefreshLayout) getView(R.id.homepage_refresh_playaplayrf);
        mGifview.sendMessage(this);
        mPlayspeak.setPullRefreshEnabled(false);
        mPlayspeak.setLoadingMoreEnabled(false);
        //设置 Header 为 贝塞尔雷达 样式
        mPlayAPlayRF.setRefreshHeader(new BezierCircleHeader(mContext)).setPrimaryColorsId(R.color.colorYellow);
        //设置 Footer 为 球脉冲 样式
        mPlayAPlayRF.setRefreshFooter(new BallPulseFooter(mContext).setSpinnerStyle(SpinnerStyle.Scale));
        mPlayAPlayRF.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                page+=1;
                mPersenter.getReceivePlayaplayurl(page);;
                refreshlayout.finishRefresh(1000/*,false*/);//传入false表示刷新失败
            }
        });

        mPlayAPlayRF.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                page--;
                if (page==0){
                    toase("没有更多数据了",2);
                }
                mPersenter.getReceivePlayaplayurl(page);
                refreshlayout.finishLoadMore(1000/*,false*/);//传入false表示加载失败
            }
        });
    }

    @Override
    public M createModel() {
        return (M) new ActivityPlayAPlayModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new ActivityPlayAPlayPersenter();
        mPersenter.initContext(this);
        return (P) mPersenter;
    }


    /***
     * 弹一弹获取消息
     * @param data
     */
    @Override
    public void getReceivePlayaplayurl(String data) {
        Gson gson = new Gson();
        SpeakBean speakBean = gson.fromJson(data, SpeakBean.class);
        Logger.d("Tagger",">>>>>>>"+speakBean.getItems().get(0).getOpen_content());
        mLastData.clear();
        for (int i = speakBean.getItems().size() - 1; i >=0; i--) {
            mLastData.add(speakBean.getItems().get(i));
        }
        Logger.i(TAG, data);
        Logger.d(TAG, speakBean.getItems().size() + "");
        mSpeakAdapter.setList(mLastData);
    }

    /***
     * 弹一弹发送消息
     * @param data
     */
    @Override
    public void getSendPlayaplayurl(String data) {
        Gson gson = new Gson();
        SendBean sendBean = gson.fromJson(data, SendBean.class);
        if (0 == sendBean.getStatus()) {
            toase("发送成功", 2);
            mPersenter.getReceivePlayaplayurl(page);
        } else {
            toase("发送失败", 2);
        }
    }

    @Override
    public void initContext(Context context) {
        super.initContext(context);
        this.mContext = context;
    }

    @Override
    public void send(String message) {
        Boolean isLogin = (Boolean) SpUtil.getSpData(mContext, "isLogin", false);
        if (!isLogin) {
            toase("还没登录哦！", 2);
            return;
        }
        if (TextUtils.isEmpty(message)) {
            toase("请输入内容,", 2);
        } else {
            mPersenter.getSendPlayaplayurl(message);
            Logger.d("ActivityPlayAPlayTAG",message);
        }
    }
}
