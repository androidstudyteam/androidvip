package com.firsttream.homepage.activitys;

import android.content.Context;

import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.homepage.R;
import com.firsttream.homepage.model.ActivityVipDrayCargoModel;
import com.firsttream.homepage.persenter.ActivityVipDrayCargoPersenter;
import com.firsttream.homepage.view.ActivityVipDrayCargoView;

/**
 * 作者:xjh
 * 时间:2018-12-15
 * 作用:ActivityVipDrayCargo
 */
public class ActivityVipDrayCargo<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> implements ActivityVipDrayCargoView {
    private final String TAG = ActivityVipDrayCargo.class.getName();
    private ActivityVipDrayCargoPersenter mPersenter;
    private Context mContext;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_vipdraycargo;
    }

    @Override
    public void initData() {
        super.initData();
        setTitle("Vip干货铺");
        initWidget();

    }

    private void initWidget() {

    }

    @Override
    public M createModel() {
        return (M) new ActivityVipDrayCargoModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new ActivityVipDrayCargoPersenter();
        mPersenter.initContext(mContext);
        return (P) mPersenter;
    }


    @Override
    public void initContext(Context context) {
        super.initContext(context);
        this.mContext = context;
    }


}
