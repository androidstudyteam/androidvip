package com.firsttream.homepage.activitys;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.widget.TextView;
import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.SpUtil;
import com.firsttream.homepage.R;
import com.firsttream.homepage.model.ActivityQrCodeModel;
import com.firsttream.homepage.persenter.ActivityQrCodePersenter;
import com.firsttream.homepage.view.ActivityQrCodeView;
import com.google.zxing.WriterException;
import com.yzq.zxinglibrary.encode.CodeCreator;

/**
 * 作者:xjh
 * 时间:2018-12-12
 * 作用:ActivityQrCode(二維碼)
 */
public class ActivityQrCode<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> implements ActivityQrCodeView {

    private final String TAG = ActivityQrCode.class.getName();
    private ActivityQrCodePersenter mPersenter;
    private Context mContext;
    private ImageView mImageView,mImagePic;
    private TextView mTxtNickName;


    @Override
    protected int getLayoutId() {
        return R.layout.activiity_qr_code;
    }


    @Override
    public void initData() {
        super.initData();
        setTitle("我的二维码");
        initWidget();
        String userName = (String) SpUtil.getSpData(mContext, "userName", "");
        String userId = (String) SpUtil.getSpData(mContext, "userId", "");
        setCaptrue("{'user_id':" + userId + ",'user_name':" + userName + "}");
        mTxtNickName.setText(userName);
    }

    private void initWidget() {
         mImageView = (ImageView) getView(R.id.homepage_img_qrcode);
         mTxtNickName = (TextView) getView(R.id.homepage_txt_qrcodenickname);
        mImagePic = (ImageView) getView(R.id.homepage_img_qrcodepic);

    }
    //生成二維碼
    private void setCaptrue(String content) {
        Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher);
        try {
            Bitmap bit = CodeCreator.createQRCode(content, 200, 200, bitmap);
            mImageView.setImageBitmap(bit);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    @Override
    public M createModel() {
        return (M) new ActivityQrCodeModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new ActivityQrCodePersenter();
        mPersenter.initContext(this);
        return (P) mPersenter;
    }

    @Override
    public void initContext(Context context) {
        super.initContext(context);
        this.mContext = context;
    }
}
