package com.firsttream.homepage.activitys;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.homepage.R;
import com.firsttream.homepage.fragments.FragmentEveryBodyBBS;
import com.firsttream.homepage.model.ActivityEveryBodyBBSModel;
import com.firsttream.homepage.persenter.ActivityEveryBodyBBSPersenter;
import com.firsttream.homepage.view.ActivityEveryBodyBBSView;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者:xjh
 * 时间:2018-12-14
 * 作用:ActivityEveryBodyBBS
 */
public class ActivityEveryBodyBBS<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> implements ActivityEveryBodyBBSView {
    private final String TAG = ActivityEveryBodyBBS.class.getName();
    private ActivityEveryBodyBBSPersenter mPersenter;
    private Context mContext;
    private TabLayout mTablayout;
    private ViewPager mViewpager;
    private int[] flags = {0,1,2};

    @Override
    protected int getLayoutId() {
        return R.layout.activity_everybodybbs;
    }

    @Override
    public void initData() {
        super.initData();
        setTitle("大家论坛");
        initWidget();
    }

    private void initWidget() {
        mTablayout = (TabLayout) getView(R.id.homepage_tablayout_tab);
        mViewpager = (ViewPager) getView(R.id.homepage_viewpager_mvp);
        final String[] titles = {"全部论坛", "精选", "热门"};
        mViewpager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
              return getReuse(flags[i]);
            }
            @Override
            public int getCount() {
                return flags.length;
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return titles[position];
            }
        });
        mViewpager.setOffscreenPageLimit(3);
        mTablayout.setupWithViewPager(mViewpager);
    }

    @Override
    public M createModel() {
        return (M) new ActivityEveryBodyBBSModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new ActivityEveryBodyBBSPersenter();
        mPersenter.initContext(this);
        return (P) mPersenter;
    }


    @Override
    public void initContext(Context context) {
        super.initContext(context);
        this.mContext = context;
    }


    //复用
    public static Fragment getReuse(int flag) {
        Fragment fragment = new FragmentEveryBodyBBS();
        Bundle bundle = new Bundle();
        bundle.putInt("flag", flag);
        fragment.setArguments(bundle);
        return fragment;
    }
}
