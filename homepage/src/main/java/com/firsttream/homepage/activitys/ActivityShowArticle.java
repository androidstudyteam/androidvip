package com.firsttream.homepage.activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.homepage.R;
import com.firsttream.homepage.model.ActivityBannerModel;
import com.firsttream.homepage.persenter.ActivityBannerPersenter;
import com.firsttream.homepage.view.ActivityBannerView;


/**
 * 作者:xjh
 * 时间:2018-12-14
 * 作用:ActivityShowArticle
 */
public class ActivityShowArticle<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> implements ActivityBannerView {
    private final String TAG = ActivityShowArticle.class.getName();
    private ActivityBannerPersenter mPersenter;
    private Context mContext;
    private WebView mWebViewWeb;
    private String desc;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_banner;
    }

    @Override
    public void initData() {
        super.initData();
        setTitle("详情");
        initWidget();

    }

    private void initWidget() {
        mWebViewWeb = (WebView) getView(R.id.homepage_webview_myweb);
        mWebViewWeb.setWebViewClient(new WebViewClient());
        StringBuilder sb = new StringBuilder();
        sb.append(getHtmlData(desc));
        mWebViewWeb.loadDataWithBaseURL(null,sb.toString(),"text/html", "utf-8", null);
    }

    private String getHtmlData(String bodyHTML) {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String head = desc = bundle.getString("msgContent");
        return "<html>" + head + "<body>" + bodyHTML + "</body></html>";
    }

    @Override
    public M createModel() {
        return (M) new ActivityBannerModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new ActivityBannerPersenter();
        mPersenter.initContext(mContext);
        return (P) mPersenter;
    }

    @Override
    public void initContext(Context context) {
        super.initContext(context);
        this.mContext = context;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWebViewWeb.destroy();
    }
}
