package com.firsttream.homepage.activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.homepage.R;
import com.firsttream.homepage.model.ActivityBannerModel;
import com.firsttream.homepage.persenter.ActivityBannerPersenter;
import com.firsttream.homepage.view.ActivityBannerView;

/**
 * 作者:xjh
 * 时间:2018-12-14
 * 作用:ActivityHomePageItem
 */
public class ActivityHomePageItem<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> implements ActivityBannerView {
    private final String TAG = ActivityHomePageItem.class.getName();
    private ActivityBannerPersenter mPersenter;
    private Context mContext;
    private WebView mWebViewWeb;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_banner;
    }

    @Override
    public void initData() {
        super.initData();
        setTitle("详情");
        initWidget();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String desc = bundle.getString("itemsBeanDesc");
        mWebViewWeb.loadUrl(desc);

    }

    private void initWidget() {
        mWebViewWeb = (WebView) getView(R.id.homepage_webview_myweb);
        mWebViewWeb.setWebViewClient(new WebViewClient());
        mWebViewWeb.getSettings().setJavaScriptEnabled(true);
    }

    @Override
    public M createModel() {
        return (M) new ActivityBannerModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new ActivityBannerPersenter();
        mPersenter.initContext(mContext);
        return (P) mPersenter;
    }


    @Override
    public void initContext(Context context) {
        super.initContext(context);
        this.mContext = context;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWebViewWeb.destroy();
    }
}
