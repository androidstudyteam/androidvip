package com.firsttream.classic.mvp.persenter;

import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.classic.mvp.modle.ClassicActivityModle;
import com.firsttream.classic.mvp.view.ClassicActivityView;

//继承 base Persenter
public class ClassicActivityPersenter extends BasePersenter<ClassicActivityModle,ClassicActivityView>{

    //删了第二个生成的方法
    @Override
    protected void onViewDestory() {
        //判断modle非空
        if(model!=null){
            //非空就调用停止方法
            model.stopRequest();
        }
    }


}
