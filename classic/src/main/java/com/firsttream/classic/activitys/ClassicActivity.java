package com.firsttream.classic.activitys;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.classic.R;
import com.firsttream.classic.mvp.modle.ClassicActivityModle;
import com.firsttream.classic.mvp.persenter.ClassicActivityPersenter;
import com.firsttream.classic.mvp.view.ClassicActivityView;

/*
* 作者:马富燕
* 日期:2018/12/11
* 作用:经典的主Activity ClassicActivity
* */

//先写泛型在前面 分别继承自己定义的关于这个类的model view  persenter 在继承父类activity  在实现本类的view接口 实现方法
public class ClassicActivity<M extends ClassicActivityModle,V extends ClassicActivityView,P extends ClassicActivityPersenter> extends BaseActivity implements ClassicActivityView {

    private ClassicActivityPersenter mClassicActivityPersenter;
    private WebView mClassicactivityweb;
    private ProgressDialog dialog;

    @Override
    protected int getLayoutId() {
        //返回本页面布局
        return R.layout.activity_classic;
    }

    @Override
    public IContract.Model createModel() {
        //new 关于本类的modle 不报错就不用强转
        return new ClassicActivityModle();
    }

    @Override
    public IContract.View createView() {
        //直接返回this 不报错就不用强转
        return this;
    }

    @Override
    public BasePersenter createPersenter() {
        //先new 本类的persenter  返回值提上去
        mClassicActivityPersenter = new ClassicActivityPersenter();
         //设置一下初始化上下文 传上下文
        //直接返回persenter
        return mClassicActivityPersenter;
    }

    //重写初始化数据方法
    @Override
    public void initData() {
        super.initData();
        //获取意图值
        Intent intent = getIntent();
        //获取标题值和网址值
        String mWeburl = intent.getStringExtra("weburl");
        //设置标题
        setTitle("详情");
        //获取控件强转 提上去
        mClassicactivityweb=(WebView)findViewById(R.id.classic_activity_web);
        //设置支持JS
        mClassicactivityweb.getSettings().setJavaScriptEnabled(true);
        mClassicactivityweb.getSettings().setAppCacheEnabled(true);
        //设置 缓存模式
        mClassicactivityweb.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        // 开启 DOM storage API 功能
        mClassicactivityweb.getSettings().setDomStorageEnabled(true);
        //设置url
        mClassicactivityweb.loadUrl(mWeburl);
        //设置不跳浏览器
        mClassicactivityweb.setWebViewClient(new WebViewClient() {
            //重写shouldOverrideUrlLoading 方法
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        //创建一个ProgressDialog
        dialog = ProgressDialog.show(this, "正在加载…", null);
       //设置进度监听
        mClassicactivityweb.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                toase("加载完毕~",1);
                dialog.dismiss();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                toase("正在加载中~",1);
            }
        });
    }

    //销毁webview
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //销毁webview
        mClassicactivityweb.destroy();
    }
}
