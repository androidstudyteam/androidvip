package com.firsttream.video.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.firsttream.baselibrary.base.BaseFragment;
import com.firsttream.video.R;
import com.firsttream.video.adapter.MoreVideoDescAdapter;
import com.firsttream.video.entity.MoreVideoDescBean;
import com.firsttream.video.mvp.modle.FragmentMoreDescModle;
import com.firsttream.video.mvp.persenter.FragmentMoreDescPersenter;
import com.firsttream.video.mvp.view.FragmentMoreDescView;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.scwang.smartrefresh.header.BezierCircleHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

/**
 * 作者:马富燕
 * 日期:2018/12/18
 * 作用:tablayout内容的fragment FragmentMoreVideoDesc
 */
//如果需要用到网络请求 就继承自己创建类的modle view persenter 如果不用继承原先的就行 在实现一下这个类相关的view接口
public class FragmentMoreVideoDesc<M extends FragmentMoreDescModle, V extends FragmentMoreDescView, P extends FragmentMoreDescPersenter> extends BaseFragment<M, V, P> implements FragmentMoreDescView {

    private FragmentMoreDescPersenter mFragmentMoreDescPersenter;
    private RecyclerView mMoreviDeorv;
    private String mType;
    private RefreshLayout mMoreviDeoitemreFreshLayout;

    @Override
    protected int getLayoutId() {
        //返回页面布局
        return R.layout.morevideo_desc_item;
    }

    @Override
    public M createModel() {
        //new 强转
        return (M) new FragmentMoreDescModle();
    }

    @Override
    public V createView() {
        //返回this  强转
        return (V) this;
    }

    @Override
    public P createPersenter() {
        //返回 new
        mFragmentMoreDescPersenter = new FragmentMoreDescPersenter();
        //初始化上下文
        mFragmentMoreDescPersenter.initContext(getActivity());
        //返回persenter  强转
        return (P) mFragmentMoreDescPersenter;
    }


    //重写初始化数据方法
    @Override
    public void initData() {
        super.initData();
        //给标题设置showbar 设置false为假隐藏
        setShowBar(false);
        //获取  argument
        Bundle bundle = getArguments();
        //取值  自己强转字符型
        mType = (String) bundle.get("type");
        //初始化数据方法
        initweidget();
        //new 两列网格管理器
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        //给recycler设置
        mMoreviDeorv.setLayoutManager(gridLayoutManager);
        //请求网络数据方法
        mFragmentMoreDescPersenter.getRvDesc(mType);
        //设置 Header 为 贝塞尔雷达 样式
        mMoreviDeoitemreFreshLayout.setRefreshHeader(new BezierCircleHeader(getActivity())).setPrimaryColorsId(R.color.holo_orange_light);
        //设置 Footer 为 球脉冲 样式
        mMoreviDeoitemreFreshLayout.setRefreshFooter(new BallPulseFooter(getActivity()).setSpinnerStyle(SpinnerStyle.Scale));
        mMoreviDeoitemreFreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                //网络请求
                mFragmentMoreDescPersenter.getRvDesc(mType);
                refreshlayout.finishRefresh(1000/*,false*/);//传入false表示刷新失败
            }
        });
        mMoreviDeoitemreFreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                //网络请求
                mFragmentMoreDescPersenter.getRvDesc(mType);
                refreshlayout.finishLoadMore(1000/*,false*/);//传入false表示加载失败
            }
        });
    }

    //初始化数据方法
    private void initweidget() {
        //获取控件强转提上去
        mMoreviDeorv=(RecyclerView)getView(R.id.morevideo_rv);
        mMoreviDeoitemreFreshLayout=(RefreshLayout)getView(R.id.morevideo_item_refreshLayout);
    }

    @Override
    public void getRvDesc(MoreVideoDescBean moreVideoDescBean) {
        //获取bean类集合
        List<MoreVideoDescBean.ItemsBean> items = moreVideoDescBean.getItems();
        //设置适配器
        MoreVideoDescAdapter moreVideoDescAdapter = new MoreVideoDescAdapter(getActivity());
        //设置集合
        moreVideoDescAdapter.setList(items);
        //给recycler设置适配器
        mMoreviDeorv.setAdapter(moreVideoDescAdapter);
    }
}
