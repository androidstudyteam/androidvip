package com.firsttream.video.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.firsttream.baselibrary.base.recycle.RecycleAdapter;
import com.firsttream.baselibrary.base.viewholder.ViewHolder;
import com.firsttream.video.R;
import com.firsttream.video.activitys.MoreDescActivity;
import com.firsttream.video.activitys.VideoActivity;
import com.firsttream.video.entity.MoreVideoDescBean;

/**
 * 作者:马富燕
 * 日期:2018/12/19
 * 作用:更多视频tablayout内容的适配器
 */
//继承父类适配器 泛型写本页面的bean类  在生成方法
public class MoreVideoDescAdapter extends RecycleAdapter<MoreVideoDescBean.ItemsBean> {

    //删了final
    private Context mcontext;

    public MoreVideoDescAdapter(Context mcontext) {
        super(mcontext);
        //this.上下文=上下文提上去
        this.mcontext=mcontext;
    }

    @Override
    protected int getLayoutId() {
        //返回recycler条目布局
        return R.layout.morevideo_desc_rv_item;
    }

    @Override
    protected void convert(ViewHolder viewHolder, MoreVideoDescBean.ItemsBean itemsBean, int postion) {
        //赋值给图片
        viewHolder.setSimpleDraweViewUrl(R.id.morevideo_rv_img,itemsBean.getImage());
        //赋值标题
        viewHolder.setText(R.id.morevideo_rv_title,itemsBean.getTitle());
        //条目点击视频意图传位置跳转Activity
        viewHolder.setClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //创建意图传位置跳转Activity
                //跳转Activity
                //new 意图
                Intent intent = new Intent(mcontext, MoreDescActivity.class);
                //我那个意图里传位置和标题
                intent.putExtra("positionid",itemsBean.getPosition());
                intent.putExtra("titles",itemsBean.getTitle());
                intent.putExtra("videoimgae",itemsBean.getImage());
                //上下文启动跳转 传意图
                mcontext.startActivity(intent);
            }
        }, R.id.morevideo_rv_layout);
    }

}