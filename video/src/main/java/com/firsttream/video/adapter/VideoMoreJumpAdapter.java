package com.firsttream.video.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firsttream.baselibrary.base.recycle.RecycleAdapter;
import com.firsttream.baselibrary.base.viewholder.ViewHolder;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.video.R;
import com.firsttream.video.entity.MoreJumpBean;
import com.firsttream.video.entity.VideoItemBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者:马富燕
 * 日期:2018/12/19
 * 作用:更多视频列表的适配器
 */
//继承父类适配器 泛型写该有的bean类
public class VideoMoreJumpAdapter extends RecyclerView.Adapter<VideoMoreJumpAdapter.MyViewHolder> {

    private Context mContext;
    private View mView;
    private SetOnitem mSetOnitem;

    public VideoMoreJumpAdapter(Context context) {
        this.mContext = context;
    }

    private List<MoreJumpBean.VideoListBean> list = new ArrayList<>();

    public void setList(List<MoreJumpBean.VideoListBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mView = View.inflate(mContext, R.layout.video_desc_rv_item, null);
        MyViewHolder myViewHolder = new MyViewHolder(mView);
        myViewHolder.video_desc_layout=mView.findViewById(R.id.video_desc_layout);
        myViewHolder.video_video_rv_title=mView.findViewById(R.id.video_video_rv_title);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        //赋值给控件
        myViewHolder.video_video_rv_title.setText(list.get(i).getVideo_title());
        //判断换色
        if(list.get(i).isClick()){
             myViewHolder.video_video_rv_title.setTextColor(Color.RED);
        }else {
            myViewHolder.video_video_rv_title.setTextColor(Color.BLACK);
        }
      myViewHolder.video_desc_layout.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              //改变字体颜色的bean先
               for (int a=0;a<list.size();a++){
                   if(i==a){
                       list.get(a).setClick(true);
                   }else {
                       list.get(a).setClick(false);
                   }
               }
              list.get(i).setClick(true);
              //先打印每次点击的
//                Logger.i("条目视频的url", "哈哈" + videoListBean.getVideo_url());
              mSetOnitem.success(list.get(i).getVideo_url());
              notifyDataSetChanged();
          }
      });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout video_desc_layout;
        TextView video_video_rv_title;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
    //接口回调方法
    public void result(SetOnitem setOnitem){
        //this点接口等于接口
        this.mSetOnitem=setOnitem;
    }
    //接口
    public interface SetOnitem {
        //成功方法传字符串视频地址
        void success(String stock);
    }
}