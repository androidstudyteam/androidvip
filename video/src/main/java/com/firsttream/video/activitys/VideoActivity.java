package com.firsttream.video.activitys;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.DimUtil;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.baselibrary.util.NetUtil;
import com.firsttream.baselibrary.util.SpUtil;
import com.firsttream.video.R;
import com.firsttream.video.adapter.VideoItemAdapter;
import com.firsttream.video.entity.AddDanmaKuBean;
import com.firsttream.video.entity.DanmakuBean;
import com.firsttream.video.entity.VideoItemBean;
import com.firsttream.video.entity.VideoUrlBean;
import com.firsttream.video.mvp.modle.VideoActivityModle;
import com.firsttream.video.mvp.persenter.VideoActivityPersenter;
import com.firsttream.video.mvp.view.VideoActivityView;
import com.firsttream.video.utiles.AcFunDanmakuParser;
import com.firsttream.video.utiles.BiliDanmukuParser;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.scwang.smartrefresh.header.BezierCircleHeader;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.header.WaterDropHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import chuangyuan.ycj.videolibrary.listener.VideoInfoListener;
import chuangyuan.ycj.videolibrary.video.ExoUserPlayer;
import chuangyuan.ycj.videolibrary.video.VideoPlayerManager;
import chuangyuan.ycj.videolibrary.widget.VideoPlayerView;
import master.flame.danmaku.controller.IDanmakuView;
import master.flame.danmaku.danmaku.loader.ILoader;
import master.flame.danmaku.danmaku.loader.IllegalDataException;
import master.flame.danmaku.danmaku.loader.android.DanmakuLoaderFactory;
import master.flame.danmaku.danmaku.model.BaseDanmaku;
import master.flame.danmaku.danmaku.model.DanmakuTimer;
import master.flame.danmaku.danmaku.model.IDanmakus;
import master.flame.danmaku.danmaku.model.IDisplayer;
import master.flame.danmaku.danmaku.model.android.DanmakuContext;
import master.flame.danmaku.danmaku.model.android.Danmakus;
import master.flame.danmaku.danmaku.parser.BaseDanmakuParser;
import master.flame.danmaku.danmaku.parser.IDataSource;
import master.flame.danmaku.ui.widget.DanmakuView;

/*
 * 作者:马富燕&高佳宝
 * 日期:2018/12/11
 * 作用:视频的主Activity VideoActivity
 * */
//先是泛型分别继承这个类的m  v  p 在继承baseActivity 在实现自己的view接口
public class VideoActivity<M extends VideoActivityModle, V extends VideoActivityView, P extends VideoActivityPersenter> extends BaseActivity implements VideoActivityView {
    private VideoActivityPersenter mVideoActivityPersenter;
    private XRecyclerView mVideoXRecyclerView;
    private VideoItemAdapter mVideoItemAdapter;
    private TextView mVideoHeadDesc;
    private String mPositionId, mVideoImgae;
    private VideoPlayerView mVideoPlayerView;
    private DanmakuView mDanmakuView;
    private ImageView mSendText, mShowandhide;
    private DanmakuContext mContext;
    private EditText mVideoHeaded;
    private boolean isHideDanmu = false;
    private ExoUserPlayer mExoUserPlayer;
    //    private BaseDanmakuParser mParser;
    private AcFunDanmakuParser mParser;
    private int page = 1;
    private RelativeLayout exora;
    private static final String TAG = "VideoActivity";
    private String mVideoUrl,mTitle,mUserId,mUserName;
    private TextView mVideoHeadte;
    private boolean isBoFang = false;
    private RefreshLayout mTwoVideoFreshLayout;
    /**
     * 没有连接网络
     */
    private static final int NETWORK_NONE = -1;
    /**
     * 移动网络
     */
    private static final int NETWORK_MOBILE = 0;
    /**
     * 无线网络
     */
    private static final int NETWORK_WIFI = 1;
    @Override
    protected int getLayoutId() {
        //返回本页面布局
        return R.layout.activity_video;
    }

    @Override
    public IContract.Model createModel() {
        //返回new modle 强转
        return new VideoActivityModle();
    }

    @Override
    public IContract.View createView() {
        //直接返回this  强转
        return this;
    }

    @Override
    public BasePersenter createPersenter() {
        //new persnetr 返回值 提上去
        mVideoActivityPersenter = new VideoActivityPersenter();
        //记得要写初始化上下文 括号里传上下文
        mVideoActivityPersenter.initContext(this);
        return mVideoActivityPersenter;
    }

    //重写初始化数据方法
    @Override
    public void initData() {
        super.initData();
        //获取意图值
        Intent intent = getIntent();
        //获取位置值和标题值
        mPositionId = intent.getStringExtra("positionid");
        String titles = intent.getStringExtra("titles");
        mVideoImgae = intent.getStringExtra("videoimgae");
        //获取用户id 和用户昵称
        mUserId = (String) SpUtil.getSpData(this, "userId", "");
        mUserName = (String) SpUtil.getSpData(this, "userName", "");
        Logger.i("我的用户名和密码", "" + mUserId + "好不会吧" + mUserName);
        //        //请求网络的方法 调用videopersenter本类的
        mVideoActivityPersenter.getItem(mPositionId);
        //吐司位置
        //     toase("哈哈"+positionid,1);
        //设置标题
        setTitle(titles);
        //初始化数据方法
        initwidget();
        //new 线性管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        //设置方向
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //给recycler设置线性管理器
        mVideoXRecyclerView.setLayoutManager(linearLayoutManager);
        //停止上拉和下拉刷新
        mVideoXRecyclerView.setPullRefreshEnabled(false);
        //设置 Header 为 贝塞尔雷达 样式
        mTwoVideoFreshLayout.setRefreshHeader(new WaterDropHeader(this));
        //设置 Footer 为 球脉冲 样式
        mTwoVideoFreshLayout.setRefreshFooter(new BallPulseFooter(this).setSpinnerStyle(SpinnerStyle.Scale));
        mTwoVideoFreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                //        //请求网络的方法 调用videopersenter本类的
                mVideoActivityPersenter.getItem(mPositionId);
                refreshlayout.finishRefresh(1000/*,false*/);//传入false表示刷新失败
            }
        });
        mTwoVideoFreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                //        //请求网络的方法 调用videopersenter本类的
                mVideoActivityPersenter.getItem(mPositionId);
                refreshlayout.finishLoadMore(1000/*,false*/);//传入false表示加载失败
            }
        });
    }

    //初始化播放器
    private void initExoPlay() {
        mExoUserPlayer = new VideoPlayerManager.Builder(VideoPlayerManager.TYPE_PLAY_GESTURE, mVideoPlayerView)
                .setPlayUri(mVideoUrl)//设置地址
                .setTitle(mTitle)//标题
                .setPlayerGestureOnTouch(true)
                .setShowVideoSwitch(true)
                .create();//创建
//                        .setVerticalFullScreen(true)//开启竖屏全屏
        //判断网络状态
        int netWorkState = NetUtil.getNetWorkState(this);
        if(netWorkState==NETWORK_NONE){//没网状态
//            mExoUserPlayer.onPause();
            toase("网络好像断开了~",1);
            return;
        }else if(netWorkState==NETWORK_MOBILE){//移动网络状态
            mExoUserPlayer.onPause();
            toase("当前为移动网络，注意流量资费~",1);
            return;
        }else if(netWorkState==NETWORK_WIFI){//无线网络
            mExoUserPlayer.startPlayer();
            toase("当前为wifi网络已帮您自动播放",1);
            return;
        }

    }




    //初始化数据方法
    private void initwidget() {
        //获取控件强转 提上去
        mVideoXRecyclerView = (XRecyclerView) getView(R.id.video_video_rv);
        mTwoVideoFreshLayout = (RefreshLayout) getView(R.id.two_video_refreshLayout);
        //设置线性管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        //设置方向
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //给recycler设置管理器
        mVideoXRecyclerView.setLayoutManager(linearLayoutManager);
        //view inf  传this 条目布局 和空
        View inflate = View.inflate(this, R.layout.video_head_view, null);
        //找控件 强转控件类型提上去
        mVideoHeadDesc = (TextView) inflate.findViewById(R.id.video_head_desc);
        mVideoPlayerView = (VideoPlayerView) inflate.findViewById(R.id.exo_play_context_id);
        exora = (RelativeLayout) inflate.findViewById(R.id.exo_ra_context_id);
//        int widthPixel = this.getResources().getDisplayMetrics().widthPixels;
//        Logger.i("屏幕的宽度",DimUtil.pxToDip(widthPixel)+"");
//        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) exora.getLayoutParams();
//        layoutParams.height = DimUtil.pxToDip(widthPixel);
//        exora.setLayoutParams(layoutParams);
        mDanmakuView = (DanmakuView) inflate.findViewById(R.id.sv_danmaku);
        mShowandhide = (ImageView) inflate.findViewById(R.id.video_showandhide);
        mSendText = (ImageView) inflate.findViewById(R.id.video_head_sendText);
        mVideoHeaded = (EditText) inflate.findViewById(R.id.video_head_ed);
        mVideoHeadte = (TextView) inflate.findViewById(R.id.video_head_te);
        //设置弹出
        mVideoHeadte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isBoFang){
                    if (mUserId == null && mUserName == null) {
                        toase("登陆后才可以发表评论哦~", 1);
                    } else {
                        mVideoHeadte.setVisibility(View.GONE);
                        mVideoHeaded.setVisibility(View.VISIBLE);
                    }
                }else {
                    toase("还没播放那~",1);
                }

            }
        });
        //弹幕线束隐藏
        mShowandhide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isHideDanmu) {
                    mShowandhide.setImageResource(R.drawable.show);
                    toase("已开启弹幕", 1);
                    isHideDanmu = false;
                    mDanmakuView.show();
                } else {
                    mShowandhide.setImageResource(R.drawable.hide);
                    toase("已关闭弹幕", 1);
                    isHideDanmu = true;
                    mDanmakuView.hide();
                }
            }
        });
        //发送
        mSendText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isBoFang){
                //获取发送弹幕的输入框内容
                String danmaKuText = mVideoHeaded.getText().toString();
                if (TextUtils.isEmpty(danmaKuText)) {
                    toase("内容不能为空~", 1);
                    return;
                } else {
                    //发送弹幕到服务器
                    mVideoActivityPersenter.getAddDanmaKu(danmaKuText, mUserId, mUserName);
                    //在本地添加弹幕
                    addDanmaku(true, danmaKuText);
                    //隐藏输入框
                    mVideoHeaded.setText("");
                    mVideoHeaded.setVisibility(View.GONE);
                    mVideoHeadte.setVisibility(View.VISIBLE);
                }
                }else {
                    toase("还没播放那~",1);
                }
            }
        });
        //给recycler社会自头部
        mVideoXRecyclerView.addHeaderView(inflate);
    }

    //这是view重写的方法
    @Override
    public void getItem(String data) {
        //穿的json 返回值
        VideoItemBean videoItemBean = new Gson().fromJson(data, VideoItemBean.class);
        //获取集合
        List<VideoItemBean.VideoListBean> video_list = videoItemBean.getVideo_list();
        //设置默认第一条变红
        video_list.get(0).setClick(true);
        //给头部简介赋值
        mVideoHeadDesc.setText(videoItemBean.getVideo_desc());
        //请求视频url
        mVideoActivityPersenter.getVideoUrl(videoItemBean.getVideo_url());
        //实例化适配器传this
        mVideoItemAdapter = new VideoItemAdapter(this);
        //设置适配器
        mVideoXRecyclerView.setAdapter(mVideoItemAdapter);
        //给适配器设置集合
        mVideoItemAdapter.setList(video_list);
        //调用适配器里的接口回掉改变视频地址
        mVideoItemAdapter.result(new VideoItemAdapter.SetOnitem() {
            @Override
            public void success(String stock) {
                //请求视频url
                mVideoActivityPersenter.getVideoUrl(stock);
            }
        });

    }

    //请求视频
    @Override
    public void getVideoUrl(String data) {
        //先对返回的json进行截取
        String substring = data.substring(13, data.length() - 1);
        Logger.i("返回的data", data);
        VideoUrlBean videoUrlBean = new Gson().fromJson(substring, VideoUrlBean.class);
        mTitle = videoUrlBean.getVl().getVi().get(0).getTi();
//        Logger.i("视频标题",ti);
        //在对饭后的json进行拼接
        VideoUrlBean.VlBean.ViBean viBean = videoUrlBean.getVl().getVi().get(0);
        mVideoUrl = viBean.getUl().getUi().get(2).getUrl() + viBean.getFn() + "?vkey=" + viBean.getFvkey();
        Logger.i("这是视频", mVideoUrl);
        Glide.with(this).load(mVideoImgae).into(mVideoPlayerView.getPreviewImage());
        mVideoPlayerView.setShowVideoSwitch(true);//设置开启线路切换按钮
        mVideoPlayerView.setShowBack(false);//设置返回返回按钮
        //初始化播放器
        initExoPlay();
        //设置弹幕
        init();
    }

    //初始化弹幕
    private void init() {
        mContext = DanmakuContext.create();
        // 设置最大显示行数
        HashMap<Integer, Integer> maxLinesPair = new HashMap<>();
        maxLinesPair.put(BaseDanmaku.TYPE_SCROLL_RL, 8); // 滚动弹幕最大显示5行
        // 设置是否禁止重叠
        HashMap<Integer, Boolean> overlappingEnablePair = new HashMap<>();
        overlappingEnablePair.put(BaseDanmaku.TYPE_SCROLL_RL, true);
        overlappingEnablePair.put(BaseDanmaku.TYPE_FIX_TOP, true);
        mContext.setDanmakuStyle(IDisplayer.DANMAKU_STYLE_STROKEN, 0) //描边的厚度
                .setDuplicateMergingEnabled(false)
                .setScrollSpeedFactor(1.3f) //弹幕的速度。注意！此值越小，速度越快！值越大，速度越慢。// by phil
                .setScaleTextSize(1.2f)  //缩放的值
                //.setCacheStuffer(new SpannedCacheStuffer(), mCacheStufferAdapter) // 图文混排使用SpannedCacheStuffer
//        .setCacheStuffer(new BackgroundCacheStuffer())  // 绘制背景使用BackgroundCacheStuffer
                .setMaximumLines(maxLinesPair)
                .preventOverlapping(overlappingEnablePair);
        mParser = new AcFunDanmakuParser();
//        mParser = createParser(this.getResources().openRawResource(R.raw.comments));
        mDanmakuView.prepare(mParser, mContext);
        //mDanmakuView.showFPS(true);
        mDanmakuView.enableDanmakuDrawingCache(true);
        if (mDanmakuView != null) {
            //播放监听
            exoPlayLinerter();
        }
    }

    //设置弹幕内容
    private void addDanmaku(boolean islive, String danmaKuText) {
//        Logger.i("发送弹幕","hhhhhh"+danmaKuText);
//        mContext = DanmakuContext.create();
        BaseDanmaku danmaku = mContext.mDanmakuFactory.createDanmaku(BaseDanmaku.TYPE_SCROLL_RL);
        if (danmaku == null || mDanmakuView == null) {
            return;
        }
        danmaku.text = danmaKuText;
        danmaku.padding = 5;
        danmaku.priority = 0;  // 可能会被各种过滤器过滤并隐藏显示
        danmaku.isLive = islive;
        danmaku.setTime(mDanmakuView.getCurrentTime() + 1200);
        danmaku.textSize = 20f * (mParser.getDisplayer().getDensity() - 0.6f); //文本弹幕字体大小
        danmaku.textColor = getRandomColor(); //文本的颜色
        danmaku.textShadowColor = getRandomColor(); //文本弹幕描边的颜色
        //danmaku.underlineColor = Color.DKGRAY; //文本弹幕下划线的颜色
//        danmaku.borderColor = getRandomColor(); //边框的颜色
        mDanmakuView.addDanmaku(danmaku);
    }

    // 从一系列颜色中随机选择一种颜色
    private int getRandomColor() {
        int[] colors = {Color.RED, Color.YELLOW, Color.BLUE, Color.GREEN, Color.CYAN, Color.BLACK, Color.DKGRAY};
        int i = ((int) (Math.random() * 10)) % colors.length;
        return colors[i];
    }

    //聚焦的方法
    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if(mExoUserPlayer!=null){
            mExoUserPlayer.onResume();
        }
    }

    //暂停的方法
    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        mDanmakuView.pause();
        mExoUserPlayer.onPause();
    }

    //销毁的反方
    @Override
    protected void onDestroy() {
        mExoUserPlayer.onDestroy();
        super.onDestroy();
    }

    //横竖屏切换
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        mExoUserPlayer.onConfigurationChanged(newConfig);//横竖屏切换
        super.onConfigurationChanged(newConfig);
    }

    //视频返回键
    @Override
    public void onBackPressed() {
        if (mExoUserPlayer.onBackPressed()) {
            finish();
        }
    }

    //弹幕解析
    private BaseDanmakuParser createParser(InputStream stream) {
        if (stream == null) {
            return new BaseDanmakuParser() {
                @Override
                protected Danmakus parse() {
                    return new Danmakus();
                }
            };
        }
        ILoader loader = DanmakuLoaderFactory.create(DanmakuLoaderFactory.TAG_BILI);
        try {
            loader.load(stream);
        } catch (IllegalDataException e) {
            e.printStackTrace();
        }
        BaseDanmakuParser parser = new BiliDanmukuParser();
        IDataSource<?> dataSource = loader.getDataSource();
        parser.load(dataSource);
        return parser;
    }

    //获取弹幕
    @Override
    public void getDanmakuData(String data) {
        DanmakuBean danmakuBean = new Gson().fromJson(data, DanmakuBean.class);
        List<DanmakuBean.ItemsBean> items = danmakuBean.getItems();
        Logger.i("弹幕集合长度", items.size() + "哈哈哈");
        for (int a = 0; a < items.size(); a++) {
            String open_content = items.get(a).getOpen_content();
            Logger.i("弹幕集合内容", open_content);
//            danlists.add(open_content);
            //判断是否在播放 再继续加载弹幕
                addDanmaku(true, open_content);
        }
        //判断是否在播放 再继续加载弹幕
        if (mExoUserPlayer.isPlaying()) {
            page++;
            //请求弹幕的数据
            mVideoActivityPersenter.getDanmakuData(page);
        }
    }

    //添加弹幕的接口返回
    @Override
    public void getAddDanmaKu(String data) {
        AddDanmaKuBean addDanmaKuBean = new Gson().fromJson(data, AddDanmaKuBean.class);
        if (addDanmaKuBean.getStatus() == 0) {
            toase("发表成功 喵~", 1);
        }
    }

    //播放监听
    private void exoPlayLinerter() {
        mExoUserPlayer.addVideoInfoListener(new VideoInfoListener() {
            @Override
            public void onPlayStart(long currPosition) {
                isBoFang = true;
//                Logger.i("开始播放", currPosition + "");
////                if (currPosition == 0) {
////                    mDanmakuView.start();
////                } else {
//                    mDanmakuView.seekTo(currPosition);
//                }
                //请求弹幕的数据
                mVideoActivityPersenter.getDanmakuData(page);
            }

            @Override
            public void onLoadingChanged() {

            }

            @Override
            public void onPlayerError(@Nullable ExoPlaybackException e) {
                mDanmakuView.stop();
            }

            @Override
            public void onPlayEnd() {
                mDanmakuView.stop();
            }

            @Override
            public void isPlaying(boolean playWhenReady) {
                if (playWhenReady) {
                    mDanmakuView.start();
                } else {
                    mDanmakuView.pause();
                }
            }
        });

    }

    //实时监听网络变化之后的类型
    @Override
    public void onNetChange(int netMobile) {
        // TODO Auto-generated method stub
           if(netMobile==NETWORK_NONE){//没网状态
//               mExoUserPlayer.onPause();
              toase("网络好像断开了~",1);
              return;
           }else if(netMobile==NETWORK_MOBILE){//移动网络状态
               mExoUserPlayer.onPause();
               toase("当前为移动网络，注意流量资费~",1);
               return;
        }else if(netMobile==NETWORK_WIFI){//无线网络
               mExoUserPlayer.startPlayer();
               toase("当前为wifi网络已帮您自动播放",1);
               return;
           }
    }


}


