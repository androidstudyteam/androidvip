package com.firsttream.video.activitys;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.baselibrary.util.SpUtil;
import com.firsttream.video.R;
import com.firsttream.video.adapter.VideoMoreJumpAdapter;
import com.firsttream.video.entity.AddDanmaKuBean;
import com.firsttream.video.entity.DanmakuBean;
import com.firsttream.video.entity.MoreJumpBean;
import com.firsttream.video.entity.VideoUrlBean;
import com.firsttream.video.mvp.modle.MoreDescActivityModle;
import com.firsttream.video.mvp.persenter.MoreDescActivityPersenter;
import com.firsttream.video.mvp.view.MoreDescActivityView;
import com.firsttream.video.utiles.AcFunDanmakuParser;
import com.firsttream.video.utiles.BiliDanmukuParser;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.header.WaterDropHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import chuangyuan.ycj.videolibrary.listener.VideoInfoListener;
import chuangyuan.ycj.videolibrary.video.ExoUserPlayer;
import chuangyuan.ycj.videolibrary.video.VideoPlayerManager;
import chuangyuan.ycj.videolibrary.widget.VideoPlayerView;
import master.flame.danmaku.danmaku.loader.ILoader;
import master.flame.danmaku.danmaku.loader.IllegalDataException;
import master.flame.danmaku.danmaku.loader.android.DanmakuLoaderFactory;
import master.flame.danmaku.danmaku.model.BaseDanmaku;
import master.flame.danmaku.danmaku.model.IDisplayer;
import master.flame.danmaku.danmaku.model.android.DanmakuContext;
import master.flame.danmaku.danmaku.model.android.Danmakus;
import master.flame.danmaku.danmaku.parser.BaseDanmakuParser;
import master.flame.danmaku.danmaku.parser.IDataSource;
import master.flame.danmaku.ui.widget.DanmakuView;

/*
 * 作者:马富燕&高佳宝
 * 日期:2018/12/19
 * 作用:更多视频点击事件的Activity MoreDescActivity
 * */
//先是泛型分别继承这个类的m  v  p 在继承baseActivity 在实现自己的view接口
public class MoreDescActivity<M extends MoreDescActivityModle, V extends MoreDescActivityView, P extends MoreDescActivityPersenter> extends BaseActivity implements MoreDescActivityView {

    private MoreDescActivityPersenter mMoreDescActivityPersenter;
    private XRecyclerView mJumpRecyView;
    private TextView mVideoheaddesc;
    private VideoPlayerView mExopaly;
    private DanmakuView mDanmakuView;
    private ImageView mShowandhide;
    private ImageView mSendText;
    private EditText mVideoHeaded;
    private VideoMoreJumpAdapter mVideoItemAdapter;
    private ExoUserPlayer mExoUserPlayer;
    private String mVideoimgae;
    private boolean isHideDanmu = false;
    private DanmakuContext mContext;
    private AcFunDanmakuParser mParser;
    private boolean isBoFang = false;
    private String mVideoTitle;
    private String mVideoUrl;
    private RefreshLayout mThreerefreshLayout;
    private int page = 1;
    private TextView mVideoHeadte;
    private String mUserId;
    private String mUserName;

    @Override
    protected int getLayoutId() {
        //返回本页面
        return R.layout.activity_more_desc;
    }

    @Override
    public IContract.Model createModel() {
        //new 本页面modle 不报错就不用强转
        return new MoreDescActivityModle();
    }

    @Override
    public IContract.View createView() {
        //返回this
        return this;
    }

    @Override
    public BasePersenter createPersenter() {
        //先new persenter 提上去
        mMoreDescActivityPersenter = new MoreDescActivityPersenter();
        //初始化上下文传this
        mMoreDescActivityPersenter.initContext(this);
        //返回persenter
        return mMoreDescActivityPersenter;
    }

    //重写初始化数据方法
    @Override
    public void initData() {
        super.initData();
        //获取用户id 和用户昵称
        mUserId = (String) SpUtil.getSpData(this, "userId", "");
        mUserName = (String) SpUtil.getSpData(this, "userName", "");
        Logger.i("我的用户名和密码", "" + mUserId + "好不会吧" + mUserName);
        //获取标题和图片和位置
        Intent intent = getIntent();
        String titles = intent.getStringExtra("titles");
        String positionid = intent.getStringExtra("positionid");
        mVideoimgae = intent.getStringExtra("videoimgae");
        //设置标题
        setTitle(titles);
        //初始化数据方法
        initwidget();
        //new 线性管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        //设置方向
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //给recycler设置线性管理器
        mJumpRecyView.setLayoutManager(linearLayoutManager);
//        //请求网络的方法 调用videopersenter本类的
        mMoreDescActivityPersenter.getJumpItem(positionid);
        //停止上拉和下拉刷新
        mJumpRecyView.setPullRefreshEnabled(false);
        //设置 Header 为 贝塞尔雷达 样式
        mThreerefreshLayout.setRefreshHeader(new WaterDropHeader(this));
        //设置 Footer 为 球脉冲 样式
        mThreerefreshLayout.setRefreshFooter(new BallPulseFooter(this).setSpinnerStyle(SpinnerStyle.Scale));
        mThreerefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                //        //请求网络的方法 调用videopersenter本类的
                mMoreDescActivityPersenter.getJumpItem(positionid);
                refreshlayout.finishRefresh(1000/*,false*/);//传入false表示刷新失败
            }
        });
        mThreerefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                //        //请求网络的方法 调用videopersenter本类的
                mMoreDescActivityPersenter.getJumpItem(positionid);
                refreshlayout.finishLoadMore(1000/*,false*/);//传入false表示加载失败
            }
        });

    }

    //初始化数据方法
    private void initwidget() {
        //获取控件强转提上
        mJumpRecyView = (XRecyclerView) getView(R.id.video_jump_rv);
        mThreerefreshLayout = (RefreshLayout) getView(R.id.three_refreshLayout);
        //设置线性管理器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        //设置方向
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //给recycler设置管理器
        mJumpRecyView.setLayoutManager(linearLayoutManager);
        //view inf  传this 条目布局 和空
        View inflate = View.inflate(this, R.layout.video_head_view, null);
        //找控件 强转控件类型提上去
        mVideoheaddesc = (TextView) inflate.findViewById(R.id.video_head_desc);
        mExopaly = (VideoPlayerView) inflate.findViewById(R.id.exo_play_context_id);
        mDanmakuView = (DanmakuView) inflate.findViewById(R.id.sv_danmaku);
        mShowandhide = (ImageView) inflate.findViewById(R.id.video_showandhide);
        mSendText = (ImageView) inflate.findViewById(R.id.video_head_sendText);
        mVideoHeaded = (EditText) inflate.findViewById(R.id.video_head_ed);
        mVideoHeadte = (TextView) inflate.findViewById(R.id.video_head_te);
        //设置弹出
        mVideoHeadte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isBoFang){
                    if (mUserId == null && mUserName == null) {
                        toase("登陆后才可以发表评论哦~", 1);
                    } else {
                        mVideoHeadte.setVisibility(View.GONE);
                        mVideoHeaded.setVisibility(View.VISIBLE);
                    }
                }else {
                    toase("还没播放那~",1);
                }

            }
        });
        //弹幕线束隐藏
        mShowandhide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isHideDanmu) {
                    mShowandhide.setImageResource(R.drawable.show);
                    toase("已开启弹幕", 1);
                    isHideDanmu = false;
                    mDanmakuView.show();
                } else {
                    mShowandhide.setImageResource(R.drawable.hide);
                    toase("已关闭弹幕", 1);
                    isHideDanmu = true;
                    mDanmakuView.hide();
                }
            }
        });
        //发送
        mSendText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBoFang) {
                    //获取发送弹幕的输入框内容
                    String danmaKuText = mVideoHeaded.getText().toString();
                    if (TextUtils.isEmpty(danmaKuText)) {
                        toase("内容不能为空~", 1);
                        return;
                    } else {
                        //发送弹幕到服务器
                        mMoreDescActivityPersenter.getAddDanmaKu(danmaKuText, mUserId, mUserName);
                        //在本地添加弹幕
                        addDanmaku(true, danmaKuText);
                        //隐藏输入框
                        mVideoHeaded.setText("");
                        mVideoHeaded.setVisibility(View.GONE);
                        mVideoHeadte.setVisibility(View.VISIBLE);
                    }
                } else {
                    toase("还没播放那~", 1);
                }
            }
        });
        //给recycler社会自头部
        mJumpRecyView.addHeaderView(inflate);
    }

    //请求跳过来的条目
    @Override
    public void getJumpItem(MoreJumpBean moreJumpBean) {
        //给头部简介赋值
        mVideoheaddesc.setText(moreJumpBean.getVideo_desc());
        //请求视频url
        mMoreDescActivityPersenter.getVideoUrl(moreJumpBean.getVideo_url());
        //获取集合
        List<MoreJumpBean.VideoListBean> video_list = moreJumpBean.getVideo_list();
        //设置第一条变色
        video_list.get(0).setClick(true);
        //请求视频url
//        videoActivityPersenter.getVideoUrl(videoItemBean.getVideo_url());
        //实例化适配器传this
        mVideoItemAdapter = new VideoMoreJumpAdapter(this);
        //设置适配器
        mJumpRecyView.setAdapter(mVideoItemAdapter);
        //给适配器设置集合
        mVideoItemAdapter.setList(video_list);
        //调用适配器里的接口回掉改变视频地址
        mVideoItemAdapter.result(new VideoMoreJumpAdapter.SetOnitem() {
            @Override
            public void success(String stock) {
                mMoreDescActivityPersenter.getVideoUrl(stock);
            }
        });

    }

    //请求跳过来的视频地址
    @Override
    public void getVideoUrl(String data) {
//先对返回的json进行截取
        String substring = data.substring(13, data.length() - 1);
        Logger.i("返回的data", data);
        VideoUrlBean videoUrlBean = new Gson().fromJson(substring, VideoUrlBean.class);
        mVideoTitle = videoUrlBean.getVl().getVi().get(0).getTi();
//        Logger.i("视频标题",ti);
        //在对饭后的json进行拼接
        VideoUrlBean.VlBean.ViBean viBean = videoUrlBean.getVl().getVi().get(0);
        mVideoUrl = viBean.getUl().getUi().get(2).getUrl() + viBean.getFn() + "?vkey=" + viBean.getFvkey();
        Logger.i("这是视频", mVideoUrl);
        Glide.with(this).load(mVideoimgae).into(mExopaly.getPreviewImage());
        mExopaly.setShowVideoSwitch(true);//设置开启线路切换按钮
        mExopaly.setShowBack(false);//设置返回返回按钮
        initExoPlay();
        //设置弹幕
        init();
    }

    //初始化播放器
    private void initExoPlay() {
        mExoUserPlayer = new VideoPlayerManager.Builder(VideoPlayerManager.TYPE_PLAY_GESTURE, mExopaly)
                .setPlayUri(mVideoUrl)//设置地址
                .setTitle(mVideoTitle)//标题
                .setPlayerGestureOnTouch(true)
                .setShowVideoSwitch(true)
                .create();//创建
//                        .setVerticalFullScreen(true)//开启竖屏全屏

    }

    //初始化弹幕
    private void init() {
        mContext = DanmakuContext.create();
        // 设置最大显示行数
        HashMap<Integer, Integer> maxLinesPair = new HashMap<>();
        maxLinesPair.put(BaseDanmaku.TYPE_SCROLL_RL, 8); // 滚动弹幕最大显示5行
        // 设置是否禁止重叠
        HashMap<Integer, Boolean> overlappingEnablePair = new HashMap<>();
        overlappingEnablePair.put(BaseDanmaku.TYPE_SCROLL_RL, true);
        overlappingEnablePair.put(BaseDanmaku.TYPE_FIX_TOP, true);

        mContext.setDanmakuStyle(IDisplayer.DANMAKU_STYLE_STROKEN, 0) //描边的厚度
                .setDuplicateMergingEnabled(false)
                .setScrollSpeedFactor(1.2f) //弹幕的速度。注意！此值越小，速度越快！值越大，速度越慢。// by phil
                .setScaleTextSize(1.2f)  //缩放的值
                //.setCacheStuffer(new SpannedCacheStuffer(), mCacheStufferAdapter) // 图文混排使用SpannedCacheStuffer
//        .setCacheStuffer(new BackgroundCacheStuffer())  // 绘制背景使用BackgroundCacheStuffer
                .setMaximumLines(maxLinesPair)
                .preventOverlapping(overlappingEnablePair);
        mParser = new AcFunDanmakuParser();
//        mParser = createParser(this.getResources().openRawResource(R.raw.comments));
        mDanmakuView.prepare(mParser, mContext);
//        mDanmakuView.showFPS(true);
        mDanmakuView.enableDanmakuDrawingCache(true);
        if (mDanmakuView != null) {
            exoPlayLinerter();
        }
    }

    //设置弹幕内容
    private void addDanmaku(boolean islive, String danmaKuText) {
        Logger.i("发送弹幕","hhhhhh"+danmaKuText);
//        mContext = DanmakuContext.create();
        BaseDanmaku danmaku = mContext.mDanmakuFactory.createDanmaku(BaseDanmaku.TYPE_SCROLL_RL);
        if (danmaku == null || mDanmakuView == null) {
            return;
        }
        danmaku.text = danmaKuText;
        danmaku.padding = 5;
        danmaku.priority = 0;  // 可能会被各种过滤器过滤并隐藏显示
        danmaku.isLive = islive;
        danmaku.setTime(mDanmakuView.getCurrentTime() + 1200);
        danmaku.textSize = 20f * (mParser.getDisplayer().getDensity() - 0.6f); //文本弹幕字体大小
        danmaku.textColor = getRandomColor(); //文本的颜色
        danmaku.textShadowColor = getRandomColor(); //文本弹幕描边的颜色
        //danmaku.underlineColor = Color.DKGRAY; //文本弹幕下划线的颜色
//        danmaku.borderColor = getRandomColor(); //边框的颜色
        mDanmakuView.addDanmaku(danmaku);
    }

    // 从一系列颜色中随机选择一种颜色
    private int getRandomColor() {
        int[] colors = {Color.RED, Color.YELLOW, Color.BLUE, Color.GREEN, Color.CYAN, Color.BLACK, Color.DKGRAY};
        int i = ((int) (Math.random() * 10)) % colors.length;
        return colors[i];
    }

    //聚焦的方法
    @Override
    public void onResume() {
        super.onResume();
        if (mExoUserPlayer != null) {
            mExoUserPlayer.onResume();
        }

    }

    //暂停的方法
    @Override
    public void onPause() {
        super.onPause();
        mExoUserPlayer.onPause();
    }

    //销毁的反方
    @Override
    protected void onDestroy() {
        mExoUserPlayer.onDestroy();
        super.onDestroy();
    }

    //横竖屏切换
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mExoUserPlayer.onConfigurationChanged(newConfig);//横竖屏切换
    }

    //视频返回键
    @Override
    public void onBackPressed() {
        if (mExoUserPlayer.onBackPressed()) {
            finish();
        }
    }

    //弹幕解析
    private BaseDanmakuParser createParser(InputStream stream) {
        if (stream == null) {
            return new BaseDanmakuParser() {
                @Override
                protected Danmakus parse() {
                    return new Danmakus();
                }
            };
        }
        ILoader loader = DanmakuLoaderFactory.create(DanmakuLoaderFactory.TAG_BILI);
        try {
            loader.load(stream);
        } catch (IllegalDataException e) {
            e.printStackTrace();
        }
        BaseDanmakuParser parser = new BiliDanmukuParser();
        IDataSource<?> dataSource = loader.getDataSource();
        parser.load(dataSource);
        return parser;
    }

    //播放监听
    private void exoPlayLinerter() {
        mExoUserPlayer.addVideoInfoListener(new VideoInfoListener() {
            @Override
            public void onPlayStart(long currPosition) {
                isBoFang = true;
//                Logger.i("开始播放", currPosition + "");
//                if (currPosition == 0) {
//                    mDanmakuView.start();
//                } else {
//                    mDanmakuView.seekTo(currPosition);
//                }
                mMoreDescActivityPersenter.getDanmakuData(page);
            }

            @Override
            public void onLoadingChanged() {

            }

            @Override
            public void onPlayerError(@Nullable ExoPlaybackException e) {
                mDanmakuView.stop();
            }

            @Override
            public void onPlayEnd() {
                mDanmakuView.stop();
            }

            @Override
            public void isPlaying(boolean playWhenReady) {
                if (playWhenReady) {
                    mDanmakuView.start();
                } else {
                    mDanmakuView.pause();
                }
            }
        });
    }

    //获取弹幕
    @Override
    public void getDanmakuData(String data) {
        DanmakuBean danmakuBean = new Gson().fromJson(data, DanmakuBean.class);
        List<DanmakuBean.ItemsBean> items = danmakuBean.getItems();
        Logger.i("弹幕集合长度", items.size() + "哈哈哈");
        for (int a = 0; a < items.size(); a++) {
            String open_content = items.get(a).getOpen_content();
            Logger.i("弹幕集合内容", open_content);
//            danlists.add(open_content);
            //判断是否在播放 再继续加载弹幕
            addDanmaku(true, open_content);
        }
        //判断是否在播放 再继续加载弹幕
        if (mExoUserPlayer.isPlaying()) {
            page++;
            //请求弹幕的数据
            mMoreDescActivityPersenter.getDanmakuData(page);
        }
    }

    //添加弹幕的接口返回
    @Override
    public void getAddDanmaKu(String data) {
        AddDanmaKuBean addDanmaKuBean = new Gson().fromJson(data, AddDanmaKuBean.class);
        if (addDanmaKuBean.getStatus() == 0) {
            toase("发表成功 喵~", 1);
        }
    }
}
