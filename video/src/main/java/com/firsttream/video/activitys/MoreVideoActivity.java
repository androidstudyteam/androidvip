package com.firsttream.video.activitys;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.video.R;
import com.firsttream.video.entity.MoreVideoBean;
import com.firsttream.video.fragment.FragmentMoreVideoDesc;
import com.firsttream.video.mvp.modle.MoreVideoModle;
import com.firsttream.video.mvp.persenter.MoreVideoPersenter;
import com.firsttream.video.mvp.view.MoreVideoView;

import java.util.ArrayList;
import java.util.List;

/*
 * 作者:马富燕
 * 日期:2018/12/18
 * 作用:更多视频的Activity MoreVideoActivity
 * */

//先是泛型分别继承这个类的m  v  p 在继承baseActivity 在实现自己的view接口
public class MoreVideoActivity<M extends MoreVideoModle, V extends MoreVideoView, P extends MoreVideoPersenter> extends BaseActivity implements MoreVideoView {

    private MoreVideoPersenter mMoreVideoPersenter;
    private List<MoreVideoBean.ItemsBean> mItems = new ArrayList<>();
    private TabLayout mMorevideotab;
    private ViewPager mMorevideovp;
    private List<Fragment> mFragments = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        //返回本页面布局
        return R.layout.activity_more_video;
    }

    @Override
    public IContract.Model createModel() {
        //返回new  本类modle 要是报错就强转 不报错 就不用管
        return new MoreVideoModle();
    }

    @Override
    public IContract.View createView() {
        //直接返回this
        return this;
    }

    @Override
    public BasePersenter createPersenter() {
        //先new persenter 返回值 提上去
        mMoreVideoPersenter = new MoreVideoPersenter();
        //记得初始化persenter 初始化上下文 传上下文
        mMoreVideoPersenter.initContext(this);
        //直接返回persenter
        return mMoreVideoPersenter;
    }

    //重写初始化数据方法
    @Override
    public void initData() {
        super.initData();
        //设置标题 更多视频
        setTitle("更多视频");
        //初始化数据方法
        initwidget();
        //先给这个类的persenter写一个请求 网络数据的方法  生成到自己的persenter
        mMoreVideoPersenter.getMoreVideoTitles();
    }

    //初始化数据方法
    private void initwidget() {
        //获取控件强转 提上去
        mMorevideotab = (TabLayout) getView(R.id.morevideo_tab);
        mMorevideovp = (ViewPager) getView(R.id.morevideo_vp);
    }

    //请求更多视频标题
    @Override
    public void getMoreVideoTitles(MoreVideoBean moreVideoBean) {
        //获取bean类的集合 提上去记得实例化
        mItems = moreVideoBean.getItems();
        //因为这是从modle view传过来的集合 所有 关于这个集合使用的情况下 把循环 和设置适配器设置tab 一类都放到这个方法里面
        //循环标题的数据长度 我那个fragment里new frgament
        for (int i = 0; i < mItems.size(); i++) {
            //往集合里new 添加frgament
            mFragments.add(new FragmentMoreVideoDesc<>());
        }
        //设置适配器 继承frgament pager
        //实例化适配器 传获取activity.getSupportFragmentManager
        MyAdapter myAdapter = new MyAdapter(this.getSupportFragmentManager());
        //再给vp设置适配器
        mMorevideovp.setAdapter(myAdapter);
        //设置ViewPage缓存界面数 有几页就缓存几页
        mMorevideovp.setOffscreenPageLimit(3);
        //给tab设置模型 括号里传大写tab mode scrollable
//        ctab.setTabMode(TabLayout.MODE_SCROLLABLE);
        //给tab设置upwithviewpager 传cvp
        mMorevideotab.setupWithViewPager(mMorevideovp);
    }

    //设置适配器继承frgamentpager适配器
    public class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            //返回fragmments集合获取位置
            Fragment fragment = mFragments.get(i);
            //new bundle
            Bundle bundle = new Bundle();
            //往bundle里put存入值
            bundle.putString("type", mItems.get(i).getType());
            //往frgament里面设置arguments 传bundle
            fragment.setArguments(bundle);
            //返回frgament
            return fragment;
        }

        @Override
        public int getCount() {
            //返回fragments集合大小
            return mFragments.size();
        }

        //重写获取标题的方法
        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            //返回标题数组的位置在获取名称
            return mItems.get(position).getName();
        }
    }
}
