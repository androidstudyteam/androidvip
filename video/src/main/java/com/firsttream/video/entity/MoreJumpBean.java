package com.firsttream.video.entity;

import java.util.List;
/**
 * 作者:马富燕
 * 日期:2018/12/19
 * 作用:更多视频跳过来的bean类
 */
public class MoreJumpBean {


    /**
     * title : Android Studio工具讲解
     * video_url : b0318cpzggn
     * video_desc : 在Android Studio发布之后，无论国内外，都掀起了一股Android Studio PK Eclipse的热潮，人们争相热议如何看待Google I/O上推出的Android Studio？能完全取代Eclipse吗？Android Studio是一项全新的基于IntelliJ IDEA的Android开发环境。类似于Eclipse ADT插件，Android Studio提供了集成的Android开发工具用于开发和调试。
     * isList : false
     * video_list : [{"video_title":"1.Android Studio工具讲解（第一集）","video_url":"b0318cpzggn","video_time":"","video_id":0},{"video_title":"2.Android Studio工具讲解（第二集）","video_url":"g0318f44uqy","video_time":"","video_id":0},{"video_title":"3.Android Studio工具讲解（第三集）","video_url":"t0318dk4dqw","video_time":"","video_id":0},{"video_title":"4.Android Studio工具讲解（第四集）","video_url":"y0318przo4v","video_time":"","video_id":0}]
     */

    private String title;
    private String video_url;
    private String video_desc;
    private boolean isList;


    private List<VideoListBean> video_list;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_desc() {
        return video_desc;
    }

    public void setVideo_desc(String video_desc) {
        this.video_desc = video_desc;
    }

    public boolean isIsList() {
        return isList;
    }

    public void setIsList(boolean isList) {
        this.isList = isList;
    }

    public List<VideoListBean> getVideo_list() {
        return video_list;
    }

    public void setVideo_list(List<VideoListBean> video_list) {
        this.video_list = video_list;
    }

    public static class VideoListBean {
        /**
         * video_title : 1.Android Studio工具讲解（第一集）
         * video_url : b0318cpzggn
         * video_time :
         * video_id : 0
         */

        private String video_title;
        private String video_url;
        private String video_time;
        private int video_id;
        private boolean isClick;

        public boolean isClick() {
            return isClick;
        }

        public void setClick(boolean click) {
            isClick = click;
        }
        public String getVideo_title() {
            return video_title;
        }

        public void setVideo_title(String video_title) {
            this.video_title = video_title;
        }

        public String getVideo_url() {
            return video_url;
        }

        public void setVideo_url(String video_url) {
            this.video_url = video_url;
        }

        public String getVideo_time() {
            return video_time;
        }

        public void setVideo_time(String video_time) {
            this.video_time = video_time;
        }

        public int getVideo_id() {
            return video_id;
        }

        public void setVideo_id(int video_id) {
            this.video_id = video_id;
        }
    }
}
