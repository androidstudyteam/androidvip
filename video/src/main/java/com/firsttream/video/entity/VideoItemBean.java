package com.firsttream.video.entity;

import java.util.List;
/**
 * 作者:马富燕
 * 日期:2018/12/14
 * 作用:视频列表的bean类
 */
public class VideoItemBean {

    /**
     * title : Android视频集锦
     * video_url : j0318bl3oa2
     * video_desc : 这个是Android系列的视频集锦，不区分章节，每个章节都是一个独立的视频，请大家知晓
     * isList : false
     * video_list : [{"video_title":"Android MVP架构设计","video_url":"j0318bl3oa2","video_time":"","video_id":0},{"video_title":"Android开发实战进阶\u2018插件化设计\u201c精讲","video_url":"e0515x453v8","video_time":"","video_id":0},{"video_title":"Android插件化架构设计","video_url":"g03187gj9vx","video_time":"","video_id":0},{"video_title":"学习爆炸特效实战精讲","video_url":"y0515c7o6ne","video_time":"","video_id":0},{"video_title":"自定义动画框架","video_url":"j0302qdjzog","video_time":"","video_id":0},{"video_title":"AsyncTask的使用精讲","video_url":"a0177w6gj7q","video_time":"","video_id":0},{"video_title":"插件式开发","video_url":"b031847eqie","video_time":"","video_id":0},{"video_title":"Tween补间动画","video_url":"v018524pgtu","video_time":"","video_id":0},{"video_title":"Android之贝塞尔曲线","video_url":"l0318hojj24","video_time":"","video_id":0},{"video_title":"android开发教程：百度地图","video_url":"r0185da2ojt","video_time":"","video_id":0},{"video_title":"Android开发教程：适配器框架解决","video_url":"b01862sts7k","video_time":"","video_id":0},{"video_title":"NDK高效率图片处理","video_url":"s0318e54awu","video_time":"","video_id":0},{"video_title":"SurfaceView绘图","video_url":"f0186b2kaim","video_time":"","video_id":0},{"video_title":"Android开发fragment通信框架","video_url":"x0512hbp0u8","video_time":"","video_id":0},{"video_title":"VR百度地图全景公开课精讲视频","video_url":"b14224y8o2e","video_time":"","video_id":0},{"video_title":"Git的使用","video_url":"v1421in35aw","video_time":"","video_id":0},{"video_title":"静默安装","video_url":"s0186b4zxp1","video_time":"","video_id":0},{"video_title":"增量更新（上）","video_url":"n0313x7isnu","video_time":"","video_id":0},{"video_title":"增量更新（下）","video_url":"w0313vx2u9b","video_time":"","video_id":0}]
     */

    private String title;
    private String video_url;
    private String video_desc;
    private boolean isList;


    private List<VideoListBean> video_list;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_desc() {
        return video_desc;
    }

    public void setVideo_desc(String video_desc) {
        this.video_desc = video_desc;
    }

    public boolean isIsList() {
        return isList;
    }

    public void setIsList(boolean isList) {
        this.isList = isList;
    }

    public List<VideoListBean> getVideo_list() {
        return video_list;
    }

    public void setVideo_list(List<VideoListBean> video_list) {
        this.video_list = video_list;
    }

    public static class VideoListBean {
        /**
         * video_title : Android MVP架构设计
         * video_url : j0318bl3oa2
         * video_time :
         * video_id : 0
         */

        private String video_title;
        private String video_url;
        private String video_time;
        private int video_id;
        private boolean isClick;

        public boolean isClick() {
            return isClick;
        }

        public void setClick(boolean click) {
            isClick = click;
        }
        public String getVideo_title() {
            return video_title;
        }

        public void setVideo_title(String video_title) {
            this.video_title = video_title;
        }

        public String getVideo_url() {
            return video_url;
        }

        public void setVideo_url(String video_url) {
            this.video_url = video_url;
        }

        public String getVideo_time() {
            return video_time;
        }

        public void setVideo_time(String video_time) {
            this.video_time = video_time;
        }

        public int getVideo_id() {
            return video_id;
        }

        public void setVideo_id(int video_id) {
            this.video_id = video_id;
        }
    }
}
