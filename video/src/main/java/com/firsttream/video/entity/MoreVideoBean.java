package com.firsttream.video.entity;

import java.util.List;

/**
 * 作者:马富燕
 * 日期:2018/12/18
 * 作用:更多视频的bean类
 */

public class MoreVideoBean {

    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * name : Android
         * type : 0
         */

        private String name;
        private String type;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
