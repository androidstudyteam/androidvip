package com.firsttream.video.mvp.persenter;


import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.video.mvp.modle.FragmentMoreDescModle;
import com.firsttream.video.mvp.view.FragmentMoreDescView;

/**
 *作者:马富燕
 * 日期:2018/12/18
 * 作用:tablayout内容的fragmentpersenter FragmentMoreDeacPersenter
 */

//继承父类persenter泛型 m  v 自己定义的关于这个类的
public class FragmentMoreDescPersenter extends BasePersenter<FragmentMoreDescModle,FragmentMoreDescView> {

    @Override
    protected void onViewDestory() {
        //先判断modle非空
        if(model!=null){
            //非空的话 modle就调用删除方法
            model.stopRequest();
        }
    }

    public void getRvDesc(String mType) {
        //model 调用方法传上下文和type
        model.getRvDesc(context,mType,getView());
    }
}
