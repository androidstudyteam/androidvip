package com.firsttream.video.mvp.persenter;


/*
 * 作者:马富燕
 * 日期:2018/12/18
 * 作用:更多视频的  persenter  MoreVideoPersenter
 * */

import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.video.mvp.modle.MoreVideoModle;
import com.firsttream.video.mvp.view.MoreVideoView;

//继承父类persenter 泛型传这个类关于的modle和view
public class MoreVideoPersenter extends BasePersenter<MoreVideoModle,MoreVideoView>{

    @Override
    protected void onViewDestory() {
        //判断modle非空
        if(model!=null){
            //非空就用modle调用方法
            model.stopRequest();
        }
    }
    //把生成的第二个方法删了

    public void getMoreVideoTitles() {
        //用modle调用写一个方法生成到 这个类相关的modle里 括号里传获取view的方法
        model.getMoreVideoTitiles(context,getView());
    }


}
