package com.firsttream.video.mvp.view;

import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.video.entity.MoreVideoDescBean;

/**
 *作者:马富燕
 * 日期:2018/12/18
 * 作用:tablayout内容的fragment view 一个接口  FragmentMoreDeacView
 */

//继承契约类view 接口
public interface FragmentMoreDescView extends IContract.View {

    void getRvDesc(MoreVideoDescBean moreVideoDescBean);
}
