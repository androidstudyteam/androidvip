package com.firsttream.video.mvp.view;

import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.video.entity.MoreVideoBean;

import java.util.List;

/*
 * 作者:马富燕
 * 日期:2018/12/18
 * 作用:更多视频的  view   接口 MoreVideoView
 * */
//继承契约类的view  接口
public interface MoreVideoView extends IContract.View {

    void getMoreVideoTitles(MoreVideoBean moreVideoBean);
    
}
