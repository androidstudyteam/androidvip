package com.firsttream.video.mvp.view;

/*
 * 作者:马富燕
 * 日期:2018/12/13
 * 作用:视频的  view   接口 VideoActivityView
 * */

import com.firsttream.baselibrary.contract.IContract;

//接口view 继承契约类的view
public interface VideoActivityView extends IContract.View {
    void getItem(String data);

    void getVideoUrl(String data);

    void getDanmakuData(String data);

    void getAddDanmaKu(String data);
}
