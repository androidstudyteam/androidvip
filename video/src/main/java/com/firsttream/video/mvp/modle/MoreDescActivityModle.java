package com.firsttream.video.mvp.modle;

/*
 * 作者:马富燕
 * 日期:2018/12/19
 * 作用:更多视频内容的  molde MoreDescActivityModle
 * */

import android.content.Context;

import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.video.entity.MoreJumpBean;
import com.firsttream.video.entity.MoreVideoDescBean;
import com.firsttream.video.mvp.view.MoreDescActivityView;
import com.firsttream.video.mvp.view.VideoActivityView;
import com.google.gson.Gson;

import java.util.HashMap;

//继承父类modle
public class MoreDescActivityModle extends BaseModel {

    private MoreDescActivityView mDescActivityView;
    private static final int VIDEOJUMPTYPE=0x230;
    private static final int VIDEOJUMPYRLTYPE=0x231;
    private static final String TAG="MoreDescActivityModle";
    private static final int VIDEODANMUYPE = 0x232;
    private static final int VIDEOADDDANMUYPE = 0x233;
    public void getJumpItem(Context context, MoreDescActivityView view, String positionid) {
        getString(context, Http.VIDEOMOREDESCLIST+positionid+".txt", null, VIDEOJUMPTYPE, true, true,true);
//        //view 强转本类的view返回值 提上去
        mDescActivityView =(MoreDescActivityView)view;
    }
  //请求视频地址
    public void getVideoUrl(Context context, MoreDescActivityView view,String stock) {
        HashMap<String,String> map= new HashMap<>();
        map.put("stock",stock);
        getString(context, Http.VIDEO_U_URL , map, VIDEOJUMPYRLTYPE, true, true,true);
        mDescActivityView =(MoreDescActivityView)view;
    }
    //获取视频弹幕
    public void getDanmakuData(Context context, MoreDescActivityView view, int page) {
        HashMap<String, String> map = new HashMap<>();
        map.put("open_id", "100");
        map.put("open_num", "10");
        map.put("open_page", page + "");
        getString(context, Http.HOMEPAFE_RECEIVEPLAYAPALYURL, map, VIDEODANMUYPE, false, false, false);
        mDescActivityView = ((MoreDescActivityView) view);
    }
    //    添加弹幕
    public void getAddDanmaKu(Context context, MoreDescActivityView view, String danmaKuText,String userid,String username) {
        HashMap<String, String> map = new HashMap<>();
        map.put("open_user", userid);
        map.put("open_nickname", username);
        map.put("open_id", "100");
        map.put("open_content", danmaKuText);
        getString(context, Http.HOMEPAFE_SENDPLAYAPALYURL, map, VIDEOADDDANMUYPE, false, false, false);
        mDescActivityView = ((MoreDescActivityView) view);
    }
    //成功方法
    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        //选择类型
        switch (type) {
            //更多视频列表跳过去
            case VIDEOJUMPTYPE:
                MoreJumpBean moreJumpBean = new Gson().fromJson(data, MoreJumpBean.class);
                mDescActivityView.getJumpItem(moreJumpBean);
                break;
                //更多视频列表跳过去
            case VIDEOJUMPYRLTYPE:
                //new ggson from  传data 和bean 类
                Logger.i(TAG, "数据" + data);
                mDescActivityView.getVideoUrl(data);
                break;
            case VIDEODANMUYPE:
                //new ggson from  传data 和bean 类
                Logger.i("视频弹幕", "数据" + data);
                mDescActivityView.getDanmakuData(data);
                break;
            case VIDEOADDDANMUYPE:
                //new ggson from  传data 和bean 类
                Logger.i("弹幕发表", "数据" + data);
                mDescActivityView.getAddDanmaKu(data);
                break;
        }
    }

    //失败方法
    @Override
    public void failString(String msg) {
        super.failString(msg);
        //打印失败
        Logger.i(TAG, "失败啦");
    }


}
