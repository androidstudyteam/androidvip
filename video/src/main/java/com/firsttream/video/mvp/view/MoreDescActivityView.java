package com.firsttream.video.mvp.view;

/*
 * 作者:马富燕
 * 日期:2018/12/18
 * 作用:更多视频内容的  view   接口 MoreVideoView
 * */

import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.video.entity.MoreJumpBean;

//继承契约类的view  接口
public interface MoreDescActivityView extends IContract.View {
    void getJumpItem(MoreJumpBean moreJumpBean);

    void getVideoUrl(String data);

    void getDanmakuData(String data);

    void getAddDanmaKu(String data);
}
