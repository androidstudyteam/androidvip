package com.firsttream.video.mvp.modle;

import android.content.Context;

import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.video.entity.MoreVideoDescBean;
import com.firsttream.video.mvp.view.FragmentMoreDescView;
import com.google.gson.Gson;

/**
 * 作者:马富燕
 * 日期:2018/12/18
 * 作用:tablayout 内容的fragmentmodle FragmentClassicModle
 */

//继承父类modle
public class FragmentMoreDescModle extends BaseModel {

    //设置常量类型
    private static final int VIDEORVDESCTYPE = 0x229;
    private FragmentMoreDescView mMoreDescView;

    public void getRvDesc(Context context, String mType, FragmentMoreDescView view) {
        //请求get网络请求
        getString(context, Http.VIDEOMOREDESC +mType+".txt", null, VIDEORVDESCTYPE, true, true, true);
        //强转自己view返回值提上去
        mMoreDescView = (FragmentMoreDescView) view;
    }

    //重写成功方法
    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        //选择类型
        switch (type){
            case VIDEORVDESCTYPE:
                //new gson  from 传data 和bean类
                MoreVideoDescBean moreVideoDescBean = new Gson().fromJson(data, MoreVideoDescBean.class);
                //调用上面的关于本类的view生成方法到frgamnetview 里面括号里传bean类
                mMoreDescView.getRvDesc(moreVideoDescBean);
                break;
        }
    }


}
