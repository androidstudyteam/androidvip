package com.firsttream.video.mvp.modle;

/*
 * 作者:马富燕
 * 日期:2018/12/18
 * 作用:更多视频的  molde MoreVideoModle
 * */

import android.content.Context;

import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.video.entity.MoreVideoBean;
import com.firsttream.video.mvp.view.MoreVideoView;
import com.google.gson.Gson;

//继承父类modle
public class MoreVideoModle extends BaseModel{

    //设置常量类型
    private static final int MOREVIDEOTYPE=0x228;
    private static final String TAG="MoreVideoModle";

    private MoreVideoView mMoreVideoView;

    public void getMoreVideoTitiles(Context context, MoreVideoView view) {
        //直接调用get传网址和map如果没有就填空 和类型
        getString(context, Http.VIDEOMORETITLE, null, MOREVIDEOTYPE,false,false,false);
        //自己的View 强转一下自己的view 返回值 提上去
        mMoreVideoView =(MoreVideoView)view;
    }
    //重写成功方法
    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        //选择type
        switch (type) {
            case MOREVIDEOTYPE:
                //new gson 传data 和bean类
                MoreVideoBean moreVideoBean = new Gson().fromJson(data, MoreVideoBean.class);
                //调用上面的classview生成到这个类的View 括号里传bean类
                mMoreVideoView.getMoreVideoTitles(moreVideoBean);
                break;
        }
    }

    //重写失败方法
    @Override
    public void failString(String msg) {
        super.failString(msg);
        //打印log失败
        Logger.i(TAG, "失败");
    }
}
