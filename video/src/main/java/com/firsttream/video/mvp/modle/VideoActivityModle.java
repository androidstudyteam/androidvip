package com.firsttream.video.mvp.modle;

/*
 * 作者:马富燕
 * 日期:2018/12/13
 * 作用:视频的  molde VideoActivityModle
 * */

import android.content.Context;

import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.video.mvp.view.VideoActivityView;

import java.util.HashMap;

//继承父类modle
public class VideoActivityModle extends BaseModel {
    private static final int VIDEOITEMTYPE = 0x226;
    private static final int VIDEOURLYPE = 0x227;
    private static final int VIDEODANMUYPE = 0x232;
    private static final int VIDEOADDDANMUYPE = 0x233;
    private VideoActivityView videoActivityView;

    //获取视频条目
    public void getItem(Context context, IContract.View view, String positionid) {
//        //创建hasmap
//        HashMap<String,String> map=new HashMap<>();
//        //往map里面传位置
//        map.put("position",positionid+".txt");
//        //get请求传上下文后面两true 其他一样和别的
        getString(context, Http.VIDEORVURL + positionid + ".txt", null, VIDEOITEMTYPE, true, true, true);
//        //view 强转本类的view返回值 提上去
        videoActivityView = ((VideoActivityView) view);
    }

    //获取视频地址
    public void getVideoUrl(Context context, VideoActivityView view, String stock) {
        HashMap<String, String> map = new HashMap<>();
        map.put("stock", stock);
        getString(context, Http.VIDEO_U_URL, map, VIDEOURLYPE, true, true, true);
        videoActivityView = ((VideoActivityView) view);
    }

    //获取视频弹幕
    public void getDanmakuData(Context context, VideoActivityView view, int page) {
        HashMap<String, String> map = new HashMap<>();
        map.put("open_id", "100");
        map.put("open_num", "10");
        map.put("open_page", page + "");
        getString(context, Http.HOMEPAFE_RECEIVEPLAYAPALYURL, map, VIDEODANMUYPE, false, false, false);
        videoActivityView = ((VideoActivityView) view);
    }

    //    添加弹幕
    public void getAddDanmaKu(Context context, VideoActivityView view, String danmaKuText,String userid,String username) {
        HashMap<String, String> map = new HashMap<>();
        map.put("open_user", userid);
        map.put("open_nickname", username);
        map.put("open_id", "100");
        map.put("open_content", danmaKuText);
        getString(context, Http.HOMEPAFE_SENDPLAYAPALYURL, map, VIDEOADDDANMUYPE, false, false, false);
        videoActivityView = ((VideoActivityView) view);
    }

    //成功方法
    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        //选择类型
        switch (type) {
            case VIDEOITEMTYPE:
                //new ggson from  传data 和bean 类
//                Logger.i("视频列表", "数据" + data);
                videoActivityView.getItem(data);
                break;
            case VIDEOURLYPE:
                //new ggson from  传data 和bean 类
//                Logger.i("视频url", "数据" + data);
                videoActivityView.getVideoUrl(data);
                break;
            case VIDEODANMUYPE:
                //new ggson from  传data 和bean 类
                Logger.i("视频弹幕", "数据" + data);
                videoActivityView.getDanmakuData(data);
                break;
            case VIDEOADDDANMUYPE:
                //new ggson from  传data 和bean 类
                Logger.i("弹幕发表", "数据" + data);
                videoActivityView.getAddDanmaKu(data);
                break;
        }
    }

    //失败方法
    @Override
    public void failString(String msg) {
        super.failString(msg);
        //打印失败
        Logger.i("获取视频列表", "失败啦");
    }


}
