package com.firsttream.video.mvp.persenter;

/*
 * 作者:马富燕
 * 日期:2018/12/13
 * 作用:视频的  persenter  VideoActivityPersenter
 * */

import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.video.mvp.modle.VideoActivityModle;
import com.firsttream.video.mvp.view.VideoActivityView;

//继承父类persenter 泛型传这个类关于的modle和view
public class VideoActivityPersenter extends BasePersenter<VideoActivityModle, VideoActivityView> {

    @Override
    protected void onViewDestory() {
        //判断modle非空
        if (model != null) {
            //非空就用modle调用方法
            model.stopRequest();
        }
    }

    public void getItem(String positionid) {
        //调用modle设置获取条目方法 传上下文和获取view方法
        model.getItem(context, getView(), positionid);
    }

    public void getVideoUrl(String stock) {
        model.getVideoUrl(context, getView(), stock);
    }

    //获取弹幕数据
    public void getDanmakuData(int page) {
        model.getDanmakuData(context, getView(),page);
    }

    public void getAddDanmaKu(String danmaKuText,String userid,String username) {
        model.getAddDanmaKu(context, getView(),danmaKuText,userid,username);
    }
}
