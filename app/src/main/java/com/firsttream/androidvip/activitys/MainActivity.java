package com.firsttream.androidvip.activitys;


import android.support.v4.app.FragmentManager;

import com.firsttream.androidvip.R;
import com.firsttream.androidvip.fragments.FragmentAccount;
import com.firsttream.androidvip.fragments.FragmentClassic;
import com.firsttream.androidvip.fragments.FragmentHomePage;
import com.firsttream.androidvip.fragments.FragmentLearn;
import com.firsttream.androidvip.fragments.FragmentVideo;
import com.firsttream.androidvip.tabview.TabView;
import com.firsttream.androidvip.tabview.TabViewChild;
import com.firsttream.baselibrary.base.BaseActivity;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者：周建峰
 * 时间：2018-12-13
 * MainActivity
 */
public class MainActivity<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseActivity<M, V, P> {

    private List<TabViewChild> tabchilds = new ArrayList<>();
    private FragmentManager manger;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initData() {
        super.initData();
        setShopBackBar(false);
        setShowBar(false);
        final TabView tabView = (TabView) getView(R.id.tabview);
        manger = getSupportFragmentManager();
        TabViewChild tabHome = new TabViewChild(R.drawable.index_yes, R.drawable.index_no, "首页", new FragmentHomePage<>());
        TabViewChild tabLrean = new TabViewChild(R.drawable.study_yes, R.drawable.study_no, "学习", new FragmentLearn<>());
        TabViewChild tabVedio = new TabViewChild(R.drawable.live_yes, R.drawable.live_no, "视频", new FragmentVideo<>());
        TabViewChild tabClassic = new TabViewChild(R.drawable.gan_yes, R.drawable.gan_no, "经典", new FragmentClassic<>());
        TabViewChild tabAccount = new TabViewChild(R.drawable.account_yes, R.drawable.account_no, "我的", new FragmentAccount<>());
        tabchilds.add(tabHome);
        tabchilds.add(tabLrean);
        tabchilds.add(tabVedio);
        tabchilds.add(tabClassic);
        tabchilds.add(tabAccount);
        manger = getSupportFragmentManager();
        tabView.setTabViewChild(tabchilds, manger);
    }

    @Override
    public M createModel() {
        return null;
    }

    @Override
    public V createView() {
        return null;
    }

    @Override
    public P createPersenter() {
        return null;
    }

}