package com.firsttream.androidvip.activitys;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.firsttream.androidvip.R;
import com.firsttream.androidvip.launcher.LauncherView;
import com.firsttream.baselibrary.util.UltimateBar;

/**
 * 作者:马富燕
 * 日期:2018/12/17
 * 作用:启动页Activity
 */

public class StartActivity extends AppCompatActivity {
    private LauncherView mLvloadView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        //沉浸式
        UltimateBar.newImmersionBuilder().applyNav(false)
                .build(this).apply();
        //初始化数据方法
        initwidget();
        //new 线程
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLvloadView.start();
            }
        }, 500);

    }

    //初始化数据方法
    private void initwidget() {
        mLvloadView = (LauncherView) findViewById(R.id.lv_load_view);
    }

    //销毁activity
    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
