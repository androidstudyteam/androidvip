package com.firsttream.androidvip;

import android.support.multidex.MultiDex;

import com.firsttream.baselibrary.BaseApp;
import com.firsttream.baselibrary.util.CrashHandler;

public   class App extends BaseApp {
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        //全局异常
        CrashHandler.getInstance().init(this);
    }
}
