package com.firsttream.androidvip.entity;
/***
 *作者:XJH
 *时间:2018-12-13
 *作用:MenuPopwindowBean
 */
public    class MenuPopwindowBean   {
    private int icon;
    private String text;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
