package com.firsttream.androidvip.entity;

import java.util.List;

/**
 * 作者:xjh
 * 时间:2018-12-12
 * 作用:BannerBean
 */
public class BannerBean {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : 乐视有毒
         * link : https://www.huxiu.com/article/273393.html
         * image : https://img.huxiucdn.com/article/cover/201811/26/074214125875.jpg
         */

        private String title;
        private String link;
        private String image;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
