package com.firsttream.androidvip.entity;

/**
 * 作者：周建峰
 * 时间：2018-12-13
 * 我的列表的entity
 */
public class IteamContentBean {
    private String name;
    private int id;

    public IteamContentBean(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}