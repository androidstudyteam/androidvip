package com.firsttream.androidvip.entity;

import java.util.List;
/**
 * 作者:马富燕
 * 日期:2018/12/14
 * 作用:经典标题bean类
 */
public class ClassicTitleBean {

    private List<TitlesBean> titles;

    public List<TitlesBean> getTitles() {
        return titles;
    }

    public void setTitles(List<TitlesBean> titles) {
        this.titles = titles;
    }

    public static class TitlesBean {
        /**
         * name : 优质文章
         * id : 1
         */

        private String name;
        private String id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
