package com.firsttream.androidvip.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.firsttream.androidvip.R;
import com.firsttream.androidvip.entity.LearnBean;
import com.firsttream.baselibrary.activityhelper.ActionHelper;
import com.firsttream.baselibrary.base.recycle.RecycleAdapter;
import com.firsttream.baselibrary.base.viewholder.ViewHolder;
import com.firsttream.learn.activitys.ActivityLearnList;

/**
*作者：gaojiabao
*时间：2018/12/13 11:17
*作用：学习页面适配器
*/
public class LearnAdapter extends RecycleAdapter<LearnBean.ItemsBean>{

    private  Context context;
    public LearnAdapter(Context mcontext) {
        super(mcontext);
        this.context=mcontext;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item_learn_recy;
    }

    @Override
    protected void convert(ViewHolder viewHolder, LearnBean.ItemsBean itemsBean, int postion) {

       viewHolder.setSimpleDraweViewUrl(R.id.item_learn_simp,itemsBean.getImage());
       viewHolder.setText(R.id.item_learn_te1,itemsBean.getTitle());
       viewHolder.setClick(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent = new Intent(context,ActivityLearnList.class);
               intent.putExtra("study_id",itemsBean.getStudy_id());
               intent.putExtra("islist",itemsBean.getIslist()+"");
               intent.putExtra("title",itemsBean.getTitle());
               context.startActivity(intent);
//               Toast.makeText(context,"点击了"+itemsBean.getStudy_id(),Toast.LENGTH_SHORT).show();
           }
       }, R.id.learn_layout);
    }
}
