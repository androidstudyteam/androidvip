package com.firsttream.androidvip.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.firsttream.androidvip.R;
import com.firsttream.androidvip.entity.VideoHomeBean;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.video.activitys.MoreVideoActivity;

import java.util.List;

/**
 * 作者:高佳宝
 * 日期:2018/12/1
 * 作用:视频的列表适配器
 */

public class VideoHeadFootAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int HEAD_TYPE = 00001;
    private static final int BODY_TYPE = 00002;
    private static final int FOOT_TYPE = 00003;
    private Context context;
    private int headCount = 1;//头部个数，后续可以自己拓展
    private int footCount = 1;//尾部个数，后续可以自己拓展
    private LayoutInflater mLayoutInflater;
    private List<VideoHomeBean.ItemsBean> listData;

    public VideoHeadFootAdapter(Context context, List<VideoHomeBean.ItemsBean> listData) {
        this.context = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.listData = listData;
    }

    private int getBodySize() {
        return listData.size() - 1;
    }

    private boolean isHead(int position) {
        return headCount != 0 && position < headCount;
    }

    private boolean isFoot(int position) {
        return footCount != 0 && (position >= (getBodySize() + headCount));
    }

    @Override
    public int getItemViewType(int position) {
        if (isHead(position)) {
            return HEAD_TYPE;
        } else if (isFoot(position)) {
            return FOOT_TYPE;
        } else {
            return BODY_TYPE;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEAD_TYPE:
            case BODY_TYPE:
                return new BodyViewHolder(mLayoutInflater.inflate(R.layout.video_rv_item, parent, false));
            case FOOT_TYPE:
                return new FootViewHolder(mLayoutInflater.inflate(R.layout.video_foot_rv_item, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeadViewHolder) {
        } else if (holder instanceof BodyViewHolder) {
            ((BodyViewHolder) holder).title.setText(listData.get(position).getTitle());
            ((BodyViewHolder) holder).rvimg.setImageURI(Uri.parse(listData.get(position).getImage()));
        } else if (holder instanceof FootViewHolder) {
            ((FootViewHolder) holder).moretext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   context.startActivity(new Intent(context, MoreVideoActivity.class));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return headCount + getBodySize() + footCount;
    }

    //头部的viewholder
    private static class HeadViewHolder extends RecyclerView.ViewHolder {

        public HeadViewHolder(View itemView) {
            super(itemView);
        }

    }

    //中间部分的viewholder
    private static class BodyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        SimpleDraweeView rvimg;

        public BodyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.video_rv_title);
            rvimg = (SimpleDraweeView) itemView.findViewById(R.id.video_rv_img);
        }
    }

    //尾部的viewholder
    private static class FootViewHolder extends RecyclerView.ViewHolder {
        TextView moretext;
        public FootViewHolder(View itemView) {
            super(itemView);
            moretext=(TextView) itemView.findViewById(R.id.video_foot_more);
        }

    }




}
