package com.firsttream.androidvip.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.firsttream.androidvip.R;
import com.firsttream.androidvip.entity.VideoHomeBean;
import com.firsttream.video.activitys.MoreVideoActivity;
import com.firsttream.video.activitys.VideoActivity;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.ArrayList;
import java.util.List;
/**
 * 作者:高佳宝
 * 日期:2018/12/19
 * 作用:视频的适配器
 */
public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {

    private Context context;
    private View view;
    private List<VideoHomeBean.ItemsBean> list=new ArrayList<>();
    //封装一个位置
    private String firstpo;
    //封装一个位置
    private String firsttitle;
    //封装一个位置
    private String firstimage;

    public void setData(String firstpo, String firsttitle, String firstimage) {
        this.firstpo = firstpo;
        this.firsttitle = firsttitle;
        this.firstimage = firstimage;
    }

    public VideoAdapter(Context context) {
        this.context = context;
    }

    public void setList(List<VideoHomeBean.ItemsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = View.inflate(context, R.layout.video_layout_item, null);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        myViewHolder.video_img = view.findViewById(R.id.video_img);
        myViewHolder.video_title = view.findViewById(R.id.video_title);
        myViewHolder.video_foot_more = view.findViewById(R.id.video_foot_more);
        myViewHolder.video_layout_xrecy = view.findViewById(R.id.video_layout_xrecy);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        //图片重新赋值
        myViewHolder.video_img.setImageURI(Uri.parse(firstimage));
        //图片点击事件
        myViewHolder.video_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //创建意图传位置跳转Activity
                //跳转Activity
                //new 意图
                Intent intent = new Intent(context, VideoActivity.class);
                //我那个意图里传位置和标题
                intent.putExtra("positionid",firstpo);
                intent.putExtra("titles",firsttitle);
                intent.putExtra("videoimgae",firstimage);
                //上下文启动跳转 传意图
                context.startActivity(intent);
            }
        });
        myViewHolder.video_title.setText(firsttitle);
        //跳转更多
        myViewHolder.video_foot_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               context.startActivity(new Intent(context, MoreVideoActivity.class));
            }
        });
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        myViewHolder.video_layout_xrecy.setLayoutManager(gridLayoutManager);
        VideoHomeAdapter videoHomeAdapter = new VideoHomeAdapter(context);
        myViewHolder.video_layout_xrecy.setAdapter(videoHomeAdapter);
        videoHomeAdapter.setList(list);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
        }
        SimpleDraweeView video_img;
        TextView video_title, video_foot_more;
        RecyclerView video_layout_xrecy;
    }


}
