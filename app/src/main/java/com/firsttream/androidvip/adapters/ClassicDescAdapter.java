package com.firsttream.androidvip.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.firsttream.androidvip.R;
import com.firsttream.androidvip.entity.ClassicDescBean;
import com.firsttream.baselibrary.base.recycle.RecycleAdapter;
import com.firsttream.baselibrary.base.viewholder.ViewHolder;
import com.firsttream.classic.activitys.ClassicActivity;

/**
 *
 * 作者:马富燕
 * 日期:2018/12/14
 * 作用:经典内容的适配器
 */

//继承父类适配器 泛型写本页面的bean类  在生成方法
public class ClassicDescAdapter extends RecycleAdapter<ClassicDescBean.ItemsBean> {

    //删了final
    private Context mcontext;

    public ClassicDescAdapter(Context mcontext) {
        super(mcontext);
        //this.上下文=上下文提上去
        this.mcontext=mcontext;
    }

    @Override
    protected int getLayoutId() {
        //返回recyclerview条目布局
        return R.layout.desc_rv_item;
    }

    @Override
    protected void convert(ViewHolder viewHolder, ClassicDescBean.ItemsBean itemsBean, int postion) {
        //赋值给控件
        viewHolder.setText(R.id.desc_rv_title,itemsBean.getNews_title());
        viewHolder.setText(R.id.desc_rv_author,itemsBean.getNews_author());
        viewHolder.setText(R.id.desc_rv_time,itemsBean.getNews_time());
        viewHolder.setText(R.id.desc_rv_desc,itemsBean.getNews_desc());
        //设置图片
        viewHolder.setUrlImage(R.id.desc_rv_img,itemsBean.getNews_image());
        //条目布局点击事件
        viewHolder.setClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //跳转Activity
                //new 意图
                Intent intent = new Intent(mcontext, ClassicActivity.class);
                //我那个意图里传网址和标题
                intent.putExtra("weburl",itemsBean.getNews_link());
                //上下文启动跳转 传意图
                mcontext.startActivity(intent);
            }
        }, R.id.desc_layout);
    }
}
