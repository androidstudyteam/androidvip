package com.firsttream.androidvip.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.firsttream.androidvip.R;
import com.firsttream.androidvip.activitys.MainActivity;
import com.firsttream.androidvip.entity.ButtonArticleBean;
import com.firsttream.baselibrary.base.recycle.RecycleAdapter;
import com.firsttream.baselibrary.base.viewholder.ViewHolder;
import com.firsttream.homepage.activitys.ActivityHomePageItem;

/**
 * 作者:xjh
 * 时间:2018-12-12
 * 作用:ButtonAticleAdapter
 */
public class ButtonAticleAdapter extends RecycleAdapter<ButtonArticleBean.ItemsBean> {
    private  Context context;


    public ButtonAticleAdapter(Context mcontext) {
        super(mcontext);
        this.context=mcontext;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.buttonaticleadapter_xrecycle_item;
    }

    @Override
    protected void convert(ViewHolder viewHolder, ButtonArticleBean.ItemsBean itemsBean, int postion) {
        viewHolder.setUrlImage(R.id.homepage_img_buttomaticleimg,itemsBean.getNews_image()).setText(R.id.homepage_txt_buttomaticletitle,itemsBean.getNews_title())
        .setText(R.id.homepage_txt_buttomaticledesc,itemsBean.getNews_desc())  .setText(R.id.homepage_txt_buttomaticleauthor,itemsBean.getNews_author())
                .setText(R.id.homepage_txt_buttomaticletime,itemsBean.getNews_time());
        viewHolder.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ActivityHomePageItem.class);
                Bundle bundle = new Bundle();
                bundle.putString("itemsBeanDesc",itemsBean.getNews_link());
                intent.putExtras(bundle);
                ((MainActivity)context).startActivity(intent);
            }
        });

    }


}
