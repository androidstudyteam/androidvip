package com.firsttream.androidvip.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.firsttream.account.activitys.ActivityAttention;
import com.firsttream.account.activitys.ActivityAttentionOuther;
import com.firsttream.account.activitys.ActivityLoginOrRegister;
import com.firsttream.account.activitys.ActivitySettings;
import com.firsttream.account.activitys.ActivityShare;
import com.firsttream.androidvip.R;
import com.firsttream.androidvip.entity.IteamContentBean;
import com.firsttream.baselibrary.base.recycle.RecycleAdapter;
import com.firsttream.baselibrary.base.viewholder.ViewHolder;
import com.firsttream.baselibrary.util.SpUtil;

/**
 * 作者：周建峰
 * 时间：2018-12-13
 * 我的列表的adapter
 */
public class IteamAdapter extends RecycleAdapter<IteamContentBean> {

    private Context context;

    public IteamAdapter(Context mcontext) {
        super(mcontext);
        this.context = mcontext;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.layout_iteam_account;
    }

    /**
     * 获取是否登录
     * @return
     */
    public boolean isLogin() {
        boolean isLogin = (boolean) SpUtil.getSpData(context, "isLogin", false);
        return isLogin;
    }
    @Override
    protected void convert(ViewHolder viewHolder, IteamContentBean iteamContentBean, int postion) {
        viewHolder.setImageResource(R.id.iv_iteam_image, iteamContentBean.getId())
                .setText(R.id.txt_iteam_content, iteamContentBean.getName());
        viewHolder.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLogin()){
                    switch (postion){
                        case 0:
                            context.startActivity(new Intent(context, ActivityAttention.class));
                            break;
                        case 1:
                            context.startActivity(new Intent(context, ActivityAttentionOuther.class));
                            break;
                        case 2:
                            context.startActivity(new Intent(context,ActivityShare.class));
                            break;
                        case 5:
                            context.startActivity(new Intent(context,ActivitySettings.class));
                            break;
                    }
                }else{
                    context.startActivity(new Intent(context, ActivityLoginOrRegister.class));
                }
            }
        });
    }
}