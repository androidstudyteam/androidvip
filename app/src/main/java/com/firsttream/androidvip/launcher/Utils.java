package com.firsttream.androidvip.launcher;

import android.content.Context;

/**
 * 作者: 马富燕
 * 日期: 2018/12/20
 * 作用: 启动页自定义view
 */
public class Utils {
    /**
     * dp转px
     *
     * @param context 上下文
     * @param dpValue dp值
     * @return px值
     */
    public static int dp2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

}
