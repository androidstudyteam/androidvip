package com.firsttream.androidvip.fragments;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.firsttream.androidvip.R;
import com.firsttream.androidvip.adapters.VideoAdapter;
import com.firsttream.androidvip.adapters.VideoHeadFootAdapter;
import com.firsttream.androidvip.entity.VideoHomeBean;
import com.firsttream.androidvip.model.FragmentVideoModle;
import com.firsttream.androidvip.persenter.FragmentVideoPersenter;
import com.firsttream.androidvip.view.FragmentVideoView;
import com.firsttream.baselibrary.base.BaseFragment;
import com.firsttream.video.activitys.VideoActivity;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.scwang.smartrefresh.header.BezierCircleHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

/**
 * 作者:马富燕
 * 日期:2018/12/12
 * 作用:视频的fragment FragmentVideo
 */

//如果需要用到网络请求 就继承自己创建类的modle view persenter 如果不用继承原先的就行 在实现一下这个类相关的view接口
public class FragmentVideo<M extends FragmentVideoModle, V extends FragmentVideoView, P extends FragmentVideoPersenter> extends BaseFragment<M, V, P> implements FragmentVideoView {

    private FragmentVideoPersenter mFragmentVideoPersenter;
    private RecyclerView mViDeoRv;
    private RefreshLayout mVideoreFreshLayout;

    @Override
    protected int getLayoutId() {
        //返回本页面的布局
        return R.layout.fragment_video;
    }

    @Override
    public M createModel() {
        //返回 new 本类相关的modle
        return (M) new FragmentVideoModle();
    }

    @Override
    public V createView() {
        //直接返回this  强转
        return (V) this;
    }

    @Override
    public P createPersenter() {
        //new 这类相关的frgament  提上去 下面返回强转
        mFragmentVideoPersenter = new FragmentVideoPersenter();
        mFragmentVideoPersenter.initContext(getActivity());
        return (P) mFragmentVideoPersenter;
    }

    //重写初始化数据方法
    @Override
    public void initData() {
        super.initData();
        //设置标题的方法引号自己的标题
        setTitle("视频");
        //初始化数据方法
        initwidget();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //给recycler设置
        mViDeoRv.setLayoutManager(linearLayoutManager);
        //调用本类的persenter 生成网络请求方法到persenter里面
        mFragmentVideoPersenter.getVideoHome();
        //设置 Header 为 贝塞尔雷达 样式
        mVideoreFreshLayout.setRefreshHeader(new BezierCircleHeader(getActivity())).setPrimaryColorsId(R.color.colorYellow);
        //设置 Footer 为 球脉冲 样式
        mVideoreFreshLayout.setRefreshFooter(new BallPulseFooter(getActivity()).setSpinnerStyle(SpinnerStyle.Scale));
        mVideoreFreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                //网络请求
                mFragmentVideoPersenter.getVideoHome();
                refreshlayout.finishRefresh(1000/*,false*/);//传入false表示刷新失败
            }
        });
        mVideoreFreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                refreshlayout.finishLoadMore(false);//传入false表示加载失败
            }
        });
    }

    //初始化数据方法
    private void initwidget() {
        //获取控件强转提上去
        mViDeoRv=(RecyclerView)getView(R.id.video_rv);
        mVideoreFreshLayout = (RefreshLayout) getView(R.id.video_refreshLayout);
    }

    //这是view里重写的方法
    @Override
    public void getVideoHome(VideoHomeBean videoHomeBean) {
        //先获取bean类的集合
        List<VideoHomeBean.ItemsBean> items = videoHomeBean.getItems();
        //设置适配器
        VideoAdapter videoAdapter = new VideoAdapter(getActivity());
        mViDeoRv.setAdapter(videoAdapter);
        videoAdapter.setList(items);
        //给适配器传第一条数据
        videoAdapter.setData(videoHomeBean.getPosition(),videoHomeBean.getTitle(),videoHomeBean.getImage());
    }


}
