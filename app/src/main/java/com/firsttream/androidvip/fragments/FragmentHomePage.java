package com.firsttream.androidvip.fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.firsttream.androidvip.R;
import com.firsttream.androidvip.activitys.MainActivity;
import com.firsttream.androidvip.adapters.ButtonAticleAdapter;
import com.firsttream.androidvip.customview.MenuPopwindow;
import com.firsttream.androidvip.entity.BannerBean;
import com.firsttream.androidvip.entity.ButtonArticleBean;
import com.firsttream.androidvip.entity.MenuPopwindowBean;
import com.firsttream.androidvip.model.FragmentHomePageModel;
import com.firsttream.androidvip.persenter.FragmentHomePagePersenter;
import com.firsttream.androidvip.view.FragmentHomePageView;
import com.firsttream.baselibrary.base.BaseFragment;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.homepage.activitys.ActivityQrCode;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.scwang.smartrefresh.header.BezierCircleHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.yzq.zxinglibrary.android.CaptureActivity;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.bgabanner.BGABanner;


/**
 * 作者:xjh
 * 时间:2018-12-12
 * 作用:FragmentHomePage
 */
public class FragmentHomePage<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseFragment<M, V, P> implements FragmentHomePageView {
    private final String TAG = FragmentHomePage.class.getName();
    private FragmentHomePagePersenter mPersenter;
    private XRecyclerView mXRecyGroundFloor;
    private boolean isChexked = true;
    private BGABanner mBGABanner;
    private List<String> mImages = new ArrayList<>();
    private List<String> mDescs = new ArrayList<>();
    private ButtonAticleAdapter mButtonAticleAdapter;
    private List<BannerBean.DataBean> data1;
    private RefreshLayout mHomePageFragmentRF;


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_homepage;
    }

    @Override
    public void initData() {
        super.initData();
        initWidget();
        setTitle("首页");
        setShowRight(true);
        mPersenter.getBanner();
        mPersenter.getButtomArticle();
    }

    private void initWidget() {
        mXRecyGroundFloor = (XRecyclerView) getView(R.id.homepage_xrecycler_groundfloor);
        View view = View.inflate(getContext(), R.layout.layout_homepage_groundfloorup, null);
        mBGABanner = (BGABanner) view.findViewById(R.id.homepage_bagbanner_banner);
        mHomePageFragmentRF = (RefreshLayout) getView(R.id.homepage_refresh_homepagefragment);
        mXRecyGroundFloor.addHeaderView(view);
        mButtonAticleAdapter = new ButtonAticleAdapter(getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mXRecyGroundFloor.setLayoutManager(linearLayoutManager);
        mXRecyGroundFloor.setAdapter(mButtonAticleAdapter);
        mXRecyGroundFloor.setPullRefreshEnabled(false);
        mXRecyGroundFloor.setLoadingMoreEnabled(false);
        //设置 Header 为 贝塞尔雷达 样式
        mHomePageFragmentRF.setRefreshHeader(new BezierCircleHeader(getContext())).setPrimaryColorsId(com.firsttream.homepage.R.color.colorYellow);
        //设置 Footer 为 球脉冲 样式
        mHomePageFragmentRF.setRefreshFooter(new BallPulseFooter(getContext()).setSpinnerStyle(SpinnerStyle.Scale));
        mHomePageFragmentRF.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                mPersenter.getButtomArticle();
                refreshlayout.finishRefresh(1000/*,false*/);//传入false表示刷新失败
            }
        });

        mHomePageFragmentRF.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                mPersenter.getButtomArticle();
                refreshlayout.finishLoadMore(1000/*,false*/);//传入false表示加载失败
            }
        });
    }

    @Override
    public M createModel() {
        return (M) new FragmentHomePageModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mPersenter = new FragmentHomePagePersenter();
        mPersenter.initContext(getActivity());
        return (P) mPersenter;
    }

    /**
     * banner图
     *
     * @param data
     */
    @Override
    public void getBanner(String data) {
        Gson gson = new Gson();
        BannerBean bannerBean = gson.fromJson(data, BannerBean.class);
        data1 = bannerBean.getData();
        for (int i = 0; i < data1.size(); i++) {
            mImages.add(data1.get(i).getImage());
            mDescs.add(data1.get(i).getTitle());
        }
        mBGABanner.setAdapter(new BGABanner.Adapter<ImageView, String>() {
            @Override
            public void fillBannerItem(BGABanner banner, ImageView itemView, @Nullable String model, int position) {
                Glide.with(getContext()).load(mImages.get(position)).into(itemView);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toase("点击banner" + position, 1);
                    }
                });
            }
        });
        mBGABanner.setData(mImages, mDescs);

    }

    @Override
    public void getButtomArticle(String data) {
        Gson gson = new Gson();
        ButtonArticleBean buttonArticleBean = gson.fromJson(data, ButtonArticleBean.class);
        mButtonAticleAdapter.setList(buttonArticleBean.getItems());
    }

    @Override
    public void rightClick() {
        int[] icons = {R.drawable.iv_scan, R.drawable.iv_qrcode};
        String[] texts = {"扫一扫", "我的二维码"};
        List<MenuPopwindowBean> list = new ArrayList<MenuPopwindowBean>();
        MenuPopwindowBean bean = null;
        for (int i = 0; i < texts.length; i++) {
            bean = new MenuPopwindowBean();
            bean.setIcon(icons[i]);
            bean.setText(texts[i]);
            list.add(bean);
        }
        MenuPopwindow pw = new MenuPopwindow((MainActivity) getContext(), list);
        pw.setOnItemClick(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Adapter adapter = parent.getAdapter();
                MenuPopwindowBean thisBean = (MenuPopwindowBean) adapter.getItem(position);
                switch (thisBean.getText()) {
                    case "扫一扫":
                        getContext().startActivity(new Intent(getContext(), CaptureActivity.class));
                        break;
                    case "我的二维码":
                        getContext().startActivity(new Intent(getContext(), ActivityQrCode.class));
                        break;
                }
            }
        });

        //获取屏幕的宽
        Resources resources = this.getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        float density = dm.density;
        int width = dm.widthPixels;
        pw.showPopupWindow(getView(R.id.homepage_view_popview), width, 0);
        isChexked = !isChexked;
    }


}
