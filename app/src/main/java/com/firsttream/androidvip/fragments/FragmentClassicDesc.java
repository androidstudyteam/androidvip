package com.firsttream.androidvip.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.firsttream.androidvip.R;
import com.firsttream.androidvip.adapters.ClassicDescAdapter;
import com.firsttream.androidvip.entity.ClassicDescBean;
import com.firsttream.androidvip.model.FragmentClassicDescModle;
import com.firsttream.androidvip.persenter.FragmentClassicDescPersenter;
import com.firsttream.androidvip.view.FragmentClassicDescView;
import com.firsttream.androidvip.view.FragmentVideoView;
import com.firsttream.baselibrary.base.BaseFragment;
import com.firsttream.baselibrary.util.Logger;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.scwang.smartrefresh.header.BezierCircleHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者:马富燕
 * 日期:2018/12/12
 * 作用:tablayout内容的fragment FragmentClassicDesc
 */

//如果需要用到网络请求 就继承自己创建类的modle view persenter 如果不用继承原先的就行 在实现一下这个类相关的view接口
public class FragmentClassicDesc<M extends FragmentClassicDescModle, V extends FragmentClassicDescView, P extends FragmentClassicDescPersenter> extends BaseFragment<M, V, P> implements FragmentClassicDescView {

    private RecyclerView mDescRv;
    private FragmentClassicDescPersenter mFragmentClassicDescPersenter;
    private List<ClassicDescBean.ItemsBean> mItems = new ArrayList<>();
    private String mId;
    private RefreshLayout mDescreFreshLayout;
    private ClassicDescAdapter mClassicDescAdapter;
    private static final String TAG="FragmentClassicDesc";

    @Override
    protected int getLayoutId() {
        //返回本页面的布局
        return R.layout.classic_tab_desc;
    }

    @Override
    public M createModel() {
        //返回 new 本类相关的modle
        return (M) new FragmentClassicDescModle();
    }

    @Override
    public V createView() {
        //直接返回this  强转
        return (V) this;
    }

    @Override
    public P createPersenter() {
        //new 这类相关的frgament  提上去 下面返回强转
        mFragmentClassicDescPersenter = new FragmentClassicDescPersenter();
        mFragmentClassicDescPersenter.initContext(getActivity());
        return (P) mFragmentClassicDescPersenter;
    }

    //重写初始化数据方法
    @Override
    public void initData() {
        super.initData();
        //给标题设置showbar 设置false为假隐藏
        setShowBar(false);
        //获取arguments返回值
        Bundle bundle = getArguments();
        //在从bundle里面获取id 获取引号id返回值 自己改成字符型 提上去
        mId = (String) bundle.get("id");
        //打印logger
        Logger.i(TAG, "哈哈哈" + mId);
        //初始化数据方法
        initwidget();
        //new recycler view 的线性布局
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        //设置方向
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //给recyclerview 设置线性管理器
        mDescRv.setLayoutManager(linearLayoutManager);

        //实例化适配器
         mClassicDescAdapter = new ClassicDescAdapter(getActivity());
        //给recyclerview 设置适配器
        mDescRv.setAdapter(mClassicDescAdapter);
        //先调用关于这个类的persenter设置方法生成到关于这个类的fragmentpersnet
        mFragmentClassicDescPersenter.getDesc(mId);
        //设置 Header 为 贝塞尔雷达 样式
        mDescreFreshLayout.setRefreshHeader(new BezierCircleHeader(getActivity())).setPrimaryColorsId(R.color.colorYellow);
        //设置 Footer 为 球脉冲 样式
        mDescreFreshLayout.setRefreshFooter(new BallPulseFooter(getActivity()).setSpinnerStyle(SpinnerStyle.Scale));
        mDescreFreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                //网络请求
                mFragmentClassicDescPersenter.getDesc(mId);
                refreshlayout.finishRefresh(1000/*,false*/);//传入false表示刷新失败
            }
        });
        mDescreFreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                //网络请求
                mFragmentClassicDescPersenter.getDesc(mId);
                refreshlayout.finishLoadMore(1000/*,false*/);//传入false表示加载失败
            }
        });
    }

    //初始化数据方法
    private void initwidget() {
        //获取控件 强转 提上去
        mDescRv = (RecyclerView) getView(R.id.desc_rv);
        mDescreFreshLayout= (RefreshLayout) getView(R.id.desc_refreshLayout);
    }

    //这是从实现frgamentview 里面生成的方法
    @Override
    public void getDesc(ClassicDescBean classicDescBean) {
        //先获取bean类的集合提上去 记得实例化 要是有关于这个集合操作的尽量写在这个方法里
        mItems = classicDescBean.getItems();
        Logger.i(TAG, "哈哈" + mItems);
        //给适配器设置集合
        mClassicDescAdapter.setList(mItems);

    }
}
