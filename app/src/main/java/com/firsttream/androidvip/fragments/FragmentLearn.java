package com.firsttream.androidvip.fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.firsttream.androidvip.R;
import com.firsttream.androidvip.adapters.LearnAdapter;
import com.firsttream.androidvip.entity.LearnBean;
import com.firsttream.androidvip.model.FragmentLearnModel;
import com.firsttream.androidvip.persenter.FragmentLearnPersenter;
import com.firsttream.androidvip.view.FragmentLearnView;
import com.firsttream.baselibrary.base.BaseFragment;
import com.firsttream.baselibrary.net.Http;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.scwang.smartrefresh.header.BezierCircleHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

/**
 * 作者：gaojiabao
 * 时间：2018/12/12 10:56
 * 作用：学习fragment
 */
public class FragmentLearn<M extends FragmentLearnModel, V extends FragmentLearnView, P extends FragmentLearnPersenter> extends BaseFragment<M, V, P> implements FragmentLearnView {
    private FragmentLearnPersenter mFragmentLearnPersenter;
    private RecyclerView mRecylerview;
    private LearnAdapter mLearnAdapter;
    private RefreshLayout mLearnrefreshLayout;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_learn;
    }

    @Override
    public M createModel() {
        return (M) new FragmentLearnModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        mFragmentLearnPersenter = new FragmentLearnPersenter();
        mFragmentLearnPersenter.initContext(getActivity());
        return (P) mFragmentLearnPersenter;
    }

    //初始化页面
    @Override
    public void initData() {
        super.initData();
        setTitle("学习");
        mFragmentLearnPersenter.getLearnData();
        initwidget();//初始化控件
        //设置 Header 为 贝塞尔雷达 样式
        mLearnrefreshLayout.setRefreshHeader(new BezierCircleHeader(getActivity())).setPrimaryColorsId(R.color.colorYellow);
        //设置 Footer 为 球脉冲 样式
        mLearnrefreshLayout.setRefreshFooter(new BallPulseFooter(getActivity()).setSpinnerStyle(SpinnerStyle.Scale));
        mLearnrefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                //网络请求
                mFragmentLearnPersenter.getLearnData();
                refreshlayout.finishRefresh(1000/*,false*/);//传入false表示刷新失败
            }
        });
        mLearnrefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                //网络请求
                mFragmentLearnPersenter.getLearnData();
                refreshlayout.finishLoadMore(1000/*,false*/);//传入false表示加载失败
            }
        });
    }

    //初始化控件
    private void initwidget() {
        mRecylerview = (RecyclerView) getView(R.id.fragment_learn_recyview);
        mLearnrefreshLayout = (RefreshLayout) getView(R.id.learn_refreshLayout);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecylerview.setLayoutManager(linearLayoutManager);

    }

    @Override
    public void getLearnData(LearnBean learnBean) {
        List<LearnBean.ItemsBean> learnlist = learnBean.getItems();
        mLearnAdapter = new LearnAdapter(getActivity());
        mRecylerview.setAdapter(mLearnAdapter);
        mLearnAdapter.setList(learnlist);
    }

}
