package com.firsttream.androidvip.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.firsttream.androidvip.R;
import com.firsttream.androidvip.entity.ClassicTitleBean;
import com.firsttream.androidvip.model.FragmentClassicModle;
import com.firsttream.androidvip.persenter.FragmentClassicPersenter;
import com.firsttream.androidvip.view.FragmentClassicView;
import com.firsttream.baselibrary.base.BaseFragment;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者:马富燕
 * 日期:2018/12/12
 * 作用:经典的fragment FragmentClassic
 */

//如果需要用到网络请求 就继承自己创建类的modle view persenter 如果不用继承原先的就行  在实现一下这个类相关的view接口
public class FragmentClassic<M extends FragmentClassicModle, V extends FragmentClassicView, P extends FragmentClassicPersenter> extends BaseFragment<M, V, P> implements FragmentClassicView{

    private FragmentClassicPersenter mFragmentClassicPersenter;
    private TabLayout mClassicTab;
    private List<Fragment> mFragments=new ArrayList<>();
    private ViewPager mClassicVp;
    private List<ClassicTitleBean.TitlesBean> mTitles=new ArrayList<>();

    @Override
    protected int getLayoutId() {
        //返回本页面的布局
        return R.layout.fragment_classic;
    }

    @Override
    public M createModel() {
        //返回 new 自己的经典frgament的modle 强转
        return (M) new FragmentClassicModle();
    }

    @Override
    public V createView() {
        //返回this 强转
        return (V) this;
    }

    @Override
    public P createPersenter() {
        //先new  经典的frgamentpersenter 返回值 提上去 删了泛型
        mFragmentClassicPersenter = new FragmentClassicPersenter();
        //在返回提上去的persenter 强转
        mFragmentClassicPersenter.initContext(getActivity());
        return (P) mFragmentClassicPersenter;
    }

    //重写初始化数据方法
    @Override
    public void initData() {
        super.initData();
        //设置标题的方法引号自己的标题
        setTitle("经典");
        //初始化数据方法
        initwidget();
        //先给这个类的persenter写一个请求 网络数据的方法  生成到自己的persenter
        mFragmentClassicPersenter.getClassicTitles();
    }

    //初始化数据方法
    private void initwidget() {
        //获取控件的方法记得强转
        mClassicTab=(TabLayout)getView(R.id.fragmentclassic_tab);
        mClassicVp=(ViewPager)getView(R.id.fragmentclassic_vp);
    }

    //这是在view里写方法之后 重写的方法
    @Override
    public void getClassicTitles(ClassicTitleBean classicTitleBean) {
        //获取传过来的Bean类里的集合  返回值 提上去 记得实例化
        mTitles = classicTitleBean.getTitles();
         //因为这是从modle view传过来的集合 所有 关于这个集合使用的情况下 把循环 和设置适配器设置tab 一类都放到这个方法里面
        //循环标题的数据长度 我那个fragment里new frgament
        for(int i=0;i<mTitles.size();i++){
            //往集合里new 添加frgament
            mFragments.add(new FragmentClassicDesc<>());
        }
        //设置适配器 继承frgament pager
        //实例化适配器 传获取activity.getSupportFragmentManager
        MyAdapter myAdapter=new MyAdapter(getChildFragmentManager());
        //再给vp设置适配器
        mClassicVp.setAdapter(myAdapter);
        //设置ViewPage缓存界面数 有几页就缓存几页
        mClassicVp.setOffscreenPageLimit(4);
        //给tab设置模型 括号里传大写tab mode scrollable
//        ctab.setTabMode(TabLayout.MODE_SCROLLABLE);
        //给tab设置upwithviewpager 传cvp
        mClassicTab.setupWithViewPager(mClassicVp);
    }

    //设置适配器继承frgamentpager适配器
     public class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            //返回fragmments集合获取位置
            //获取fragments的位置 返回值
            Fragment fragment = mFragments.get(i);
            //new bundle 返回值
            Bundle bundle = new Bundle();
            //往bundle里面put字符串 传id 集合里的id
            bundle.putString("id",mTitles.get(i).getId());
            //给frgament 设置arguments 括号里传bundle
            fragment.setArguments(bundle);
            //直接返回fragment
            return fragment;
        }

        @Override
        public int getCount() {
            //返回fragments集合大小
            return mFragments.size();
        }
        //重写获取标题的方法
        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            //返回标题数组的位置在获取名称
            return mTitles.get(position).getName();
        }
    }
}
