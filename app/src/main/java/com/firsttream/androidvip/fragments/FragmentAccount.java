package com.firsttream.androidvip.fragments;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.firsttream.account.activitys.ActivityLoginOrRegister;
import com.firsttream.account.activitys.ActivityMassage;
import com.firsttream.account.activitys.ActivitySign;
import com.firsttream.account.activitys.RobotActivity;
import com.firsttream.androidvip.R;
import com.firsttream.androidvip.adapters.IteamAdapter;
import com.firsttream.androidvip.entity.IteamContentBean;
import com.firsttream.androidvip.model.FragmentAccountModel;
import com.firsttream.androidvip.persenter.FragmentAccountPersenter;
import com.firsttream.androidvip.view.FragmentAccountView;
import com.firsttream.baselibrary.base.BaseFragment;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.util.SpUtil;

import java.util.List;

/**
 * 作者：周建峰
 * 时间：2018-12-13
 * 我的fragment
 */
public class FragmentAccount<M extends IContract.Model, V extends IContract.View, P extends BasePersenter> extends BaseFragment<M, V, P> implements FragmentAccountView, View.OnClickListener {

    private final String TAG = FragmentAccount.class.getName();
    private Context context;
    private FragmentAccountPersenter persenter;
    private IteamAdapter iteamAdapter;
    private ImageView smUserImage;
    private TextView txtUserName;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_account;
    }

    @Override
    public M createModel() {
        return (M) new FragmentAccountModel();
    }

    @Override
    public V createView() {
        return (V) this;
    }

    @Override
    public P createPersenter() {
        persenter = new FragmentAccountPersenter();
        persenter.initContext(getActivity());
        return (P) persenter;
    }

    @Override
    public void initContext(Context context) {
        super.initContext(context);
        this.context = context;
    }

    @Override
    public void initData() {
        super.initData();
        //初始化控件
        initWeight();
        setTitle("我的");
        persenter.getIteamData();
    }

    public void initWeight() {
        //初始化控件
        smUserImage = (ImageView) getView(R.id.sm_user_image);
        txtUserName = (TextView) getView(R.id.txt_user_name);
        RecyclerView recyList = (RecyclerView) getView(R.id.recy_list);
        setClick(this, R.id.sm_user_image,R.id.linear_robot,R.id.linear_face,R.id.linear_sign);
        //机器人整个线性条目点击事件
        //初始化adapter
        iteamAdapter = new IteamAdapter(context);
        //初始化控制器
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        Glide.with(context).load(R.drawable.user_image).apply(RequestOptions.bitmapTransform(new CircleCrop())).into(smUserImage);
        recyList.setLayoutManager(linearLayoutManager);
        recyList.setAdapter(iteamAdapter);

    }

    @Override
    public void getIteamData(List<IteamContentBean> list) {
        iteamAdapter.setList(list);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sm_user_image:
                goToLoginOrRegister();
                break;
            case R.id.linear_robot:
                //  启动跳转机器人activity
                startActivity(new Intent(context, RobotActivity.class));
                break;
            case R.id.linear_sign:
                startActivity(new Intent(context, ActivitySign.class));
                break;
        }
    }

    /**
     * 去登录个人信息
     */
    private void goToLoginOrRegister() {
        if (!isLogin()) {
            startActivity(new Intent(context, ActivityLoginOrRegister.class));
            return;
        }
        startActivity(new Intent(context, ActivityMassage.class));
    }

    /**
     *
     * 是否登录
     * @return
     */
    public boolean isLogin() {
        boolean isLogin = (boolean) SpUtil.getSpData(getActivity(), "isLogin", false);
        return isLogin;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isLogin()) {
            String nickName = (String) SpUtil.getSpData(getActivity(), "userNickname", "");
            txtUserName.setText(nickName);
        }else{
            txtUserName.setText("登录");
        }
    }
}