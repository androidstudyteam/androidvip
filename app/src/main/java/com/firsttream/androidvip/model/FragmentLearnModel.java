package com.firsttream.androidvip.model;

import android.content.Context;

import com.firsttream.androidvip.entity.LearnBean;
import com.firsttream.androidvip.fragments.FragmentLearn;
import com.firsttream.androidvip.view.FragmentLearnView;
import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;
import com.google.gson.Gson;

/**
 * 作者：gaojiabao
 * 时间：2018/12/12 10:56
 * 作用：学习model
 */
public class FragmentLearnModel extends BaseModel {

    private FragmentLearnView mFragmentLearnview;
    private static final int LEARNTYPE = 0x321;
    public void getLearnData(Context context,IContract.View view) {
        getString(context, Http.LEARNURL, null, LEARNTYPE,true,true,true);
        mFragmentLearnview = ((FragmentLearnView) view);
    }

    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        switch (type) {
            case LEARNTYPE:
                LearnBean learnBean = new Gson().fromJson(data, LearnBean.class);
                ((FragmentLearnView) mFragmentLearnview).getLearnData(learnBean);
                break;
        }
    }

    @Override
    public void failString(String msg) {
        super.failString(msg);

    }
}
