package com.firsttream.androidvip.model;

import android.content.Context;

import com.firsttream.androidvip.entity.ClassicDescBean;
import com.firsttream.androidvip.view.FragmentClassicDescView;
import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.baselibrary.util.Logger;
import com.google.gson.Gson;

import java.util.HashMap;

/**
 *作者:马富燕
 * 日期:2018/12/12
 * 作用:tablayout 内容的fragmentmodle FragmentClassicModle
 */

//继承basemodle
public class FragmentClassicDescModle extends BaseModel {

    private FragmentClassicDescView mClassicDescView;
    //设置常量类型
    private static final int CLASSICDESCTYPE=0x224;
    private static final String TAG="FragmentClassicDescModle";

    public void getDesc(Context context, FragmentClassicDescView view, String mId) {
        //创建新的hasmap
        HashMap<String,String> map=new HashMap<>();
        //往hasmap里传id
        map.put("news_type",mId);
        //调用get请求网络数据方法
        getString(context,Http.CLASSICDESCURL,map,CLASSICDESCTYPE,true,true,true);
        //在强转自己的view view 返回值 提上去
        mClassicDescView =(FragmentClassicDescView) view;
    }

    //重写成功方法
    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        //选择类型
        switch (type){
            case CLASSICDESCTYPE:
                //打印
                Logger.i(TAG,"哈哈"+data);
                //new gson  from 传data 和bean类
                ClassicDescBean classicDescBean = new Gson().fromJson(data, ClassicDescBean.class);
                //调用上面的关于本类的view生成方法到frgamnetview 里面括号里传bean类
                mClassicDescView.getDesc(classicDescBean);
                break;
        }
    }
}
