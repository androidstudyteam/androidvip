package com.firsttream.androidvip.model;

import android.content.Context;
import android.widget.Toast;

import com.firsttream.androidvip.entity.ClassicTitleBean;
import com.firsttream.androidvip.view.FragmentClassicView;
import com.firsttream.androidvip.view.FragmentVideoView;
import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;
import com.firsttream.baselibrary.util.Logger;
import com.google.gson.Gson;

import java.util.List;

/**
 * 作者:马富燕
 * 日期:2018/12/12
 * 作用:经典的fragmentmodle FragmentClassicModle
 */

//继承basemodle
public class FragmentClassicModle extends BaseModel {


    private FragmentClassicView mClassicView;
    //设置常量类型
    private static final int CLASSICTYPE=0x223;
    private static final String TAG="FragmentClassicModle";


    public void getClassicTitiles(Context context, IContract.View view) {
        //直接调用get传网址和map如果没有就填空 和类型
        getString(context,Http.CLASSICURL, null, CLASSICTYPE,true,true,true);
        //自己的View 强转一下自己的view 返回值 提上去
        mClassicView = (FragmentClassicView)view;
    }

    //重写成功方法
    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        //选择type
        switch (type) {
            case CLASSICTYPE:
                //new gson 传data 和bean类
                ClassicTitleBean classicTitleBean = new Gson().fromJson(data, ClassicTitleBean.class);
                //调用上面的classview生成到这个类的View 括号里传bean类
                mClassicView.getClassicTitles(classicTitleBean);
                break;
        }
    }

    //重写失败方法
    @Override
    public void failString(String msg) {
        super.failString(msg);
        //打印log失败
        Logger.i(TAG, "失败");
    }
}
