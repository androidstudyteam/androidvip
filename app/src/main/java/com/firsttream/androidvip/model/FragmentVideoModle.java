package com.firsttream.androidvip.model;

import android.content.Context;

import com.firsttream.androidvip.entity.VideoHomeBean;
import com.firsttream.androidvip.view.FragmentVideoView;
import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.net.Http;
import com.google.gson.Gson;

/**
 * 作者:马富燕
 * 日期:2018/12/12
 * 作用:视频的fragmentmodle FragmentVideoModle
 */

//继承basemodle
public class FragmentVideoModle extends BaseModel {

    //设置常量类型
    private static final int VIDEOHOMETYPE = 0x225;
    private FragmentVideoView videoView;

    public void getVideoHome(Context context, FragmentVideoView view) {
        //请求get网络请求
        getString(context,Http.VIDEOHOMEURL, null, VIDEOHOMETYPE,true,true,true);
        //强转自己view返回值提上去
        videoView = (FragmentVideoView) view;
    }

    //成功方法

    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        //选择类型
        switch (type){
            case VIDEOHOMETYPE:
                //new gson from 传data和bean类
                VideoHomeBean videoHomeBean = new Gson().fromJson(data, VideoHomeBean.class);
                //调用上面的view 生成方法 传bean类括号里
                videoView.getVideoHome(videoHomeBean);
                break;
        }
    }
}
