package com.firsttream.androidvip.model;

import android.content.Context;

import com.firsttream.androidvip.R;
import com.firsttream.androidvip.entity.IteamContentBean;
import com.firsttream.androidvip.view.FragmentAccountView;
import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 作者：周建峰
 * 时间：2018-12-13
 * FragmentAccountModel
 */
public class FragmentAccountModel extends BaseModel {


    public void getIteamData(IContract.View view) {
        List<IteamContentBean> list = new ArrayList<>();
        list.add(new IteamContentBean("我的关注", R.drawable.account_about_icon));
        list.add(new IteamContentBean("关注我的", R.drawable.account_refresh_icon));
        list.add(new IteamContentBean("我的分享", R.drawable.account_share_icon));
        list.add(new IteamContentBean("我的帖子", R.drawable.account_service_icon));
        list.add(new IteamContentBean("我的弹一弹", R.drawable.account_push_icon));
        list.add(new IteamContentBean("设置", R.drawable.account_setting_icon));
        ((FragmentAccountView) view).getIteamData(list);
    }

}
