package com.firsttream.androidvip.model;

import android.content.Context;

import com.firsttream.androidvip.view.FragmentHomePageView;
import com.firsttream.baselibrary.base.BaseModel;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;

import java.util.HashMap;
import java.util.Map;

/**
 * 作者:xjh
 * 时间:2018-12-12
 * 作用:FragmentHomePageModel
 */
public class FragmentHomePageModel extends BaseModel {

    private IContract.View view;
    private static final int HOMEPAGE_BANNER = 0x123;
    private static final int HOMEPAGE_BUTTOMARTICLE = 0x124;

    /**
     * banner图
     */
    public void getBanner(Context context,IContract.View view) {
        this.view = view;
        getString(context,Http.HOMEPAFE_BANNER, null, HOMEPAGE_BANNER,true,true,true);
    }

    /**
     * 底部文章
     */
    public void getButtomArticle(Context context,IContract.View view) {
        this.view = view;
        Map<String,String> map=new HashMap<>();
        map.put("news_type",0+"");
        getString(context,Http.HOMEPAFE_BUTTOMARTICLE, map, HOMEPAGE_BUTTOMARTICLE,true,true,true);
    }

    /***
     * 成功方法
     * @param data
     * @param type
     */
    @Override
    public void successString(String data, int type) {
        super.successString(data, type);
        switch (type) {
            case HOMEPAGE_BANNER:
                ((FragmentHomePageView)view).getBanner(data);
                break;
            case HOMEPAGE_BUTTOMARTICLE:
                ((FragmentHomePageView)view).getButtomArticle(data);
                break;
        }
    }

    /***
     * 失败方法
     * @param msg
     */
    @Override
    public void failString(String msg) {
        super.failString(msg);
    }
}
