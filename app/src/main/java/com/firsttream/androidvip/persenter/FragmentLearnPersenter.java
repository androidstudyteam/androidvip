package com.firsttream.androidvip.persenter;

import com.firsttream.androidvip.model.FragmentLearnModel;
import com.firsttream.androidvip.view.FragmentLearnView;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
/**
*作者：gaojiabao
*时间：2018/12/13 11:18
*作用：学习persenter
*/
public class FragmentLearnPersenter extends BasePersenter<FragmentLearnModel, FragmentLearnView> {

    @Override
    protected void onViewDestory() {
        if (model != null) {
            model.stopRequest();
        }
    }

    public void getLearnData() {
        model.getLearnData(context,getView());
    }
}
