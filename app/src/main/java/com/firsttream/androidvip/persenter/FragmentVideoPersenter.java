package com.firsttream.androidvip.persenter;

import com.firsttream.androidvip.model.FragmentVideoModle;
import com.firsttream.androidvip.view.FragmentVideoView;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;

/**
 *作者:马富燕
 * 日期:2018/12/12
 * 作用:视频的fragmentpersenter FragmentVideoPersenter
 */

//继承BasePersenter 泛型 自己创建的modle和view 在实现方法
public class FragmentVideoPersenter extends BasePersenter<FragmentVideoModle,FragmentVideoView> {

    @Override
    protected void onViewDestory() {
        //先判断modle非空
        if(model!=null){
            //非空的话 modle就调用删除方法
            model.stopRequest();
        }
    }

    //生成过来的方法
    public void getVideoHome() {
        //用modle生成方法传到modle里面 括号里传getview()方法
        model.getVideoHome(context,getView());
    }
}
