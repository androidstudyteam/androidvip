package com.firsttream.androidvip.persenter;

import com.firsttream.androidvip.model.FragmentClassicModle;
import com.firsttream.androidvip.view.FragmentClassicView;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;

/**
 *作者:马富燕
 * 日期:2018/12/12
 * 作用:经典的fragmentpersenter FragmentClassicPersenter
 */

//继承BasePersenter 泛型 自己创建的modle和view 在实现方法
public class FragmentClassicPersenter extends BasePersenter<FragmentClassicModle,FragmentClassicView> {

    @Override
    protected void onViewDestory() {
        //判断model非空
        if (model != null) {
            //model 父类里的停止方法
            model.stopRequest();
        }
    }

    //要是有atatic 就删了static 没有就不用了
    public void getClassicTitles() {
        //用modle调用写一个方法生成到 这个类相关的modle里 括号里传获取view的方法
        model.getClassicTitiles(context,getView());
    }
}
