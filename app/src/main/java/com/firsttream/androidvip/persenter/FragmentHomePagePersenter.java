package com.firsttream.androidvip.persenter;

import com.firsttream.androidvip.model.FragmentHomePageModel;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
import com.firsttream.baselibrary.net.Http;
/**
 * 作者:xjh
 * 时间:2018-12-12
 * 作用:FragmentHomePagePersenter(首页)
 */
public class FragmentHomePagePersenter<M extends IContract.Model, V extends IContract.View> extends BasePersenter<M, V> {
    @Override
    protected void onViewDestory() {

    }

    /**
     * banner图
     */
    public void getBanner() {
        ((FragmentHomePageModel) model).getBanner(context,getView());
    }
    /**
     * 底部文章
     */
    public void getButtomArticle() {
        ((FragmentHomePageModel) model).getButtomArticle(context,getView());
    }

}
