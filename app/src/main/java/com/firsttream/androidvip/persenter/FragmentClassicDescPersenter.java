package com.firsttream.androidvip.persenter;

import com.firsttream.androidvip.model.FragmentClassicDescModle;
import com.firsttream.androidvip.view.FragmentClassicDescView;
import com.firsttream.baselibrary.base.BasePersenter;

/**
 *作者:马富燕
 * 日期:2018/12/12
 * 作用:tablayout内容的fragmentpersenter FragmentClassicDescPersenter
 */

//继承BasePersenter 泛型 自己创建的modle和view 在实现方法
public class FragmentClassicDescPersenter extends BasePersenter<FragmentClassicDescModle,FragmentClassicDescView> {

    @Override
    protected void onViewDestory() {
        //先判断modle非空
        if(model!=null){
            //非空的话 modle就调用删除方法
            model.stopRequest();
        }
    }

    public void getDesc(String mId) {
        //用modle生成方法 到modle里面 括号里传获取view方法 getview()
        model.getDesc(context,getView(),mId);
    }

}
