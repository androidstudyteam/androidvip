package com.firsttream.androidvip.persenter;

import com.firsttream.androidvip.model.FragmentAccountModel;
import com.firsttream.baselibrary.base.BasePersenter;
import com.firsttream.baselibrary.contract.IContract;
/**
 * 作者：周建峰
 * 时间：2018-12-13
 * FragmentAccountPersenter
 */
public class FragmentAccountPersenter<M extends IContract.Model, V extends IContract.View> extends BasePersenter<M, V> {
    @Override
    protected void onViewDestory() {

    }

    public void getIteamData() {
        ((FragmentAccountModel) model).getIteamData(getView());
    }
}
