package com.firsttream.androidvip.view;

import com.firsttream.androidvip.entity.ClassicDescBean;
import com.firsttream.baselibrary.contract.IContract;

/**
 *作者:马富燕
 * 日期:2018/12/12
 * 作用:tablayout内容的fragment view 一个接口  FragmentClassicDescView
 */

//继承契约类的view
public interface FragmentClassicDescView extends IContract.View {

    void getDesc(ClassicDescBean classicDescBean);
}
