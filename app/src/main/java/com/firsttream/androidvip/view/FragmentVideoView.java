package com.firsttream.androidvip.view;

import com.firsttream.androidvip.entity.VideoHomeBean;
import com.firsttream.baselibrary.contract.IContract;

/**
 *作者:马富燕
 * 日期:2018/12/12
 * 作用:视频的fragmentview 一个接口  FragmentVideoView
 */

//继承契约类的view
public interface FragmentVideoView extends IContract.View {

    void getVideoHome(VideoHomeBean videoHomeBean);
}
