package com.firsttream.androidvip.view;

import com.firsttream.baselibrary.contract.IContract;

/**
 * 作者:xjh
 * 时间:2018-12-12
 * 作用:FragmentHomePageView(首页)
 */
public interface FragmentHomePageView extends IContract.View {

    void getBanner(String data);

    /**
     * 底部文章
     */
    void getButtomArticle(String data);

}
