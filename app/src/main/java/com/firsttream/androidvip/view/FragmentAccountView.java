package com.firsttream.androidvip.view;

import com.firsttream.androidvip.entity.IteamContentBean;
import com.firsttream.baselibrary.contract.IContract;

import java.util.List;

public interface FragmentAccountView extends IContract.View {
    void getIteamData(List<IteamContentBean> list);
}
