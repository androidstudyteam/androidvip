package com.firsttream.androidvip.view;

import com.firsttream.androidvip.entity.LearnBean;
import com.firsttream.baselibrary.contract.IContract;
/**
*作者：gaojiabao
*时间：2018/12/13 11:18
*作用：学习view
*/
public interface FragmentLearnView extends IContract.View{

    void getLearnData(LearnBean learnBean);

}
