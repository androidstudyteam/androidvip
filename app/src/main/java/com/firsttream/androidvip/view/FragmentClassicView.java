package com.firsttream.androidvip.view;

import com.firsttream.androidvip.entity.ClassicTitleBean;
import com.firsttream.baselibrary.contract.IContract;

/**
 *作者:马富燕
 * 日期:2018/12/12
 * 作用:经典的fragmentview 一个接口  FragmentClassicView
 */

//继承契约类的view
public interface FragmentClassicView extends IContract.View {

    void getClassicTitles(ClassicTitleBean classicTitleBean);
}
