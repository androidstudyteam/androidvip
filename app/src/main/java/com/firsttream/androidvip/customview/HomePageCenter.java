package com.firsttream.androidvip.customview;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.firsttream.androidvip.R;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.homepage.activitys.ActivityDryquestions;
import com.firsttream.homepage.activitys.ActivityEveryBodyBBS;
import com.firsttream.homepage.activitys.ActivityPlayAPlay;
import com.firsttream.baselibrary.util.Logger;
import com.firsttream.homepage.activitys.ActivityPlayAPlay;

/***
 *作者:XJH
 *时间:2018-12-13
 *作用:HomePageCenter(首页下面的view)
 */
public class HomePageCenter extends RelativeLayout implements View.OnClickListener {

    private ImageView picvipdrycargo, piceverybodyBBS, pictechnicalpost, playaplay;
    private ImageView imglatesttopposts;
    private ImageView imgopenproject;
    private ImageView imgdryquestions;
    private View mPopView;

    public HomePageCenter(Context context) {
        super(context);
        initView(context);
    }

    public HomePageCenter(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);

    }

    public HomePageCenter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);

    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_homepagecenter, null);
        initWeigth(view);
        addView(view);

    }

    private void initWeigth(View view) {
        picvipdrycargo = (ImageView) view.findViewById(R.id.homepage_img_vipdrycargo);
        piceverybodyBBS = (ImageView) (ImageView) view.findViewById(R.id.homepage_img_everybodyBBS);
        pictechnicalpost = (ImageView) view.findViewById(R.id.homepage_img_technicalpost);
        playaplay = (ImageView) view.findViewById(R.id.homepage_img_playaplay);
        imgopenproject = (ImageView) view.findViewById(R.id.Homepage_img_openproject);
        imglatesttopposts = (ImageView) view.findViewById(R.id.Homepage_img_latesttopposts);
        imgdryquestions = (ImageView) view.findViewById(R.id.Homepage_img_dryquestions);
        mPopView = (View) view.findViewById(R.id.homepage_view_popview);
        picvipdrycargo.setOnClickListener(this);
        playaplay.setOnClickListener(this);
        piceverybodyBBS.setOnClickListener(this);
        imgdryquestions.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.homepage_img_playaplay://弹一弹
                Logger.d("tagHomePageCenter", "已点击");
                getContext().startActivity(new Intent(getContext(), ActivityPlayAPlay.class));
                break;
            case R.id.homepage_img_everybodyBBS://大家论坛
                Logger.d("tagHomePageCenter", "已点击");
                getContext().startActivity(new Intent(getContext(), ActivityEveryBodyBBS.class));
                break;
            case R.id.Homepage_img_dryquestions://大家论坛
                Logger.d("tagHomePageCenter", "已点击");
                getContext().startActivity(new Intent(getContext(), ActivityDryquestions.class));
                break;
        }

    }
}
